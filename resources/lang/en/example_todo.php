<?php
return [
    "todo" => "Todo",
    "todos" => "Todos",
    "menu" => [
        "all" => "All Todos",
        "completed" => "Completed",
        "incomplete" => "Incomplete",
        "create" => "Add new",
        "edit" => "Edit",
        "delete" => "Delete",
    ],
    "forms" => [
        "name" => "Name",
        "description" => "Description",
        "status" => "Status",
        "save" => "Save",
        "edit" => "Edit",
        "back" => "Back",
        "create_todo" => "Create Todo",
        "edit_todo" => "Edit Todo",
    ],
    "errors" => [
        "name" => [
            "required" => "Todo name is required"
        ],
        "description" => [
            "required" => "Description is required"
        ],
        "status" => [
            "required" => "Status is required"
        ],
    ],
];
