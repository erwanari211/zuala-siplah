<?php
return [
    "todo" => "Rencana",
    "todos" => "Rencana",
    "menu" => [
        "all" => "Semua rencana",
        "completed" => "Selesai",
        "incomplete" => "Belum selesai",
        "create" => "Tambah baru",
        "edit" => "Ubah",
        "delete" => "Hapus",
    ],
    "forms" => [
        "name" => "Rencana",
        "description" => "Rincian",
        "status" => "Status",
        "save" => "Simpan",
        "edit" => "Ubah",
        "back" => "Kembali",
        "create_todo" => "Buat Rencana",
        "edit_todo" => "Ubah Rencana",
    ],
    "errors" => [
        "name" => [
            "required" => "Rencama harus diisi"
        ],
        "description" => [
            "required" => "Rincian harus diisi"
        ],
        "status" => [
            "required" => "Status harus dipilih"
        ],
    ],
];
