@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Banner Groups')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.administrators.backend.inc.navbar-top-administrator')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.website.banners.index') }}">Banners</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.website.banner-groups.index') }}">Groups</a>
                        </li>
                    </ol>

                    <h3>Banner Groups</h3>

                    @include('flash::message')

                    <a class="btn btn-primary"
                        href="{{ route('administrator.website.banner-groups.create') }}">
                        Add Banner Group
                    </a>
                    <a class="btn btn-default"
                        href="{{ route('administrator.website.banners.index') }}">
                        View Banners
                    </a>

                    {!! Form::open([
                            'route' => ['administrator.website.cms.forget-caches'],
                            'method' => 'DELETE',
                            'style' => 'display: inline-block;'
                        ]) !!}
                        {!! Form::bs3SubmitHtml('Clear Caches', ['class'=>'btn btn-danger', 'onclick'=>'return confirm("Clear?")']); !!}
                    {!! Form::close() !!}

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Name</th>
                                            <th>Active</th>
                                            <th>Type</th>
                                            <th>Sort Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($bannerGroups))
                                            @php
                                                $no = $bannerGroups->firstItem();
                                            @endphp
                                            @foreach ($bannerGroups as $banner)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-default"
                                                            href="{{ route('administrator.website.banner-groups.show', [$banner->id]) }}" title="View">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('administrator.website.banner-groups.edit', [ $banner->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['administrator.website.banner-groups.destroy', $banner->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        {{ $banner->name }}
                                                        @if ($banner->display_name)
                                                            <br>
                                                            ({{ $banner->display_name }})
                                                        @endif
                                                    </td>
                                                    <td>{{ $banner->active ? 'Yes' : 'No' }}</td>
                                                    <td>{{ $banner->type }}</td>
                                                    <td>{{ $banner->sort_order }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $bannerGroups->links() }}
                        </div>
                    </div>



                </div>
            </div>
        </div>


    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
