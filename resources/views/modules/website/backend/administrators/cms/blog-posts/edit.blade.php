@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Edit Post')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
    <style>
      .mce-ico.mce-i-fa {
          font-weight: 900;
          font-family: "Font Awesome 5 Free";
          line-height: 1;
          text-rendering: auto;
          font-variant: normal;
          font-style: normal;
          display: inline-block;
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
      }
    </style>
@endpush

@push('js')
    <script src="{{ asset('assets/adminlte-2.4.5/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins') }}/tinymce_4.7.9/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.datetimepicker').datetimepicker();

            tinymce.PluginManager.add('product_collections', function(editor, url) {
                // Add a button that opens a window
              editor.addButton('product_collections', {
                text: false,
                icon: 'fa fa-bullhorn',
                title: 'Product Collections',
                onclick: function() {
                  // Open window
                  editor.windowManager.open({
                    title: 'Featured Product',
                    body: [
                      {type: 'textbox', name: 'collection_id', label: 'Collection Id'},
                      {type: 'textbox', name: 'limit', label: 'Limit'},
                    ],
                    onsubmit: function(e) {
                      // Insert content when the window form is submitted
                      var collectionId = e.data.collection_id;
                      var postLimit = 0;
                      var limit = e.data.limit;
                      if (collectionId) {
                        postLimit = (Number.isInteger(parseInt(limit))
                          && (parseInt(limit) > 0))
                          ? parseInt(limit)
                          : 0;

                        editor.insertContent(`
                          <div class="product-collections"
                            data-id="${collectionId}"
                            data-limit="${postLimit}"
                            style="
                              background-color: rgba(204,204,204,0.5);
                              border: 1px dashed #333;">
                            [product_collections | id:${collectionId} | limit:${postLimit} ]
                          </div>
                          <br>
                        `);
                      }
                    }
                  });
                }
              });

              // Adds a menu item to the tools menu
              editor.addMenuItem('product_collections', {
                text: 'Featured Product',
                context: 'tools',
                onclick: function() {
                  editor.windowManager.alert('Add product collections to blog post');
                }
              });
            });


            /**
             * Tinymce settings
             */
            var plugins = 'preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern code help spellchecker product_collections';
            var toolbar1 = 'formatselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat';
            var toolbar2 = 'undo redo | link unlink image media styleselect | nanospell | product_collections | fullscreen preview code ';

            tinymce.init({
                selector: '#content',
                height: 300,
                plugins: plugins,
                toolbar1: toolbar1,
                toolbar2: toolbar2,
                image_advtab: true,
                selection_toolbar: 'bold italic | quicklink h2 h3 blockquote',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",
                content_css: [
                  '//www.tinymce.com/css/codepen.min.css',
                  '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
                ],

                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
                image_class_list: [
                    {title: 'Lazyload', value: 'lazy'},
                    {title: 'None', value: ''},
                ],

                table_default_attributes: {
                    'class': 'table table-bordered'
                },
                table_default_styles: {},
                table_class_list: [
                    {title: 'None', value: ''},
                    {title: 'Table Bordered', value: 'table table-bordered'},
                ],

                menubar:true,
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.website.cms.blog-posts.index') }}">Posts</a>
                        </li>
                        <li class="active">Edit</li>
                    </ol>

                    <h3>Edit Post</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($post, [
                                'route' => ['administrator.website.cms.blog-posts.update', $post->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('title') !!}
                                {!! Form::bs3Text('slug') !!}
                                {!! Form::bs3Textarea('content') !!}
                                {!! Form::bs3Textarea('excerpt') !!}

                                <hr>

                                {!! Form::bs3Select('category', $dropdown['categories'], $postCategories) !!}

                                {!! Form::bs3Select('status', $dropdown['statuses']) !!}
                                {!! Form::bs3Datetimepicker('published_at') !!}

                                <hr>

                                {!! Form::bs3Text('seo_title', $postMeta['seo_title']) !!}
                                {!! Form::bs3Textarea('seo_description', $postMeta['seo_description']) !!}
                                {!! Form::bs3Text('seo_keywords', $postMeta['seo_keywords']) !!}

                                <hr>

                                {!! Form::bs3File('featured_image') !!}
                                @if (isset($postMeta['featured_image']))
                                    <div class="form-group ">
                                        <img class="img-thumbnail" src="{{ asset($postMeta['featured_image']) }}"
                                            style="max-height: 150px; max-width: 150px;">
                                    </div>
                                @endif
                                <span class="help-block">Recomended size 1200 x 400</span>

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('administrator.website.cms.blog-posts.index') }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
