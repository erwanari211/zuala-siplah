@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Layout Elements')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-banner-id]').on('click', function(event) {
                event.preventDefault();
                var bannerId = $(this).attr('data-banner-id');
                $('#banner_id').val(bannerId);
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.administrators.backend.inc.navbar-top-administrator')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="#">CMS</a>
                        </li>
                        <li class="active">
                            <a href="{{ route('administrator.website.cms.layout-elements.index') }}">Layout Elements</a>
                        </li>
                    </ol>

                    <h3>Layout Elements</h3>

                    @include('flash::message')
                    @include('themes.marika-natsuki.messages')

                    <a class="btn btn-primary"
                        href="{{ route('administrator.website.cms.layout-elements.create') }}">
                        Add Layout Element
                    </a>

                    {!! Form::open([
                            'route' => ['administrator.website.cms.forget-caches'],
                            'method' => 'DELETE',
                            'style' => 'display: inline-block;'
                        ]) !!}
                        {!! Form::bs3SubmitHtml('Clear Caches', ['class'=>'btn btn-danger', 'onclick'=>'return confirm("Clear?")']); !!}
                    {!! Form::close() !!}

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Group</th>
                                            <th>Position</th>
                                            <th>Name</th>
                                            <th>Active</th>
                                            <th>Sort Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($layoutElements))
                                            @php
                                                $no = $layoutElements->firstItem();
                                            @endphp
                                            @foreach ($layoutElements as $element)
                                                @php
                                                    $isExpired = false;
                                                    $now = date("Y-m-d");
                                                    if ($now > $element->expired_at) {
                                                        $isExpired = true;
                                                    }
                                                    $expiredClass = 'btn btn-xs ';
                                                    $expiredClass .= ($isExpired ? ' btn-danger' : ' btn-success');
                                                @endphp
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('administrator.website.cms.layout-elements.edit', [$element->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['administrator.website.cms.layout-elements.destroy', $element->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>{{ $element->group }}</td>
                                                    <td>{{ $element->position }}</td>
                                                    <td>{{ $element->name }}</td>
                                                    <td>{{ $element->active ? 'Yes' : 'No' }}</td>
                                                    <td>{{ $element->sort_order }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $layoutElements->links() }}
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
