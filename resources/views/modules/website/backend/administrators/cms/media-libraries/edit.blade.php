@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Edit Media')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="#">CMS</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.website.cms.media-libraries.index') }}">Media Libraries</a>
                        </li>
                        <li class="active">Edit</li>
                    </ol>

                    <h3>Edit Media</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($mediaLibrary, [
                                'route' => ['administrator.website.cms.media-libraries.update', $mediaLibrary->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Select('group', $dropdown['groups']) !!}
                                {!! Form::bs3Text('name') !!}
                                {!! Form::bs3Textarea('description') !!}
                                <div class="form-group ">
                                    <label for="file" class="control-label">File</label>
                                    <div>
                                        @if ($mediaLibrary->fileType == 'image')
                                            <img class="img-thumbnail" src="{{ asset($mediaLibrary->url) }}" style="height: 100px;">
                                        @else
                                            <a class="btn btn-default btn-xs"
                                                href="{{ asset($mediaLibrary->url) }}" target="_blank">Download</a>
                                        @endif
                                    </div>
                                </div>
                                {!! Form::bs3Text('tags') !!}
                                {!! Form::bs3Select('active', $dropdown['yes_no']) !!}
                                {!! Form::bs3Select('is_public', $dropdown['yes_no']) !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('administrator.website.cms.media-libraries.index') }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
