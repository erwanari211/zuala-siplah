@php
    // $_themeSetting['skin'] = 'skin-red';

    $postTitle = $post['title'];
    $postTitle = (isset($postMeta['seo_title']) && $postMeta['seo_title']) ? $postMeta['seo_title'] : $postTitle;
    $postDescription = $post->excerpt;
    $postDescription = isset($postMeta['seo_description']) ? $postMeta['seo_description'] : $postDescription;

    $templatePageTitle = '{page} | {APP}';
    $templatePageDescription = $postDescription;
    $searchArray = ['{APP}', '{page}'];
    $replaceArray = [env('APP_NAME'), $postTitle];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .pagination {
            margin-top: 0;
        }

        .hr-banner {
            margin: 10px 0;
            border-color: #ddd;
        }

        .product-collections {
          display: none;
        }

        .product-collections.active {
            margin: 16px 0;
            padding: 10px;
            display: block;
            background-color: #e9ebee;
        }

        .sponsored-content {
          text-align: center;
        }

        .sponsored-content-message {
          border: 1px solid #ccc;
          padding: 4px 12px;
          background-color: #eee;
        }
    </style>
@endpush

@push('js')
  <script>
    $(document).ready(function() {
      function loadData() {
        let elements = $('.product-collections');
        elements.each(function(index, elm) {
          let item = $(elm);

          item.hide();
          let id = item.attr('data-id');
          let limit = item.attr('data-limit');
          item.removeAttr('style');
          item.html('');

          let url = `{{ url('w/product-collections/') }}/${id}.json?limit=${limit}`;

          // get data by id
          $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
          })
          .done(function(data) {
            if (data.success) {
              item.addClass('active');

              let content = '';

              // loop products
              let products = data.data.products;

              let productContent = '';

              $(products).each(function(index, product) {
                productContent += `
                  <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="product">
                      <a href="${product.productUrl}">
                        <div class="product__image-wrapper">
                          <img class="product__image" src="{{ asset('') }}${product.image}">
                        </div>
                      </a>
                      <span class="product__name">
                        <a href="${product.productUrl}">
                          ${product.name}
                        </a>
                      </span>
                      <div class="product__price">
                        <span class="product__current-price">${product.formattedPrice}</span>
                      </div>
                    </div>
                  </div>
                `;

              });

              content += `
                <div class="row">
                  <div class="col-sm-12">
                    <div class="products products-grid row">
                      ${productContent}
                    </div>
                  </div>
                </div>
              `;

              content += `
                <div class="sponsored-content">
                  <small class="sponsored-content-message">Advertisement</small>
                </div>
              `;

              item.html(content);
              item.show();
            }
          });
        });

      }

      setTimeout(loadData, 2000);

    });
  </script>

@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-normal')

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="box">
                        <div class="box-content">
                            <ol class="breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                </li>
                                <li>
                                    <a href="{{ route('website.blog.posts.index') }}">Blog</a>
                                </li>
                                <li class="active">
                                    {{ $post->title }}
                                </li>
                            </ol>

                            <h3 class="mt-0">{{ $post['title'] }}</h3>


                            @if (isset($postMeta['featured_image']))
                                <img class="img-responsive img-thumbnail mb-4" src="{{ asset($postMeta['featured_image']) }}">
                            @endif

                            {!! $post['content'] !!}

                            <hr>

                            <p>
                                Posted by {{ $post->user->name }} <br>
                                <i class="fa fa-clock"></i> {{ \Carbon\Carbon::parse($post->published_at)->diffForHumans() }}
                            </p>

                            @if (count($post->categories))
                                <div>
                                    @foreach ($post->categories as $postCategory)
                                        <a href="{{ route('website.blog.categories.show', $postCategory->slug) }}"
                                            class="badge badge-secondary">
                                            <i class="fa fa-fw fa-tag"></i>
                                            {{ $postCategory->name }}
                                        </a>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-content">
                            <h3 class="mt-0">Categories</h3>

                            @foreach ($categories as $category)
                                <a href="{{ route('website.blog.categories.show', $category->slug) }}">
                                    {{ $category->name }} ({{ $category->posts_count }})
                                </a>
                                <br>
                            @endforeach
                        </div>
                    </div>

                    @include('modules.website.frontend.blog.posts.inc.sidebar-banners')
                    @include('modules.website.frontend.blog.posts.inc.related-products')
                </div>
            </div>
        </div>

    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
