@php
    // $_themeSetting['skin'] = 'skin-red';

    $templatePageTitle = '{page} | {APP}';
    $templatePageDescription = 'Official blog from {APP}. Update news, information and promo.';
    $searchArray = ['{APP}', '{page}'];
    $replaceArray = [env('APP_NAME'), 'Blog'];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .pagination {
            margin-top: 0;
        }

        .hr-banner {
            margin: 10px 0;
            border-color: #ddd;
        }
    </style>
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-normal')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="box">
                        <div class="box-content">
                            <ol class="breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                </li>
                                <li>
                                    <a href="{{ route('website.blog.posts.index') }}">Blog</a>
                                </li>
                                <li class="active">
                                    List
                                </li>
                            </ol>

                            @if (count($posts))
                                @foreach ($posts as $post)
                                    @php
                                        $postMeta = $post->getAllMeta();
                                        $imageUrl = asset('images/no-image-v2.png');
                                        if (isset($postMeta['featured_image'])) {
                                            $imageUrl = asset($postMeta['featured_image']);
                                        }
                                    @endphp
                                    <div class="media mb-4">
                                        <a class="pull-right" href="{{ route('website.blog.posts.show', $post->slug) }}">
                                            <img class="media-object"
                                                style="width: 150px; height: 100px;"
                                                src="{{ $imageUrl }}" alt="Image">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <a href="{{ route('website.blog.posts.show', $post->slug) }}">
                                                    {{ $post->title }}
                                                </a>
                                            </h4>
                                            @if ($post->excerpt)
                                                <p class="mb-4">
                                                    {{ $post->excerpt }}
                                                </p>
                                            @else
                                                <p class="mb-4">
                                                    {{ str_limit(strip_tags($post->content), 200) }}
                                                </p>
                                            @endif
                                            <p>
                                                Posted by {{ $post->user->name }} <br>
                                                <i class="fa fa-clock"></i>
                                                {{ \Carbon\Carbon::parse($post->published_at)->diffForHumans() }}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <p>No data found</p>
                            @endif


                            {{ $posts->links() }}

                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-content">
                            <h3 class="mt-0">Categories</h3>

                            @foreach ($categories as $category)
                                <a href="{{ route('website.blog.categories.show', $category->slug) }}">
                                    {{ $category->name }} ({{ $category->posts_count }})
                                </a>
                                <br>
                            @endforeach
                        </div>
                    </div>

                    @include('modules.website.frontend.blog.posts.inc.sidebar-banners')
                </div>
            </div>
        </div>

    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
