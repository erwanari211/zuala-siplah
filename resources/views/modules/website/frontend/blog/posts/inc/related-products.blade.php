@if (isset($products) && count($products))
  <h3>Related Products</h3>
  <div class="products products-grid row">
    @foreach ($products as $item)
      @php
        $product = $item->product;
      @endphp
      @php
        $productMetas = $product->getAllMeta();
        $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
        $discountAmountPercent = $product->discountAmountPercent;
        $store = $product->store;
        $marketplace = $store->marketplace;
      @endphp
      <div class="col-sm-12 col-xs-6">
        <div class="product">
            @if ($discountAmountPercent)
                <span class="product__label right">
                    <div class="text-center">
                    {{ $discountAmountPercent }}%<br>OFF
                    </div>
                </span>
            @endif
            <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                <div class="product__image-wrapper">
                    <img class="product__image lazy" data-src="{{ $product->image_url }}">
                </div>
            </a>
            <strong class="product__name">
                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                    {{ $product->name }}
                </a>
            </strong>

            @php
                $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
            @endphp

            @if (!$hasKemdikbudZonePrice)

            <div class="product__price">
                @if ($originalPrice)
                    <span class="product__original-price">{{ "Rp.".formatNumber($originalPrice) }}</span>
                @endif
                <span class="product__current-price">{{ "Rp.".formatNumber($product->price) }}</span>
            </div>

            @endif

            @if ($hasKemdikbudZonePrice)
                @php
                    $price = 0;
                    $minPrice = 0;
                    $maxPrice = 0;

                    $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
                    $kemdikbudMinMaxZonePrice = $product->getKemdikbudMinMaxZonePrices($mappedKemdikbudZonePrices);
                    $minPrice = $kemdikbudMinMaxZonePrice['minPrice'];
                    $maxPrice = $kemdikbudMinMaxZonePrice['maxPrice'];
                    $formattedZonePrice = $product->getKemdikbudFormattedZonePrices($minPrice, $maxPrice);
                @endphp
                <div class="product__price">
                    <span class="product__current-price">{{ $formattedZonePrice }}</span>
                </div>
            @endif

            <div class="product__store">
                <div class="product__store__name">
                    <a class="text-muted"
                        href="{{ route('stores.show', [$marketplace->slug, $product->store->slug]) }}">
                        <span>{{ $product->store->name }}</span>
                    </a>
                </div>
                @php
                    $store = $product->store;
                    $addresses = $store->addresses;
                    $storeAddress = null;
                    foreach ($addresses as $address) {
                        if ($address->default_address) {
                            $storeAddress = $address;
                        }
                    }

                    $location = [];
                    if ($storeAddress) {
                        if ($storeAddress->city) {
                            $cityName = $storeAddress->city->name;
                            $location[] = $cityName;
                        }
                        if ($storeAddress->province) {
                            $location[] = $storeAddress->province->name;
                        }
                    }

                    $storeAddress = '';
                    $storeAddress = ucwords(strtolower(implode(', ', $location)))
                @endphp
                <div class="product__store__location">
                    <small title="{{ $storeAddress }}">
                        <i class="fa fa-map-marker-alt text-danger"></i>
                        <span class="text-muted">{{ $storeAddress }}</span>
                    </small>
                </div>
            </div>
        </div>
      </div>
    @endforeach
  </div>
@endif
