@if (isset($blogBanners['sidebar']) && count($blogBanners['sidebar']))
  <div class="box">
    <div class="box-content">
      @foreach ($blogBanners['sidebar'] as $banner)
        @if ($banner->link)
          <a href="{{ $banner->link }}">
            <img class="lazy mb-2" data-src="{{ asset($banner->image) }}" width="100%">
          </a>
        @else
          <img class="lazy mb-2" data-src="{{ asset($banner->image) }}" width="100%">
        @endif
      @endforeach
    </div>
  </div>
@endif
