@php
@endphp
<div class="sidenav sidenav-overlay sidenav-overlay--left" id="user-sidebar">
    <div class="sidenav-overlay-toolbar">
        <button class="btn btn-default btn-xs sidenav-overlay-close"
            data-action="close-sidenav"
            data-target="#user-sidebar">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidenav-overlay-container">
        <div class="sidenav-overlay-content slimscroll">
            <nav class="metismenu-vertical-container">
                <ul class="metismenu">
                    <li>
                        <div class="media" style="padding: 10px 15px;">
                            <span class="pull-left" href="#">
                                <img class="media-object img-circle img-thumbnail" src="{{ asset($user->photo) }}" alt="Image" width="60" height="60">
                            </span>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    {{ $user->name }}
                                </h4>
                                <p><span class="btn btn-xs btn-default">User Profile</span></p>
                            </div>
                        </div>
                    </li>
                    <li> <span class="divider"><i class="fa fa-fw fa-user"></i> Account</span> </li>
                    <li> <a href="{{ route('users.accounts.profile.index') }}">Profile</a> </li>
                    <li> <a href="{{ route('users.accounts.password.edit') }}">Password</a> </li>
                    <li> <a href="{{ route('users.accounts.addresses.index') }}">Addresses</a> </li>

                    <li> <span class="divider">Orders</span>  </li>
                    <li> <a href="{{ route('users.orders.index') }}">My Orders</a> </li>
                    <li>
                        <a href="{{ url('m/delivery-status') }}" target="_blank">
                            <i class="fa fa-fw fa-external-link-alt"></i>
                            Track Delivery
                        </a>
                    </li>

                    @php
                        $storeLimit = 3;
                        $userStores = $user->stores()->paginate($storeLimit);
                        $userStoreTotal = $userStores->total();
                        $userHasStore = $userStoreTotal ? true : false;
                    @endphp
                    @if ($userHasStore)
                        <li> <span class="divider"><i class="fa fa-fw fa-store"></i> My Stores</span>  </li>
                        @foreach ($userStores as $store)
                            <li> <a href="{{ route('users.stores.index', $store->id) }}">{{ $store->name }}</a> </li>
                        @endforeach
                        @if ($userStoreTotal > $storeLimit)
                            <li>
                                <a href="{{ route('users.accounts.profile.index') }}#user-stores-container">See more</a>
                            </li>
                        @endif
                    @endif

                    @php
                        $marketplaceLimit = 3;
                        $userMarketplaces = $user->marketplaces()->paginate($marketplaceLimit);
                        $userMarketplaceTotal = $userMarketplaces->total();
                        $userHasMarketplace = $userMarketplaceTotal ? true : false;
                    @endphp
                    @if ($userHasMarketplace)
                        <li> <span class="divider"><i class="fa fa-fw fa-store"></i> My Marketplace</span>  </li>
                        @foreach ($userMarketplaces as $marketplace)
                            <li> <a href="{{ route('users.marketplaces.index', $marketplace->id) }}">{{ $marketplace->name }}</a> </li>
                        @endforeach
                        @if ($userMarketplaceTotal > $marketplaceLimit)
                            <li>
                                <a href="{{ route('users.accounts.profile.index') }}#user-marketplaces-container">See more</a>
                            </li>
                        @endif
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>
