<div class="row">
    <div class="col-md-8">
        <h4>
            <i class="fa fa-file-alt"></i>
            Documents
        </h4>
        <div class="box">
            <div class="box-content">
                <div class="messages">
                    <ul class="message-content">
                        @if (count($activities['documents']))
                            @foreach ($activities['documents'] as $activity)
                                @php
                                    $photo = $activity->user->photo ? asset($activity->user->photo) : '';
                                    $isOwner = false;
                                    if ($store->user_id == $activity->user_id) {
                                        $isOwner = true;
                                    }
                                @endphp
                                @if ($isOwner)
                                    <li class="sent">
                                        <img src="{{ $photo }}" alt="" title="{{ $activity->user->name }}">
                                        <div class="message-content">
                                            <p>
                                                <strong>{{ $activity->order->store_name }}</strong>
                                                uploaded
                                                <strong>{{ $activity->activity }}</strong>
                                            </p>
                                            @if ($activity->has_attachment)
                                                <a class=""
                                                    href="{{ asset($activity->attachments) }}"
                                                    download="{{ get_filename(asset($activity->attachments)) }}" target="_blank">
                                                    <i class="fa fa-download"></i>
                                                    Download
                                                </a>
                                            @endif
                                            @if ($activity->note)
                                              <p>
                                                  {{ $activity->note }}
                                              </p>
                                            @endif
                                            <span class="message-time">{{ $activity->created_at }}</span>
                                        </div>
                                    </li>
                                @endif
                                @if (!$isOwner)
                                    <li class="replies">
                                        <img src="{{ $photo }}" alt="" title="{{ $activity->user->name }}">
                                        <div class="message-content">
                                            <p>
                                                <strong>{{ $activity->user->name }}</strong>
                                                uploaded
                                                <strong>{{ $activity->activity }}</strong>
                                            </p>
                                            @if ($activity->has_attachment)
                                                <a class=""
                                                    href="{{ asset($activity->attachments) }}"
                                                    download="{{ get_filename(asset($activity->attachments)) }}" target="_blank">
                                                    <i class="fa fa-download"></i>
                                                    Download
                                                </a>
                                            @endif
                                            @if ($activity->note)
                                              <p>
                                                  {{ $activity->note }}
                                              </p>
                                            @endif
                                            <span class="message-time">{{ $activity->created_at }}</span>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        @else
                            No data
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-6">
        <h4>
            <i class="fa fa-file-alt"></i>
            Documents
        </h4>
        <div class="box">
            <div class="box-content">
                {!! Form::open([
                    'route' => ['users.stores.order-activities.documents.store', $store->id, $order->id],
                    'method' => 'POST',
                    'files' => true,
                    ]) !!}

                    <div class="hide">
                        {!! Form::bs3Text('group', 'document') !!}
                    </div>
                    {!! Form::bs3Text('activity', null, ['label'=>'Document']) !!}
                    {!! Form::bs3File('file') !!}
                    {!! Form::bs3Textarea('note', null, ['label'=>'Comment']) !!}

                    {!! Form::bs3Submit('Submit', ['class'=>'btn btn-primary btn-block']); !!}

                {!! Form::close() !!}

                <hr>

                <a href="{{ route('users.stores.orders.show.documents.bast', [$store->id, $order->id]) }}">BAST</a> |
                <a href="{{ route('users.stores.orders.show.documents.surat-jalan', [$store->id, $order->id]) }}">Surat Jalan</a> |
                <a href="{{ route('users.stores.orders.show.documents.faktur', [$store->id, $order->id]) }}">Faktur</a>
            </div>
        </div>
    </div>
</div>
