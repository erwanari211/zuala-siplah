@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Store Addresses')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Store Settings</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.settings.addresses.index', [$store->id]) }}">
                                Addresses
                            </a>
                        </li>
                    </ol>

                    <h3>Store Addresses</h3>

                    @include('flash::message')

                    @if (!$store->has_addresses)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Store dont have address. Please add address
                        </div>
                    @endif

                    @if (!$store->has_default_addresses)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Store dont have default address. Please set default address
                        </div>
                    @endif

                    <a class="btn btn-primary" href="{{ route('users.stores.settings.addresses.create', [$store->id]) }}">Add Address</a>

                    <div class="box mt-4">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Label</th>
                                            <th>Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($addresses))
                                            @php
                                                $no = $addresses->firstItem();
                                            @endphp
                                            @foreach ($addresses as $address)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('users.stores.settings.addresses.edit', [$store->id, $address->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['users.stores.settings.addresses.destroy', $store->id, $address->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}

                                                        @php
                                                            $defaultBtnClass = $address->default_address ? 'btn-success' : 'btn-default';
                                                            $defaultBtnLabel = $address->default_address ? '' : 'Set as default';
                                                        @endphp
                                                        {!! Form::open([
                                                                'route' => ['users.stores.settings.addresses.default-addresses.update', $store->id, $address->id],
                                                                'method' => 'PUT',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-check"></i> '.$defaultBtnLabel, ['class'=>'btn btn-xs '.$defaultBtnClass, 'onclick'=>'return confirm("Set as default?")', 'title'=>'Set as default address']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        {{ $address->label }}
                                                        @if ($address->default_address)
                                                            <br>
                                                            <span class="label label-success">Default address</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <strong>Contact</strong> <br>
                                                        {{ $address->full_name }} <br>
                                                        {{ $address->phone }} <br>

                                                        <strong>Address</strong> <br>
                                                        {{ $address->address }} <br>
                                                        {{ $address->postcode }} <br>
                                                        {{--
                                                        {{ $address->city->type.' '.$address->city->city_name }} <br>
                                                        {{ $address->province->province }}
                                                        --}}
                                                        @php
                                                            $fullAddress = [];
                                                            if ($address->district) {
                                                                $fullAddress[] = $address->district->name;
                                                            }
                                                            if ($address->city) {
                                                                $fullAddress[] = $address->city->name;
                                                            }
                                                            if ($address->province) {
                                                                $fullAddress[] = $address->province->name;
                                                            }
                                                        @endphp
                                                        @if ($fullAddress)
                                                            <span>
                                                                {{ implode(', ', $fullAddress) }}
                                                            </span>
                                                            <br>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $addresses->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
