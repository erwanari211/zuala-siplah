@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Store Settings')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Store Settings</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.settings.store-settings.edit', [$store->id]) }}">
                                Settings
                            </a>
                        </li>
                    </ol>

                    <h3>Store Settings</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($storeSettings, [
                                'route' => ['users.stores.settings.store-settings.update', $store->id],
                                'method' => 'PUT',
                                'files' => false,
                                ]) !!}

                                {!! Form::bs3Text('store_tagline') !!}
                                {!! Form::bs3Textarea('store_description') !!}
                                {!! Form::bs3Text('location', null, ['label'=>'Location / City']) !!}
                                {!! Form::bs3Text('order_prefix') !!}
                                {!! Form::bs3Select('display_collection_in_home_tab', $dropdown['yes_no']) !!}
                                <hr>

                                {!! Form::bs3Textarea('zone_price_message') !!}
                                <hr>

                                {!! Form::bs3Text('facebook_name') !!}
                                {!! Form::bs3Text('facebook_url') !!}
                                {!! Form::bs3Text('twitter_name') !!}
                                {!! Form::bs3Text('twitter_url') !!}
                                {!! Form::bs3Text('youtube_name') !!}
                                {!! Form::bs3Text('youtube_url') !!}
                                {!! Form::bs3Text('instagram_name') !!}
                                {!! Form::bs3Text('instagram_url') !!}
                                {!! Form::bs3Text('phone') !!}
                                {!! Form::bs3Text('mobile_phone') !!}

                                <hr>

                                {!! Form::bs3Text('whatsapp_number') !!}
                                <span class="help-block">
                                    Use: <code>15551234567</code>
                                    Don't use: <code>+001-(555)1234567</code>
                                </span>

                                <hr>

                                {!! Form::bs3Textarea('chatra_widget') !!}

                                {!! Form::bs3Submit('Update'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.index', [$store->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
