@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Store Dashboard')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            Dashboard
                        </li>
                    </ol>

                    <h3>Store Dashboard</h3>

                    @if (!$store->is_active)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Store is inactive. Waiting admin approval.
                        </div>
                    @endif

                    @if (!$store->has_addresses)
                        <div class="alert alert-danger">
                            Store dont have address. Please add address!
                        </div>
                    @endif

                    @if (!$store->has_default_addresses)
                        <div class="alert alert-danger">
                            Store dont have default address. Please set default address!
                        </div>
                    @endif

                    @if (!$store->has_shipping_methods)
                        <div class="alert alert-danger">
                            Store dont have shipping method. Please set shipping method!
                        </div>
                    @endif


                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <h4>Most Viewed Product</h4>
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs border-bottom-nav">
                            <li class="active">
                                <a href="#tab-most-products-viewed-weekly" data-toggle="tab">Weekly</a>
                            </li>
                            <li>
                                <a href="#tab-most-products-viewed-monthly" data-toggle="tab">Monthly</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in pt-4 active" id="tab-most-products-viewed-weekly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($mostViewedProductsThisWeek))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($mostViewedProductsThisWeek as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>
                                                                <img src="{{ asset($item->product->image) }}" width="50">
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $item->product->slug]) }}">
                                                                    {{ $item->product->name }}
                                                                </a>
                                                            </td>
                                                            <td>{{ $item->total_views }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade pt-4" id="tab-most-products-viewed-monthly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($mostViewedProductsThisMonth))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($mostViewedProductsThisMonth as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>
                                                                <img src="{{ asset($item->product->image) }}" width="50">
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $item->product->slug]) }}">
                                                                    {{ $item->product->name }}
                                                                </a>
                                                            </td>
                                                            <td>{{ $item->total_views }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h4>Popular Search</h4>
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs border-bottom-nav">
                            <li class="active">
                                <a href="#tab-popular-search-weekly" data-toggle="tab">Weekly</a>
                            </li>
                            <li>
                                <a href="#tab-popular-search-monthly" data-toggle="tab">Monthly</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in pt-4 active" id="tab-popular-search-weekly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Search</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($popularSearchThisWeek))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($popularSearchThisWeek as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item->search }}</td>
                                                            <td>{{ $item->total_search }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade pt-4" id="tab-popular-search-monthly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Search</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($popularSearchThisMonth))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($popularSearchThisMonth as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item->search }}</td>
                                                            <td>{{ $item->total_search }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 col-sm-6"></div>
                <div class="col-md-4 col-sm-6"></div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
