<table border="1">
    <thead>
        <tr>
            <th>no</th>
            <th>unique_code</th>
            <th>name</th>
            <th>sku</th>
            <th>price</th>

            <th>has_zone_price</th>

            <th>zone_1</th>
            <th>zone_2</th>
            <th>zone_3</th>
            <th>zone_4</th>
            <th>zone_5</th>

        </tr>
    </thead>
    <tbody>
        @if (count($products))
            @foreach($products as $product)
                <tr>
                    <td>{{ $product['index'] }}</td>

                    <td>{{ $product['unique_code'] }}</td>
                    <td>{{ $product['name'] }}</td>
                    <td>{{ $product['sku'] }}</td>
                    <td>{{ $product['price'] }}</td>

                    <td>{{ $product['has_kemdikbud_zone_price'] }}</td>

                    <td>{{ $product['price_zone_1'] }}</td>
                    <td>{{ $product['price_zone_2'] }}</td>
                    <td>{{ $product['price_zone_3'] }}</td>
                    <td>{{ $product['price_zone_4'] }}</td>
                    <td>{{ $product['price_zone_5'] }}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
