@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Import Zone Price')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">


                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.index', [$store->id]) }}">
                                Store Products
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">
                                Detail
                            </a>
                        </li>
                        <li>
                            Zone price
                        </li>
                        <li class="active">
                            Import
                        </li>
                    </ol>

                    <h3>Import Zone Price</h3>
                    @include('flash::message')
                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.stores.products.zone-prices.import.update', $store->id, $product->id],
                                'method' => 'POST',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3File('file') !!}
                                <span class="help-block">
                                    You can download excel templete by exporting products in form below
                                </span>

                                {!! Form::bs3Submit('Import'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            <div class="alert alert-info">
                                Recommended export maximal 100 products
                            </div>

                            {!! Form::open([
                                'route' => ['users.stores.products.zone-prices.import.template', $store->id, $product->id],
                                'method' => 'POST',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Number('start', 1) !!}
                                {!! Form::bs3Number('end', $totalProducts) !!}

                                {!! Form::bs3Submit('Export Product'); !!}

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Note</strong> <br>
                        To import zone price, first download template <br>
                        If product has zone price set has_zone_price to 1 <br>
                        Then file zone price 1 to 5 <br>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
