<table>
    <thead>
        <tr>
            <th>unique_code</th>
            <th>sku</th>
            <th>group</th>
            <th>name</th>
            <th>value</th>
            <th>sort_order</th>
        </tr>
    </thead>
    <tbody>
        @foreach($attributes as $attribute)
            <tr>
                <td>{{ $attribute['unique_code'] }}</td>
                <td>{{ $attribute['sku'] }}</td>
                <td>{{ $attribute['group'] }}</td>
                <td>{{ $attribute['name'] }}</td>
                <td>{{ $attribute['value'] }}</td>
                <td>{{ $attribute['sort_order'] }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
