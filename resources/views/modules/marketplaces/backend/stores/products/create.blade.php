@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Create Product')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush


@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
@endpush

@push('js')
    <script src="{{ asset('assets/plugins') }}/tinymce_4.7.9/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            /**
             * Tinymce settings
             */
            var plugins = 'preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern code help spellchecker';
            var toolbar1 = 'formatselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat';
            var toolbar2 = 'undo redo | link unlink image media styleselect | nanospell | fullscreen preview code ';

            tinymce.init({
                    selector: '#description',
                    height: 300,
                    plugins: plugins,
                    toolbar1: toolbar1,
                    toolbar2: toolbar2,
                    image_advtab: true,
                    selection_toolbar: 'bold italic | quicklink h2 h3 blockquote',
                    imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",
                    content_css: [
                      '//www.tinymce.com/css/codepen.min.css',
                      '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
                    ],

                    relative_urls : false,
                    remove_script_host : false,
                    convert_urls : true,
                    image_class_list: [
                        {title: 'Lazyload', value: 'lazy'},
                        {title: 'None', value: ''},
                    ],

                    table_default_attributes: {
                        'class': 'table table-bordered'
                    },
                    table_default_styles: {},
                    table_class_list: [
                        {title: 'None', value: ''},
                        {title: 'Table Bordered', value: 'table table-bordered'},
                    ],
                });

            $.fn.select2.defaults.set( "theme", "bootstrap" );

            $('.select2').select2({
                // placeholder: 'Please Select'
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.index', [$store->id]) }}">
                                Store Products
                            </a>
                        </li>
                        <li class="active">
                            Create
                        </li>
                    </ol>

                    <h3>Create Product</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.stores.products.store', $store->id],
                                'method' => 'POST',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('name', null, ['autofocus'=>'autofocus']) !!}
                                {!! Form::bs3Text('slug') !!}
                                {!! Form::bs3Text('sku', null, ['label'=>'SKU']) !!}
                                {!! Form::bs3Textarea('description') !!}
                                {!! Form::bs3Number('price') !!}
                                {{-- {!! Form::bs3Select('category', $dropdown['categories']) !!} --}}

                                {!! Form::bs3Select('category[]', $dropdown['marketplaceCategories'], null, [
                                    'multiple'=>'multiple', 'class'=>'form-control select2', 'label'=>'Category'
                                ]) !!}
                                {!! Form::bs3Select('collection[]', $dropdown['storeCategories'], null, [
                                    'multiple'=>'multiple', 'class'=>'form-control select2', 'label'=>'Collection'
                                ]) !!}

                                {!! Form::bs3Number('qty') !!}
                                {!! Form::bs3Number('weight', null, ['label'=>'Weight (gr)']) !!}
                                {!! Form::bs3File('image') !!}
                                <span class="help-block">Recomended size 300 x 300</span>
                                {!! Form::bs3Select('type', $dropdown['types'], 'product') !!}
                                {!! Form::bs3Select('active', $dropdown['yes_no'], 1) !!}
                                {!! Form::bs3Select('status', $dropdown['statuses'], 'active') !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.products.index', [$store->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
