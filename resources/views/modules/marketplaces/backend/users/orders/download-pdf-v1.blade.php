<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Title Page</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <style type="text/css">
            body {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            }

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4,
            .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }

            .text-key {
                font-weight: bold;
                padding-right: 50px;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <h2>INVOICE # {{ $order->invoice_prefix.$order->invoice_no }}</h2>

            <div class="row">
                <div class="col-sm-12">
                    @php
                        $shippingMethod = $orderMetas['shipping_method'];
                        $paymentMethod = $orderMetas['payment_method'];
                    @endphp
                    <table class="">
                        <tbody>
                            <tr>
                                <td class="text-key">Store</td>
                                <td>{{ $store->name }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Invoice No</td>
                                <td>{{ $order->invoice_prefix.$order->invoice_no }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Date</td>
                                <td>{{ $order->created_at }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Payment Method</td>
                                <td>{{ $paymentMethod['label'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Shipping Method</td>
                                <td>{{ $shippingMethod['name'].' '.$shippingMethod['service'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Order Note</td>
                                <td>{{ $paymentMethod['note'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <h3>Payment Address</h3>
                    @php
                        $address = $orderMetas['payment_address'];
                    @endphp
                    <table class="">
                        <tbody>
                            <tr>
                                <td class="text-key">Full Name</td>
                                <td>{{ $address['full_name'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Company</td>
                                <td>
                                    @if ($address['company'])
                                        {{ $address['company'] }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="text-key">Phone</td>
                                <td>{{ $address['phone'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Address</td>
                                <td>{{ $address['address'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">City</td>
                                <td>{{ $address['city'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Province</td>
                                <td>{{ $address['province'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Post Code</td>
                                <td>{{ $address['postcode'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <h3>Shipping Address</h3>
                    @php
                        $address = $orderMetas['shipping_address'];
                    @endphp
                    <table class="">
                        <tbody>
                            <tr>
                                <td class="text-key">Full Name</td>
                                <td>{{ $address['full_name'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Company</td>
                                <td>
                                    @if ($address['company'])
                                        {{ $address['company'] }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="text-key">Phone</td>
                                <td>{{ $address['phone'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Address</td>
                                <td>{{ $address['address'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">City</td>
                                <td>{{ $address['city'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Province</td>
                                <td>{{ $address['province'] }}</td>
                            </tr>
                            <tr>
                                <td class="text-key">Post Code</td>
                                <td>{{ $address['postcode'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <h3>Products</h3>
                    <table class="table table-hover table-bordered mb-0">
                        <thead>
                            <tr>
                                <th style="width: 50%">Product</th>
                                <th style="width: 10%" class="text-center">Qty</th>
                                <th style="width: 20%" class="text-right">Price</th>
                                <th style="width: 20%" class="text-right">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->product_name }}</td>
                                    <td class="text-center">{{ $product->qty }} </td>
                                    <td class="text-right">{{ formatNumber($product->price) }}</td>
                                    <td class="text-right">{{ formatNumber($product->total) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            @php
                                $courier = $orderMetas['shipping_method']['name'];
                                $service = $orderMetas['shipping_method']['service'];
                                $shippingCost = $orderMetas['shipping_method']['value'];
                                $subtotal = $orderMetas['order']['subtotal'];
                                $subtotal = floatvalue($subtotal);
                                $uniqueNumber = $orderMetas['payment_method']['unique_number'];
                                $total = $subtotal + $shippingCost + $uniqueNumber;

                                $paymentMethodInfo = $orderMetas['payment_method']['info'];
                            @endphp

                            <tr>
                                <td colspan="3">Subtotal</td>
                                <td class="text-right">{{ formatNumber($subtotal) }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">{{ $courier . ' ' . $service }}</td>
                                <td class="text-right">{{ formatNumber($shippingCost) }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">Unique Number</td>
                                <td class="text-right">{{ formatNumber($uniqueNumber) }}</td>
                            </tr>
                            <tr>
                                <td colspan="3"> <strong>Total</strong> </td>
                                <td class="text-right"><strong>{{ formatNumber($total) }}</strong> </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
         <script src="Hello World"></script>
    </body>
</html>
