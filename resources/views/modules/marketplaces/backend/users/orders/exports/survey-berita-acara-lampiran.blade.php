<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lampiran Berita Acara Survey</title>

  <!-- Bootstrap CSS -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> --}}

  <style type="text/css">
    body {
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 14px;
    }

    .container {
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }

    @media (min-width: 768px) {
      .container {
        width: 750px;
      }
    }

    @media (min-width: 992px) {
      .container {
        width: 970px;
      }
    }

    @media (min-width: 1200px) {
      .container {
        width: 1170px;
      }
    }

    table {
      background-color: transparent;
    }

    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: top;
      border-top: 1px solid #ddd;
    }

    .table-bordered {
      border: 1px solid #ddd;
    }

    .table-bordered>tbody>tr>td,
    .table-bordered>tbody>tr>th,
    .table-bordered>tfoot>tr>td,
    .table-bordered>tfoot>tr>th,
    .table-bordered>thead>tr>td,
    .table-bordered>thead>tr>th {
      border: 1px solid #ddd;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    .text-key {
      font-weight: bold;
      padding-right: 50px;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="container">
    <h3 class="text-center">Lembar Survey, Klarifikasi teknis dan Negosiasi Harga</h3>
    <p class="text-center">Nomor : ........................................</p>

    <table style="width: 100%; margin-bottom: 20px;">
      <tbody>
        <tr>
          <td style="width: 50%">
            <table style="width: 100%;">
              <tbody>
                <tr>
                  <td style="width:150px;">KEGIATAN</td>
                  <td style="width:10px;">:</td>
                  <td></td>
                  <td style="width:10px;"></td>
                </tr>
                <tr>
                  <td style="width:150px;">PEKERJAAN</td>
                  <td style="width:10px;">:</td>
                  <td></td>
                  <td style="width:10px;"></td>
                </tr>
                <tr>
                  <td style="width:150px;">NOMOR DPA</td>
                  <td style="width:10px;">:</td>
                  <td></td>
                  <td style="width:10px;"></td>
                </tr>
                <tr>
                  <td style="width:150px;">KODE REKENING</td>
                  <td style="width:10px;">:</td>
                  <td></td>
                  <td style="width:10px;"></td>
                </tr>
                <tr>
                  <td style="width:150px;">PAGU DANA</td>
                  <td style="width:10px;">:</td>
                  <td>{{ formatNumber($order->total) }}</td>
                  <td style="width:10px;"></td>
                </tr>
                <tr>
                  <td style="width:150px;">HPS</td>
                  <td style="width:10px;">:</td>
                  <td>{{ formatNumber($order->total) }}</td>
                  <td style="width:10px;"></td>
                </tr>
              </tbody>
            </table>
          </td>
          <td style="width: 50%">
            <table style="width: 100%;">
              <tbody>
                <tr>
                  <td style="width:200px; vertical-align: top;">SK PENGADAAN</td>
                  <td style="width:10px; vertical-align: top;">:</td>
                  <td></td>
                  <td style="width:10px;"></td>
                </tr>
                <tr>
                  <td style="width:200px; vertical-align: top;">Tanggal</td>
                  <td style="width:10px; vertical-align: top;">:</td>
                  <td></td>
                  <td style="width:10px;"></td>
                </tr>
                <tr>
                  <td style="width:200px; vertical-align: top;">NAMA CALON PENYEDIA</td>
                  <td style="width:10px; vertical-align: top;">:</td>
                  <td>{{ $store->name }}</td>
                  <td style="width:10px;"></td>
                </tr>
                <tr>
                  @php
                  $address = $storeAddress;
                  @endphp
                  <td style="width:200px; vertical-align: top;">ALAMAT</td>
                  <td style="width:10px; vertical-align: top;">:</td>
                  <td>
                    {{ $address['address'] }} - {{ $address['postcode'] }},
                    {{ ucwords(strtolower($address['city']['name'])) }},
                    {{ ucwords(strtolower($address['province']['name'])) }}
                  </td>
                  <td style="width:10px;"></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>

    <div style="width: 100%; margin-bottom: 20px;">
      <table class="table table-bordered" style="width: 100%; border: 1px solid #ddd; table-layout:fixed;">
        <thead>
          <tr>
            <th style="vertical-align:middle; width: 5%;"
              rowspan="2"
              class="text-center">
              No
            </th>
            <th style="vertical-align:middle; width: 15%;"
              rowspan="2"
              class="text-center">
              Jenis Pekerjaan / <br>
              Barang / Jasa
            </th>
            <th style="vertical-align:middle; width: 10%;"
              rowspan="2"
              class="text-center">
              Spesifikasi
            </th>
            <th style="vertical-align:middle; width: 15%;"
              colspan="2"
              class="text-center">
              Volume
            </th>
            <th style="vertical-align:middle; width: 15%;"
              colspan="2"
              class="text-center">
              Survey
            </th>
            <th style="vertical-align:middle; width: 30%;"
              colspan="3"
              class="text-center">
              Klarifikasi dan Negosiasi
            </th>
            <th style="vertical-align:middle; width: 10%;"
              rowspan="2"
              class="text-center">
              Keterangan
            </th>
          </tr>
          <tr>
            <th style="vertical-align:middle; width: 7%;"
              class="text-center">
              Jumlah
            </th>
            <th style="vertical-align:middle; width: 8%;"
              class="text-center">
              Satuan
            </th>

            <th style="vertical-align:middle; width: 7%;"
              class="text-center">
              Harga Satuan
            </th>
            <th style="vertical-align:middle; width: 8%;"
              class="text-center">
              Total Harga
            </th>

            <th style="vertical-align:middle; width: 10%;"
              class="text-center">
              Spesifikasi
            </th>
            <th style="vertical-align:middle; width: 10%;"
              class="text-center">
              Harga Satuan
            </th>
            <th style="vertical-align:middle; width: 10%;"
              class="text-center">
              Total Harga
            </th>
          </tr>
        </thead>
        <tbody>
          @php
            $no = 1;
          @endphp
          @foreach ($products as $product)
            <tr>
              <td style="vertical-align: top;" class="text-center">{{ $no }}</td>
              <td style="vertical-align: top;">{{ $product->product_name }}</td>
              <td style="vertical-align: top; height: 80px;"></td>
              <td style="vertical-align: top;" class="text-center">{{ $product->qty }} </td>
              <td style="vertical-align: top;"></td>
              <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->price) }}</td>
              <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->total) }}</td>
              <td style="vertical-align: top; height: 80px;"></td>
              <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->price) }}</td>
              <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->total) }}</td>
              <td style="vertical-align: top;"></td>
            </tr>
            @php
              $no++;
            @endphp
          @endforeach
          <tr>
            <td colspan="3">
              <strong>TOTAL</strong>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-right">
              <strong>{{ formatNumber($products->sum('total')) }}</strong>
            </td>
            <td></td>
            <td></td>
            <td class="text-right">
              <strong>{{ formatNumber($products->sum('total')) }}</strong>
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>


    <p>Catatan: harga tersebut diatas sudah termasuk pajak</p>

    <div style="page-break-inside: avoid;">
      <table style="width: 100%">
        <tbody>
          <tr>
            <td style="width: 25%" class="text-center">Mengetahui</td>
            <td style="width: 25%"></td>
            <td style="width: 25%"></td>
            <td style="width: 25%" class="text-center">.................... , ...... .................. ..........</td>
          </tr>
          <tr>
            <td style="width: 25%" class="text-center">{{ $store->name }}</td>
            <td style="width: 25%"></td>
            <td style="width: 25%"></td>
            <td style="width: 25%;" class="text-center">Pejabat Pengadaan</td>
          </tr>
          <tr>
            <td style="width: 25%;height: 60px;" class="text-center"></td>
            <td style="width: 25%"></td>
            <td style="width: 25%"></td>
            <td style="width: 25%;height: 60px;" class="text-center"></td>
          </tr>
          <tr>
            <td style="width: 25%" class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
            <td style="width: 25%"></td>
            <td style="width: 25%"></td>
            <td style="width: 25%;" class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
          </tr>
          <tr>
            <td style="width: 25%" class="text-center">Pimpinan</td>
            <td style="width: 25%"></td>
            <td style="width: 25%"></td>
            <td style="width: 25%" class="text-center">NIP. .....................................</td>
          </tr>
        </tbody>
      </table>
    </div>

    <table style="margin-top: 20px; width: 100%;">
      <tbody>
        <tr>
          <td width="25%"></td>
          <td width="25%"></td>
          <td width="25%"></td>
          <td width="25%"></td>
        </tr>
        <tr>
          <td colspan="4">
            <div style="border-left: 3px solid #ccc; padding-left: 10px;">
              Melalui Marketplace <br>
              {{ config('app.name') }}
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</body>
</html>
