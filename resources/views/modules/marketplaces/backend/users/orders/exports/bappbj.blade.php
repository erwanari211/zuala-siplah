<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BAPPBJ</title>

  <!-- Bootstrap CSS -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> --}}

  <style type="text/css">
    body {
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 14px;
    }

    .container {
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }

    @media (min-width: 768px) {
      .container {
        width: 750px;
      }
    }

    @media (min-width: 992px) {
      .container {
        width: 970px;
      }
    }

    @media (min-width: 1200px) {
      .container {
        width: 1170px;
      }
    }

    table {
      background-color: transparent;
    }

    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: top;
      border-top: 1px solid #ddd;
    }

    .table-bordered {
      border: 1px solid #ddd;
    }

    .table-bordered>tbody>tr>td,
    .table-bordered>tbody>tr>th,
    .table-bordered>tfoot>tr>td,
    .table-bordered>tfoot>tr>th,
    .table-bordered>thead>tr>td,
    .table-bordered>thead>tr>th {
      border: 1px solid #ddd;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    .text-key {
      font-weight: bold;
      padding-right: 50px;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="container">
    <h3 class="text-center">BERITA ACARA PEMERIKSAAN DAN PENERIMAAN BARANG DAN JASA</h3>
    <p class="text-center">Nomor : ........................................</p>

    <div>
      <p>
        Pada hari ini ................
        tanggal .................... .......... bulan .................... tahun ..........
        bertempat di {{ $order->user_name }} berdasarkan ........................................
        ........................................ ........................................
        ........................................ ........................................
        ........................................ ........................................
        yang bertanda tangan di bawah ini :
      </p>
    </div>

    <table class="table table-bordered">
      <tbody>
        <tr>
          <th class="text-center">Nama</th>
          <th class="text-center">NIP</th>
          <th class="text-center">Jabatan</th>
        </tr>
        <tr>
          <td style="height: 80px;"></td>
          <td style="height: 80px;"></td>
          <td style="height: 80px;"></td>
        </tr>
      </tbody>
    </table>

    <p>
      Masing-masing karena jabatannya, dengan ini menyatakan dengan sebenarnya
      telah melaksanakan pemeriksaan terhadap
      penyerahan barang pengadaan
      ........................................ ........................................
    </p>

    <table style="width: 100%; margin-bottom: 20px;">
      <tbody>
        <tr>
          <td style="width: 50px;">&nbsp;</td>
          <td style="width: 150px;">Nama Perusahaan</td>
          <td style="width: 10px;">:</td>
          <td style="">{{ $store->name }}</td>
          <td style="width: 50px;"></td>
        </tr>
        <tr>
          @php
            $address = $storeAddress;
          @endphp
          <td style="width: 50px;">&nbsp;</td>
          <td style="width: 150px;">Alamat</td>
          <td style="width: 10px;">:</td>
          <td style="">
            {{ $address['address'] }} - {{ $address['postcode'] }},
            {{ ucwords(strtolower($address['city']['name'])) }},
            {{ ucwords(strtolower($address['province']['name'])) }}
          </td>
          <td style="width: 50px;"></td>
        </tr>
      </tbody>
    </table>

    <p>
      Sesuai Surat pemesanan .................... .................... tanggal ..............................
      sebesar {{ formatNumber($products->sum('total')) }}
    </p>

    <div>
      Hasil pemeriksaan : <br>
      <div style="padding-left: 50px;">
        <span>1. Spesifikasi barang sesuai</span> <br>
        <span>2. Barang dalam kondisi baik</span> <br>
      </div>
    </div>

    <p>
      Demikian Berita Acara ini dibuat dalam rangkap 5 (lima) untuk dipergunakan sebagaimana mestinya
    </p>

    <div style="page-break-inside: avoid;">
      <table style="width: 100%">
        <tbody>
          <tr>
            <td style="width: 35%" class="text-center">&nbsp;</td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%" class="text-center">
              .................... , ...... .................. ..........
            </td>
          </tr>
          <tr>
            <td style="width: 35%" class="text-center">Penyedia Barang</td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%" class="text-center">
              Pejabat penerima Hasil Pekerjaan
            </td>
          </tr>
          <tr>
            <td style="width: 35%" class="text-center">{{ $store->name }}</td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%;" class="text-center"></td>
          </tr>
          <tr>
            <td style="width: 35%;height: 60px;" class="text-center"></td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%;height: 60px;" class="text-center"></td>
          </tr>
          <tr>
            <td style="width: 35%" class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%;" class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
          </tr>
          <tr>
            <td style="width: 35%" class="text-center">Pimpinan</td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%" class="text-center">NIP. .....................................</td>
          </tr>
        </tbody>
      </table>
    </div>

    <div style="page-break-inside: avoid;">
      <table style="margin-top: 20px; width: 100%;">
        <tbody>
          <tr>
            <td width="25%"></td>
            <td width="25%"></td>
            <td width="25%"></td>
            <td width="25%"></td>
          </tr>
          <tr>
            <td colspan="4">
              <div style="border-left: 3px solid #ccc; padding-left: 10px;">
                Melalui Marketplace <br>
                {{ config('app.name') }}
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>
