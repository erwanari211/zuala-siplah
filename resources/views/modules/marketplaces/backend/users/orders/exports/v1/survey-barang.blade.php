<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Survey Barang</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <style type="text/css">
            body {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            }

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4,
            .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }

            .text-key {
                font-weight: bold;
                padding-right: 50px;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">

            <h3 class="text-center">HARGA BARANG DARI SUMBER INFORMASI</h3>

            <table style="width: 100%; margin-bottom: 20px;">
                <tbody>
                    <tr>
                        <td style="width: 160px;">Nama Kegiatan</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 160px;">Nama Pekerjaan</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 160px;">Sumber Dana</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-hover table-bordered mb-0">
                <thead>
                    <tr>
                        <th style="vertical-align:middle; width: 8%"
                            class="text-center">
                            No
                        </th>
                        <th style="vertical-align:middle; width: 30%"
                            class="text-center">
                            Nama Produk
                        </th>
                        <th style="vertical-align:middle; width: 32%"
                            class="text-center">
                            Spesifikasi Barang
                        </th>
                        <th style="vertical-align:middle; width: 15%"
                            class="text-center">
                            Harga Penawaran <br> (Rp)
                        </th>
                        <th style="vertical-align:middle; width: 15%"
                            class="text-center">
                            Keterangan
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($products as $product)
                        <tr>
                            <td style="vertical-align: top;" class="text-center">{{ $no }}</td>
                            <td style="vertical-align: top;">{{ $product->product_name }}</td>
                            <td style="vertical-align: top;height: 80px;">&nbsp;</td>
                            <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->price) }}</td>
                            <td style="vertical-align: top;" class="text-right"></td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                    @endforeach
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                </tbody>
            </table>

            <div style="margin-bottom: 20px;">
                <span>Catatan :</span> <br>
                <div style="padding-left: 40px;">
                    <span>1. Harga tersebut diatas belum / sudah termasuk pengiriman sampai ditempat *</span> <br>
                    <span>2. Harga tersebut sudah termasuk pajak-pajak</span> <br>
                </div>
            </div>

            <table style="width: 100%; margin-bottom: 20px;">
                @php
                    $address = $storeAddress;
                @endphp
                <tbody>
                    <tr>
                        <td style="width: 150px;">Nama Perusahaan</td>
                        <td style="width: 10px;">:</td>
                        <td>{{ $store->name }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">Alamat Perusahaan</td>
                        <td style="width: 10px;">:</td>
                        <td>
                            {{ $address['address'] }} - {{ $address['postcode'] }},
                            {{ ucwords(strtolower($address['city']['name'])) }},
                            {{ ucwords(strtolower($address['province']['name'])) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">Nama Pimpinan</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <div style="page-break-inside: avoid;">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td style="width: 67%"></td>
                            <td style="width: 33%" class="text-center">...................., .... .................. ......</td>
                        </tr>
                        <tr>
                            <td style="width: 67%"></td>
                            <td style="width: 33%;" class="text-center">{{ $store->name }}</td>
                        </tr>
                        <tr>
                            <td style="width: 67%"></td>
                            <td style="width: 33%;height: 60px;" class="text-center"></td>
                        </tr>
                        <tr>
                            <td style="width: 67%"></td>
                            <td style="width: 33%;" class="text-center">
                                <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                                    &nbsp;
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 67%"></td>
                            <td style="width: 33%" class="text-center">Pimpinan</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <table style="margin-top: 20px; width: 100%;">
                <tbody>
                    <tr>
                        <td width="25%"></td>
                        <td width="25%"></td>
                        <td width="25%"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div style="border-left: 3px solid #ccc; padding-left: 10px;">
                                Melalui Marketplace <br>
                                {{ config('app.name') }}
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
         <script src="Hello World"></script>
    </body>
</html>
