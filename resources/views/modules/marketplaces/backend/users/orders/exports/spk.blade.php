<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Surat Perintah Kerja</title>

  <!-- Bootstrap CSS -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> --}}

  <style type="text/css">
    body {
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 14px;
    }

    .container {
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }

    @media (min-width: 768px) {
      .container {
        width: 750px;
      }
    }

    @media (min-width: 992px) {
      .container {
        width: 970px;
      }
    }

    @media (min-width: 1200px) {
      .container {
        width: 1170px;
      }
    }

    table {
      background-color: transparent;
    }

    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: top;
      border-top: 1px solid #ddd;
    }

    .table-bordered {
      border: 1px solid #ddd;
    }

    .table-bordered>tbody>tr>td,
    .table-bordered>tbody>tr>th,
    .table-bordered>tfoot>tr>td,
    .table-bordered>tfoot>tr>th,
    .table-bordered>thead>tr>td,
    .table-bordered>thead>tr>th {
      border: 1px solid #ddd;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    .text-key {
      font-weight: bold;
      padding-right: 50px;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="container">
    <table border="1" style="width: 100%; margin-bottom: 10px;">
      <tbody>
        <tr>
          <td style="width: 50%; vertical-align: middle;"
            rowspan="2"
            class="text-center">
            <strong>SURAT PERINTAH KERJA (SPK) </strong>
          </td>
          <td style="width: 50%; vertical-align: top;"
            class="text-center">
            <strong>SATUAN KERJA PERANGKAT DAERAH </strong>
          </td>
        </tr>
        <tr>
          <td style="width: 50%; vertical-align: top;"
            rowspan="3"
            class="text-center">
            <strong>NOMOR DAN TANGGAL SPK</strong> <br>
            &nbsp; <br>
            &nbsp; <br>
            Tanggal : ..............................
          </td>
        </tr>
        <tr>
          <td style="width: 50%; vertical-align: top;"
            class="text-center">
            &nbsp;
          </td>
        </tr>
        <tr>
          <td style="width: 50%; vertical-align: top;"
            rowspan="5"
            class="text-center">
            &nbsp;
          </td>
        </tr>
        <tr>
          <td style="width: 50%; vertical-align: top;"
            rowspan="3"
            class="text-center">
            <strong>NOMOR DAN TANGGAL SURVEY, KLARIFIKASI DAN NEGOSIASI</strong> <br>
            &nbsp; <br>
            &nbsp; <br>
            Tanggal : ..............................
          </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
          <td style="width: 50%; vertical-align: top; height: 80px;"
            class="text-center">
            &nbsp;
          </td>
        </tr>
        <tr>
          <td style="width: 50%; vertical-align: top;"
            colspan="2"
            class="">
            <strong>SUMBER DANA :</strong>
          </td>
        </tr>
        <tr>
          <td style="width: 50%; vertical-align: top;"
            colspan="2"
            class="">
            <strong>WAKTU PELAKSANAAN PEKERJAAN :</strong>
          </td>
        </tr>
      </tbody>
    </table>

    <div style="border: 1px solid black;" class="text-center">
      <strong>NILAI PEKERJAAN</strong>
    </div>

    <div style="width: 100%; margin-bottom: 10px;">
      <table border="1" style="width: 100%; table-layout:fixed;">
        <thead>
          <tr>
            <th style="vertical-align:middle; width: 5%;"
              class="text-center">
              No
            </th>
            <th style="vertical-align:middle; width: 45%;"
              class="text-center">
              Uraian Pekerjaan
            </th>
            <th style="vertical-align:middle; width: 10%;"
              class="text-center">
              Kuantitas
            </th>
            <th style="vertical-align:middle; width: 10%;"
              class="text-center">
              Satuan
            </th>
            <th style="vertical-align:middle; width: 15%;"
              class="text-center">
              Harga Satuan (Rp)
            </th>
            <th style="vertical-align:middle; width: 15%;"
              class="text-center">
              Sub Total (Rp)
            </th>
          </tr>
        </thead>
        <tbody>
          @php
            $no = 1;
          @endphp
          @foreach ($products as $product)
            <tr>
              <td style="vertical-align: top;" class="text-center">{{ $no }}</td>
              <td style="vertical-align: top;">{{ $product->product_name }}</td>
              <td style="vertical-align: top;" class="text-center">{{ $product->qty }} </td>
              <td style="vertical-align: top;"></td>
              <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->price) }}</td>
              <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->total) }}</td>
            </tr>
            @php
              $no++;
            @endphp
          @endforeach
          <tr>
            <td colspan="2">
              <strong>Jumlah</strong>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-right">
              <strong>{{ formatNumber($products->sum('total')) }}</strong>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div style="border: 1px solid black; margin-bottom: 10px;">
      <p style="margin: 10px 0">
        <strong>Terbilang :</strong>
      </p>

      <p>
        <strong>Harga tersebut diatas sudah termasuk pajak</strong>
      </p>

      <p>
        <span style="text-decoration: underline;">
          INSTRUKSI KEPADA PENYEDIA :
        </span>
      </p>

      <p style="padding-right: 20px;">
        Penagihan hanya dapat dilakukan setelah penyelesaian pekerjaan yang diperintahkan dalam SPK ini
        dan dibuktikan dengan Berita Acara Serah Terima.
        Jika pekerjaan tidak dapat diselesaikan dalam jangka waktu pelaksanaan pekerjaan
        karena kesalahan atau kelalaian penyedia
        maka penyedia berkewajiban untuk membayar denda kepada KPK sebesar 1/1000 (satu perseribu)
        dari nilai SPK sebelum PPN setiap hari kalender keterlambatan.
        Selain tunduk kepada ketentuan dalam SPK ini,
        penyedia berkewajiban untuk memenuhi Standar Ketentuan dan Syarat Umum SPK terlampir.
      </p>
    </div>

    <div style="page-break-inside: avoid;">
      <table style="width: 100%" border="1">
        <tbody>
          <tr>
            <td style="width: 60%">
              <div style="padding: 10px 20px;">
                Untuk dan atas nama Dinas Pendidikan <br>
                Kuasa Pengguna Anggaran Selaku <br>
                Pejabat Pembuat Komitmen <br>
                <br>
                <br>
                <br>
                <br>
                <div style="width: 40%; border-bottom: 1px solid black;">&nbsp;</div>
                NIP. ..............................
              </div>
            </td>
            <td style="width: 40%">
              <div class="text-center" style="padding: 10px 20px;">
                Untuk dan atas nama <br>
                {{ $store->name }} <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div style="width: 50%; border-bottom: 1px solid black; margin: 0 auto;">&nbsp;</div>
                Pimpinan
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <table style="margin-top: 20px; width: 100%;">
      <tbody>
        <tr>
          <td width="25%"></td>
          <td width="25%"></td>
          <td width="25%"></td>
          <td width="25%"></td>
        </tr>
        <tr>
          <td colspan="4">
            <div style="border-left: 3px solid #ccc; padding-left: 10px;">
              Melalui Marketplace <br>
              {{ config('app.name') }}
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</body>
</html>
