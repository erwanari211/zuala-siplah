@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Banners')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-banner-id]').on('click', function(event) {
                event.preventDefault();
                var bannerId = $(this).attr('data-banner-id');
                $('#banner_id').val(bannerId);
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            <a href="{{ route('users.marketplaces.banners.index', [$marketplace->id]) }}">Banners</a>
                        </li>
                    </ol>

                    <h3>Banners</h3>

                    @include('flash::message')
                    @include('themes.marika-natsuki.messages')

                    <a class="btn btn-primary"
                        href="{{ route('users.marketplaces.banners.create', [$marketplace->id]) }}">
                        Add Banner
                    </a>
                    <a class="btn btn-default"
                        href="{{ route('users.marketplaces.banner-groups.index', [$marketplace->id]) }}">
                        View Banner Groups
                    </a>

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Link</th>
                                            <th>Active</th>
                                            <th>Expired At</th>
                                            <th>Note</th>
                                            <th>Sort Order</th>
                                            <th>Groups</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($banners))
                                            @php
                                                $no = $banners->firstItem();
                                            @endphp
                                            @foreach ($banners as $banner)
                                                @php
                                                    $isExpired = false;
                                                    $now = date("Y-m-d");
                                                    if ($now > $banner->expired_at) {
                                                        $isExpired = true;
                                                    }
                                                    $expiredClass = 'btn btn-xs ';
                                                    $expiredClass .= ($isExpired ? ' btn-danger' : ' btn-success');
                                                @endphp
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-default"
                                                            data-toggle="modal"
                                                            data-banner-id="{{ $banner->id }}"
                                                            href="#modal-add-to-banner-group" title="Add to group">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('users.marketplaces.banners.edit', [$marketplace->id, $banner->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['users.marketplaces.banners.destroy', $marketplace->id, $banner->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        <img class="lazy" src="{{ asset($banner->image) }}" width="150" height="50">
                                                    </td>
                                                    <td>
                                                        @if ($banner->link)
                                                            <a href="{{ $banner->link }}">Yes</a>
                                                        @else
                                                            No
                                                        @endif
                                                    </td>
                                                    <td>{{ $banner->active ? 'Yes' : 'No' }}</td>
                                                    <td>
                                                        <span class="{{ $expiredClass }}">
                                                            {{ date('Y-m-d', strtotime($banner->expired_at)) }}
                                                        </span>
                                                    </td>
                                                    <td>{{ $banner->note }}</td>
                                                    <td>{{ $banner->sort_order }}</td>
                                                    <td>
                                                        @if (count($banner->marketplaceBannerGroupDetails))
                                                            @foreach ($banner->marketplaceBannerGroupDetails as $detail)
                                                                <a class="btn btn-xs btn-default"
                                                                    href="{{ route('users.marketplaces.banner-groups.show', [$marketplace->id, $detail->marketplaceBannerGroup->id]) }}">
                                                                    {{ $detail->marketplaceBannerGroup->name }}
                                                                </a>
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $banners->links() }}
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal-add-to-banner-group">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add to group</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open([
                        'route' => ['users.marketplaces.banner-group-details.store', $marketplace->id],
                        'method' => 'POST',
                        'files' => true,
                        ]) !!}


                        <div class="hide">
                            {!! Form::bs3Text('banner_id') !!}
                        </div>
                        {!! Form::bs3Select('group_id', $dropdown['banner_groups']) !!}
                        @if (!count($dropdown['banner_groups']))
                            <span class="help-block">
                                <strong>Please create banner group</strong>
                            </span>
                        @endif

                        {!! Form::bs3Number('sort_order', 1) !!}

                        {!! Form::bs3Submit('Save'); !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
