@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Edit Marketplace')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-action="remove-logo"]').on('click', function(event) {
                event.preventDefault();
                console.log('clicked');
                $('#remove_logo').val(1);
                $('#form-update-marketplace').submit();
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li class="active">Marketplace Profile</li>
                    </ol>

                    <h3>Edit Marketplace</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($marketplace, [
                                'route' => ['users.marketplaces.update', $marketplace->id],
                                'method' => 'PUT',
                                'files' => true,
                                'id' => 'form-update-marketplace'
                                ]) !!}

                                {!! Form::bs3Text('name') !!}
                                {!! Form::bs3Text('slug') !!}
                                {!! Form::bs3File('image') !!}
                                @if ($marketplace->image)
                                    <div class="form-group ">
                                        <img class="img-thumbnail" src="{{ asset($marketplace->image) }}" width="100" height="100">
                                    </div>
                                @endif
                                <span class="help-block">Recomended size 600 x 600, type PNG or have white background</span>
                                {!! Form::bs3File('logo') !!}
                                @if ($marketplace->logo)
                                    <div class="form-group ">
                                        <img class="img-thumbnail" src="{{ asset($marketplace->logo) }}" width="100" height="100">
                                    </div>

                                    <a class="btn btn-danger btn-xs" href="#" data-action="remove-logo">
                                        <i class="fa fa-times"></i> Remove Logo
                                    </a>
                                    <div class="hide">
                                        {!! Form::bs3Text('remove_logo', 0) !!}
                                    </div>
                                @endif
                                <span class="help-block">Recomended size 600 x 150, type PNG or have white background</span>

                                {!! Form::bs3Submit('Update'); !!}
                                <a class="btn btn-default" href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>



                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
