@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Show Banner Group')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.fn.select2.defaults.set( "theme", "bootstrap" );

            $('.select2').select2();

            $('[data-store-id]').on('click', function(event) {
                event.preventDefault();
                var storeId = $(this).attr('data-store-id');
                var sortOrder = $(this).attr('data-sort-order');
                console.log(storeId)
                console.log(sortOrder)
                $('#modal-add-to-marketplace-collection').find('#store').val(storeId).trigger('change');
                $('#modal-add-to-marketplace-collection').find('#sort_order').val(sortOrder);
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.marketplaces.store-collections.index', [$marketplace->id]) }}">Store Collections</a>
                        </li>
                        <li class="active">Detail</li>
                    </ol>

                    <h3>Show Banner Group</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($storeCollection, [
                                'route' => ['users.marketplaces.store-collections.update', $marketplace->id, $storeCollection->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('name') !!}
                                {!! Form::bs3Text('display_name') !!}
                                {!! Form::bs3Select('active', $dropdown['yes_no']) !!}
                                {{-- {!! Form::bs3Select('type', $dropdown['types']) !!} --}}
                                {!! Form::bs3Number('sort_order') !!}

                                <a class="btn btn-default" href="{{ route('users.marketplaces.store-collections.index', [$marketplace->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <h3>Details</h3>

                    <div class="box">
                        <div class="box-content">
                            <div class="mb-4">
                                <a class="btn btn-primary"
                                    data-toggle="modal"
                                    data-store-id=""
                                    data-sort-order="1"
                                    href="#modal-add-to-marketplace-collection" title="Add to collection">
                                    Add Store
                                </a>
                            </div>
                            
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered mb-0">
                                    <thead>
                                        <tr>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Store Name</th>
                                            <th>Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($details))
                                            @foreach ($details as $detail)
                                                @php
                                                    $store = $detail->store;
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            data-toggle="modal"
                                                            data-store-id="{{ $store->id }}"
                                                            data-sort-order="{{ $detail->sort_order }}"
                                                            href="#modal-add-to-marketplace-collection" title="Add to collection">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['users.marketplaces.store-collection-details.destroy', $marketplace->id, $storeCollection->id, $store->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        <img class="lazy" src="{{ asset($store->image) }}" height="50">
                                                    </td>
                                                    <td>{{ $store->name }}</td>
                                                    <td>{{ $detail->sort_order }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div> 
                           
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>


    <div class="modal fade" id="modal-add-to-marketplace-collection">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add to group</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open([
                        'route' => ['users.marketplaces.store-collection-details.store', $marketplace->id],
                        'method' => 'POST',
                        'files' => true,
                        ]) !!}

                        <div class="hide">
                            {!! Form::bs3Text('collection_id', $storeCollection->id) !!}
                        </div>
                        {!! Form::bs3Select('store', $dropdown['stores'], null, [
                            'class'=>'form-control select2'
                        ]) !!}
                        {!! Form::bs3Number('sort_order') !!}

                        {!! Form::bs3Submit('Save'); !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
