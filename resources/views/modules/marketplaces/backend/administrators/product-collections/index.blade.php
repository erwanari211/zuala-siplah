@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Product Collections')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.administrators.backend.inc.navbar-top-administrator')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.product-collections.index') }}">Product Collections</a>
                        </li>
                    </ol>

                    <h3>Product Collections</h3>

                    @include('flash::message')

                    <a class="btn btn-primary"
                        href="{{ route('administrator.product-collections.create') }}">
                        Add Collection
                    </a>
                    <a class="btn btn-default"
                        href="{{ route('administrator.product-collections.products.index') }}">
                        Add product to collection
                    </a>

                    {!! Form::open([
                            'route' => ['administrator.website.cms.forget-caches'],
                            'method' => 'DELETE',
                            'style' => 'display: inline-block;'
                        ]) !!}
                        {!! Form::bs3SubmitHtml('Clear Caches', ['class'=>'btn btn-danger', 'onclick'=>'return confirm("Clear?")']); !!}
                    {!! Form::close() !!}

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Active</th>
                                            <th>Type</th>
                                            <th>Sort Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($productCollections))
                                            @php
                                                $no = $productCollections->firstItem();
                                            @endphp
                                            @foreach ($productCollections as $collection)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-default"
                                                            href="{{ route('administrator.product-collections.show', [$collection->id]) }}" title="View">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('administrator.product-collections.edit', [ $collection->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['administrator.product-collections.destroy', $collection->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>{{ $collection->id }}</td>
                                                    <td>
                                                        {{ $collection->name }}
                                                        @if ($collection->display_name)
                                                            <br>
                                                            ({{ $collection->display_name }})
                                                        @endif
                                                    </td>
                                                    <td>{{ $collection->active ? 'Yes' : 'No' }}</td>
                                                    <td>{{ $collection->type }}</td>
                                                    <td>{{ $collection->sort_order }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $productCollections->links() }}
                        </div>
                    </div>



                </div>
            </div>
        </div>


    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
