@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Marketplaces')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-marketplace-id]').on('click', function(event) {
                event.preventDefault();
                var marketplaceId = $(this).attr('data-marketplace-id');
                $('#marketplace_id').val(marketplaceId);
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.administrators.backend.inc.navbar-top-administrator')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li class="active">
                            <a href="{{ route('administrator.marketplaces.index') }}">Marketplace</a>
                        </li>
                        <li class="active">
                            Approve
                        </li>
                    </ol>

                    <h3>Marketplaces</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Owner</th>
                                            <th>Active</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($marketplaces))
                                            @php
                                                $no = $marketplaces->firstItem();
                                            @endphp
                                            @foreach ($marketplaces as $marketplace)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-default"
                                                            data-toggle="modal"
                                                            data-marketplace-id="{{ $marketplace->id }}"
                                                            href="#modal-add-to-marketplace-collection" title="Add to collection">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                        @if (!$marketplace->active)
                                                            {!! Form::open([
                                                                    'route' => ['administrator.marketplaces.active.store', $marketplace->id],
                                                                    'method' => 'POST',
                                                                    'style' => 'display: inline-block;'
                                                                ]) !!}
                                                                {!! Form::bs3SubmitHtml('<i class="fa fa-check"></i> Approve', ['class'=>'btn btn-xs btn-success', 'onclick'=>'return confirm("Approve?")']); !!}
                                                            {!! Form::close() !!}
                                                        @else
                                                            {!! Form::open([
                                                                    'route' => ['administrator.marketplaces.active.destroy', $marketplace->id],
                                                                    'method' => 'DELETE',
                                                                    'style' => 'display: inline-block;'
                                                                ]) !!}
                                                                {!! Form::bs3SubmitHtml('<i class="fa fa-times"></i> Disapprove', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Disapprove?")']); !!}
                                                            {!! Form::close() !!}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <img src="{{ asset($marketplace->image) }}" width="50" height="50">
                                                    </td>
                                                    <td>{{ $marketplace->name }}</td>
                                                    <td>
                                                        {{ $marketplace->user->name }}
                                                    </td>
                                                    <td>{{ $marketplace->active ? 'Yes' : 'No' }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $marketplaces->appends(request()->only(['filter']))->links() }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-add-to-marketplace-collection">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add to Collection</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open([
                        'route' => ['administrator.marketplace-collection-details.store'],
                        'method' => 'POST',
                        'files' => true,
                        ]) !!}


                        <div class="hide">
                            {!! Form::bs3Text('marketplace_id') !!}
                        </div>
                        {!! Form::bs3Select('collection_id', $dropdown['marketplace_collections']) !!}
                        @if (!count($dropdown['marketplace_collections']))
                            <span class="help-block">
                                <strong>Please create banner group</strong>
                            </span>
                        @endif

                        {!! Form::bs3Number('sort_order', 1) !!}

                        {!! Form::bs3Submit('Save'); !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
