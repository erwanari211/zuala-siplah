<ul class="dropdown-menu">
    <li>
        <div class="p-3">
            <form action="{{ route('marketplaces.categories.show', [$marketplace->slug, $category->slug]) }}" method="GET" role="form"
                id="navbar-search-form">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" value="{{ request('search') }}" placeholder="Search product from category">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div><!-- /input-group -->
            </form>
            <div class="mt-3 popular-search">
                @if (isset($popularSearches) && count($popularSearches))
                    @foreach ($popularSearches as $item)
                        <span class="btn btn-xs btn-default mr-4" data-popular-search="{{ $item->search }}">
                            {{ $item->search }}
                        </span>
                    @endforeach
                @endif
            </div>
        </div>
    </li>
</ul>
