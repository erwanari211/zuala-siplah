<ul class="dropdown-menu">
    <li>
        <div class="p-3">
            <form action="{{ route('products.index', [$marketplace->slug, $store->slug]) }}" method="GET" role="form">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" value="{{ request('search') }}" placeholder="Search product from store">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div><!-- /input-group -->
            </form>
        </div>
    </li>
</ul>
