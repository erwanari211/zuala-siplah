<div class="sidenav sidenav-overlay sidenav-overlay--left" id="marketplace-category-sidebar">
    <div class="sidenav-overlay-toolbar">
        <button class="btn btn-default btn-xs sidenav-overlay-close"
            data-action="close-sidenav"
            data-target="#marketplace-category-sidebar">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidenav-overlay-container">
        <div class="sidenav-overlay-content slimscroll">
            <nav class="metismenu-vertical-container">
                <ul class="metismenu">
                    <li>
                        <a href="{{ route('marketplaces.products.index', [$marketplace->slug]) }}">
                            All Categories
                        </a>
                    </li>
                    @if (count($categories))
                        @foreach ($categories as $category)
                            <li>
                                <a href="{{ route('marketplaces.categories.show', [$marketplace->slug, $category->slug]) }}">
                                    {{ $category->name }}
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>
