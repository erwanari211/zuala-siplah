@php
    $cartExist = $_navbarCart && $_navbarCart['count'] ? true : false;
    $cartItems = 0;
    $cartSubtotal = 0;
    if ($cartExist) {
        $cartItems = $_navbarCart['count'];
        $cartSubtotal = $_navbarCart['subtotal'];
    }
@endphp
<li>
    <a href="#" data-action="open-sidenav"
        data-target="#cart-sidebar"
        title="Shopping Cart">
        <i class="fa fa-shopping-cart"></i>
        @if ($cartItems)
            <span class="label bg-skin">{{ $cartItems }}</span>
        @endif
    </a>
</li>
