@php
    $_themes['marketplace_logo_mini'] = isset($_themeSetting['marketplace_logo_mini']) ? $_themeSetting['marketplace_logo_mini'] : env('APP_NAME');
    $_themes['marketplace_logo_lg'] = isset($_themeSetting['marketplace_logo_lg']) ? $_themeSetting['marketplace_logo_lg'] :  env('APP_NAME');
    $logo = isset($websiteSettings['logo']) && $websiteSettings['logo'] ? asset($websiteSettings['logo']) : false;
    if ($logo) {
        $_themes['marketplace_logo_mini'] = asset($logo);
        $_themes['marketplace_logo_lg'] = asset($logo);
    }
@endphp
<header class="main-header fixed navbar-black-light">
    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo navbar-brand">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            @if ($logo)
                <img src="{{ $logo }}">
            @else
                {{ $_themes['marketplace_logo_mini'] }}
            @endif
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            @if ($logo)
                <img src="{{ $logo }}">
            @else
                {{ $_themes['marketplace_logo_lg'] }}
            @endif
        </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->

        <div class="navbar-left">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/') }}" title="Home">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('m/all-stores') }}"
                        title="Display All Stores">
                        <i class="fa fa-store"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ url('m/delivery-status') }}"
                        title="Track Delivery">
                        <i class="fa fa-truck"></i>
                    </a>
                </li>
                @include('modules.marketplaces.frontend.inc.navbar-top-btn-cart')
                @include('modules.marketplaces.frontend.inc.navbar-top-btn-auth')
            </ul>
        </div>
    </nav>
</header>
