<header class="main-header fixed navbar-black-light">
    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo navbar-brand">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            {{-- PB --}}
            <img src="{{ asset('assets/marika-natsuki') }}/images/logo-150x150.png">
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            {{-- PlanetBuku --}}
            <img src="{{ asset('assets/marika-natsuki') }}/images/logo-600x150.png">
        </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->

        <div class="navbar-left">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li>
                    <a href="#" data-action="open-sidenav"
                        data-target="#left-sidebar">
                        <i class="fa fa-bars"></i>
                        <span class="hidden-xs">
                            Categories
                        </span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown menu-search">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-search"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div style="padding: 16px;">
                                <form action="" method="GET" role="form">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search" placeholder="Search product from marketplace">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div><!-- /input-group -->
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown menu-list-icon">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="label bg-skin">4</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 10 notifications</li>
                        <li>
                            <ul class="menu">
                                <li> <a href="#"> <i class="fa fa-check-circle text-success"></i> List with icon </a> </li>
                                <li> <a href="#"> <i class="fa fa-info-circle text-info"></i> List with icon </a> </li>
                                <li> <a href="#"> <i class="fa fa-exclamation-triangle text-warning"></i> List with icon </a> </li>
                                <li> <a href="#"> <i class="fa fa-times-circle text-danger"></i> List with icon </a> </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-check-circle text-success"></i>
                                        List with icon with Very long description here that may not fit into the
                                        page and may cause design problems
                                    </a>
                                </li>
                                <li> <a href="#"> <i class="fa fa-info-circle text-info"></i> List with icon </a> </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <li class="dropdown menu-list-message">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="label bg-skin">4</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 4 messages</li>
                        <li>
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="https://randomuser.me/api/portraits/men/10.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4> Username <small><i class="fa fa-clock-o"></i> 5 mins</small> </h4>
                                        <p>Message</p>
                                    </a>
                                </li>
                                <!-- end message -->
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="https://randomuser.me/api/portraits/women/10.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4> Username <small><i class="fa fa-clock-o"></i> 2 hours</small> </h4>
                                        <p>Message</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="https://randomuser.me/api/portraits/women/20.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4> Username <small><i class="fa fa-clock-o"></i> Today</small> </h4>
                                        <p>Message</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="https://randomuser.me/api/portraits/women/50.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4> Username <small><i class="fa fa-clock-o"></i> Yesterday</small> </h4>
                                        <p>Message</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="https://randomuser.me/api/portraits/women/55.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4> Username <small><i class="fa fa-clock-o"></i> 2 days</small> </h4>
                                        <p>Message</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="https://randomuser.me/api/portraits/men/1.jpg" class="user-image" alt="User Image">
                        <!-- <i class="fa fa-user"></i> -->
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="https://randomuser.me/api/portraits/men/1.jpg" class="img-circle" alt="User Image">
                            <p>
                                Username
                                <small>Role</small>
                            </p>
                        </li>
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <!-- <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-cogs"></i></a>
                </li> -->
            </ul>
        </div>
    </nav>
</header>
