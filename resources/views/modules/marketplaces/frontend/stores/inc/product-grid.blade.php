{!! Form::open([
    'route' => ['cart.store', $store->slug],
    'method' => 'POST',
    'files' => false,
    'class' => 'form-inline', ]) !!}

    <div class="mb-4">
        <div class="d-inline-block change-qty-container bg-info">
            <div class="input-group">
                <input type="number" class="form-control change-qty-input" placeholder="Qty" data-id="qty" min=0>
                <span class="input-group-btn">
                    <button class="btn btn-skin" data-action="change-qty" type="button">Change</button>
                </span>
            </div>
        </div>
        <div class="d-inline-block bg-danger">
            <button type="submit" class="btn btn-primary">Add to Cart</button>
        </div>
    </div>


    <div class="table-responsive">
        <table class="table table-hover table-bordered mb-0">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="hidden-xs">Image</th>
                    <th>Product</th>
                    <th class="qty-column">Qty</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 0;
                    $no = 1;
                @endphp
                @foreach ($products as $product)
                    @php
                        $productMetas = $product->getAllMeta();
                        $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
                        $discountAmountPercent = $product->discountAmountPercent;
                    @endphp
                    <tr>
                        <td>{{ $no }}</td>
                        <td class="hidden-xs">
                            <img src="{{ asset($product->image_url) }}" width="100">
                        </td>
                        <td>
                            <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                {{ $product->name }}
                            </a>
                        </td>
                        <td>
                            <div class="form-group product-qty-container">
                                {!! Form::number("products[$i][qty]", 0, ['min'=>0, 'class'=>'form-control product-qty']) !!}
                            </div>
                            {!! Form::hidden("products[$i][product_id]", $product->id) !!}
                        </td>
                        <td>
                            @if ($originalPrice)
                                <small class="text-muted">
                                    <del>{{ "Rp.".formatNumber($originalPrice) }}</del>
                                </small>
                                <br>
                            @endif
                            Rp.{{ formatNumber($product->price) }}
                            @if ($discountAmountPercent)
                                <br>
                                <span class="btn btn-xs btn-skin">
                                    {{ $discountAmountPercent }}% OFF
                                </span>
                            @endif
                        </td>
                    </tr>
                    @php
                        $i++;
                        $no++;
                    @endphp
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="mt-4">
        <button type="submit" class="btn btn-primary">Add to Cart</button>
    </div>

{!! Form::close() !!}
