@php
    $productUrl = '';
    if ($product->unique_code) {
        $productUrl = route('shorturl.products.show', [$product->unique_code]);
    } else {
        $productUrl = route('products.show', [$marketplace->slug, $store->slug, $product->slug]);
    }
@endphp

var logo = new Image();
logo.crossOrigin = "Anonymous";
logo.onload = () => {
    require([__awesome_qr_base_path+'/awesome-qr'], function(AwesomeQR) {
        AwesomeQR.create({
            text: '{{ $productUrl }}',
            size: 800,
            margin: 20,
            logoImage: logo,
            bindElement: 'qrcode-logo-alt',

            colorDark: '#0000ff',
            dotScale: 0.7,
            logoMargin: 8,
        });
    });
};
logo.src = "{{ asset('images') }}/zuala-logo-small-v0.2.png";
