@push('css')
    <style type="text/css">
        .store-navbar-container .navbar-default {
          background-color: #ffffff;
          border-color: #e6e6e6;
        }
        .store-navbar-container .navbar-default .navbar-brand {
          color: #333333;
        }
        .store-navbar-container .navbar-default .navbar-brand:hover,
        .store-navbar-container .navbar-default .navbar-brand:focus {
          color: #333333;
          background-color: #e6e6e6;
        }
        .store-navbar-container .navbar-default .navbar-text {
          color: #333333;
        }
        .store-navbar-container .navbar-default .navbar-nav > li > a {
          color: #333333;
        }
        .store-navbar-container .navbar-default .navbar-nav > li > a:hover,
        .store-navbar-container .navbar-default .navbar-nav > li > a:focus {
          color: #333333;
          background-color: #e6e6e6;
        }
        .store-navbar-container .navbar-default .navbar-nav > .active > a,
        .store-navbar-container .navbar-default .navbar-nav > .active > a:hover,
        .store-navbar-container .navbar-default .navbar-nav > .active > a:focus {
          color: #333333;
          background-color: #e6e6e6;
        }
        .store-navbar-container .navbar-default .navbar-nav > .open > a,
        .store-navbar-container .navbar-default .navbar-nav > .open > a:hover,
        .store-navbar-container .navbar-default .navbar-nav > .open > a:focus {
          color: #333333;
          background-color: #e6e6e6;
        }
        .store-navbar-container .navbar-default .navbar-toggle {
          border-color: #e6e6e6;
        }
        .store-navbar-container .navbar-default .navbar-toggle:hover,
        .store-navbar-container .navbar-default .navbar-toggle:focus {
          background-color: #e6e6e6;
        }
        .store-navbar-container .navbar-default .navbar-toggle .icon-bar {
          background-color: #333333;
        }
        .store-navbar-container .navbar-default .navbar-collapse,
        .store-navbar-container .navbar-default .navbar-form {
          border-color: #333333;
        }
        .store-navbar-container .navbar-default .navbar-link {
          color: #333333;
        }
        .store-navbar-container .navbar-default .navbar-link:hover {
          color: #333333;
        }

        .store-navbar-container .navbar-default {
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15);
        }

        .store-navbar-container .dropdown-menu {
            padding: 5px 0;
        }

        @media (max-width: 767px) {
          .store-navbar-container .navbar-default .navbar-nav .open .dropdown-menu > li > a {
            color: #333333;
          }
          .store-navbar-container .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
          .store-navbar-container .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
            color: #333333;
          }
          .store-navbar-container .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
          .store-navbar-container .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
          .store-navbar-container .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
            color: #333333;
            background-color: #e6e6e6;
          }
        }
    </style>
@endpush

<div class="store-navbar-container mt-4">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-store-menu-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-store-menu-collapse">
                <ul class="nav navbar-nav">
                    @if ($storeMenus)
                        @foreach ($storeMenus as $menu)
                            @php
                                $hasChild = count($menu->children) ? true : false;
                                $menuLabel = $menu->label;
                                $menuLink = $menu->link ? $menu->link : '#';
                            @endphp
                            @if (!$hasChild)
                                <li>
                                    <a href="{{ $menuLink }}">{{ $menuLabel }}</a>
                                </li>
                            @else
                                <li class="dropdown">
                                    <a href="{{ $menuLink }}" class="dropdown-toggle" data-toggle="dropdown">
                                        {{ $menuLabel }} <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        @foreach ($menu->children as $childMenu)
                                            @php
                                                $menuLabel = $childMenu->label;
                                                $menuLink = $childMenu->link ? $childMenu->link : '#';
                                            @endphp
                                            <li>
                                                <a href="{{ $menuLink }}">{{ $menuLabel }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
</div>
