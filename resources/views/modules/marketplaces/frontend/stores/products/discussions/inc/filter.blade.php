@php
    $filters = [
        'all' => [
            'url' => route('products.discussions.index', [$marketplace->slug, $store->slug, $product->slug]),
            'label' => 'All'
        ],
        'me' => [
            'url' => route('products.discussions.index', [$marketplace->slug, $store->slug, $product->slug, 'author' => 'me']),
            'label' => 'My Discussion'
        ],
    ]
@endphp
<div class="discussion-filters mb-4">
    @foreach ($filters as $filterName => $filter)
        @php
            $btnClass = 'btn ';
            $btnClass .= $filterName == $discussionFilter ? 'btn-primary ' : 'btn-default ';
            if (!auth()->check()) {
                if ($filterName == 'me') {
                    $btnClass .= 'disabled';
                }
            }
        @endphp
        <a class="{{ $btnClass }}"
            href="{{ $filter['url'] }}">
            {{ $filter['label'] }}
        </a>
    @endforeach
</div>
