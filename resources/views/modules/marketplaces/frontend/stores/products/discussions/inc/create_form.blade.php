@if (auth()->check())
    <h3 class="mt-0">Add Discussion</h3>
    {!! Form::open([
        'route' => [
            'products.discussions.store',
            $marketplace->slug,
            $store->slug,
            $product->slug
        ],
        'method' => 'POST',
        'files' => false,
        'class' => 'mb-4'
        ]) !!}

        <div class="form-group {{ $errors->has('content') ?  'has-error' : '' }}">
            <textarea name="content" class="form-control" rows="3"  placeholder="Discussion"></textarea>
            @if ($errors->has('content'))
                <span class="help-block" role="alert">
                    <strong>{{ $errors->first('content') }}</strong>
                </span>
            @endif
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>

    {!! Form::close() !!}
@else
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Please <a href="{{ route('login') }}">login</a> to add discussion.
    </div>
@endif
