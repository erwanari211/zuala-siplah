@include('modules.marketplaces.frontend.stores.products.reviews.inc.create_form')
<hr>
<h3>Recent Reviews</h3>
@include('modules.marketplaces.frontend.stores.products.reviews.inc.filter')
<div class="product__discussions">
    @if (count($productReviews))
        @foreach ($productReviews as $review)
            <div class="product__discussion">
                <div class="media">
                    <a class="pull-left" href="{{ route('products.discussions.show', [$marketplace->slug, $store->slug, $product->slug, $review->id]) }}">
                        <img class="media-object lazy img-circle"
                            data-src="{{ asset($review->user->photo) }}" alt="Image" height="50">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{ $review->user->name }}</h4>
                        <small class="text-muted">
                            <i class="fa fa-clock"></i>
                            <em>{{ $review->updated_at }}</em>
                        </small>
                        <div class="mt-3">
                            <select class="rating" data-initial-rating="{{ $review->rating }}">
                              <option value="">0</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                            </select>
                        </div>
                        <p>{{ $review->content }}</p>
                    </div>
                </div>
                <hr>
            </div>
        @endforeach
        @if ($countProductReviews > 3)
            <a class="btn btn-default btn-xs"
                href="{{ route('products.reviews.index', [$marketplace->slug, $store->slug, $product->slug]) }}">
                See more reviews
            </a>
        @endif
    @else
        <p>No review</p>
    @endif
</div>
