@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = '{note} - {store} | {APP}';
    $templatePageDescription = 'Get products you need from {store}. Find smiliar product at {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{store}', '{marketplace}', '{APP}', '{note}'];
    $replaceArray = [$store->name, $marketplace->name, env('APP_NAME'), $note->title];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .change-qty-container {
            width: 200px;
        }

        .qty-column {
            width: 150px;
        }

        .product-qty-container {
            width: 135px;
        }
    </style>
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/bootstrap-number-input/bootstrap-number-input.js" ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#store-category-sidebar').metisMenu({
                toggle: false,
            });

            $('.product-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        stagePadding: 80,
                    },
                    480:{
                        items:2,
                        nav:false,
                    },
                    768:{
                        items:3
                    },
                    992:{
                        items:4
                    },
                    1200:{
                        items:4
                    }
                },
            });

            @include('modules.marketplaces.frontend.stores.inc.store-info-script')

            $('.product-qty').bootstrapNumber();

            $('[data-action="change-qty"]').on('click', function(event) {
                event.preventDefault();
                var qty = $('[data-id="qty"]').val();
                var isNumber = (isNaN(qty) || !qty.length) ? false : true;
                if (isNumber) {
                    $('.product-qty').val(qty);
                }
            });

        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')
        @include('modules.marketplaces.frontend.stores.inc.store-inactive-message')

        <div class="mb-3">
            <div class="container">
                @include('flash::message')

                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                            {{ $store->name }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('stores.notes.index', [$marketplace->slug, $store->slug]) }}">
                            Notes
                        </a>
                    </li>
                    <li class="active">{{ $note->title }}</li>
                </ol>
                @include('modules.marketplaces.frontend.stores.inc.store-info')
                @include('modules.marketplaces.frontend.stores.inc.store-menu-navbar')
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="box">
                        <div class="box-content">
                            <h3 class="mt-0">{{ $note->title }}</h3>
                            <div class="mt-4 mb-4">
                                {!! $note->content !!}
                            </div>
                            <div class="clearfix">
                                <small class="text-muted pull-right">
                                    Posted at {{ $note->created_at->format('Y-m-d') }}
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-content">
                            <h3 class="mt-0">Recent Notes</h3>

                            @if (count($storeNotes))
                                @foreach ($storeNotes as $note)
                                    <strong>
                                        <a href="{{ route('stores.notes.show', [$marketplace->slug, $store->slug, $note->id]) }}" style="margin-right: 20px;">
                                            {{ $note->title }}
                                        </a>
                                    </strong>
                                    <br>
                                    <small>
                                        <i class="fa fa-clock"></i>
                                        {{ $note->created_at->format('Y-m-d') }}
                                    </small>
                                    <hr class="mt-4 mb-2">
                                @endforeach
                                @php
                                    $countNote = count($storeNotes);
                                    $totalNote = $storeNotes->total();
                                @endphp
                                @if ($totalNote > $countNote)
                                    <a class="btn btn-default btn-xs"
                                        href="{{ route('stores.notes.index', [$marketplace->slug, $store->slug]) }}">
                                        See More
                                    </a>
                                @endif
                            @else
                                No Note
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>





    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
