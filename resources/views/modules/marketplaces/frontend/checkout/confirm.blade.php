@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Confirm Checkout')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script>
        $(document).ready(function() {
            $('body').on('click', '[data-action="checkout"]', function(event) {
                event.preventDefault();
                var formId = $(this).attr('data-form-id');
                var form = $('#'+formId);
                form.submit();
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cart.index') }}">
                                Cart
                            </a>
                        </li>
                        <li class="active">
                            Confirm Checkout
                        </li>
                    </ol>

                    <h3>Checkout</h3>

                    <div class="box">
                        <div class="box-content">
                            <a class="btn btn-default" href="{{ route('checkout.edit', [$store->slug]) }}">Step 1</a>
                            <a class="btn btn-default" href="{{ route('checkout.steps.billing-address.edit', [$store->slug]) }}">Step 2</a>
                            <a class="btn btn-default" href="{{ route('checkout.steps.shipping-address.edit', [$store->slug]) }}">Step 3</a>
                            <a class="btn btn-default" href="{{ route('checkout.steps.shipping-method.edit', [$store->slug]) }}">Step 4</a>
                            <a class="btn btn-default" href="{{ route('checkout.steps.payment-method.edit', [$store->slug]) }}">Step 5</a>
                            <a class="btn btn-primary" href="{{ route('checkout.steps.confirm.show', [$store->slug]) }}">Step 6</a>
                        </div>
                    </div>

                    @include('flash::message')

                    @php
                        $currentStore = $store;
                        $marketplace = $currentStore->marketplace;
                        $subtotal = Cart::instance($store->slug)->subtotal();
                        $subtotal = floatvalue($subtotal);
                        $i = 1;
                        $cart = Cart::instance($store->slug)->content();
                    @endphp

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['cart.update', $store->slug],
                                'method' => 'PUT',
                                'files' => false,
                                ]) !!}

                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered mb-0">
                                        <thead>
                                            <tr class="bg-info">
                                                <th colspan="7">
                                                    <a href="{{ route('stores.show', [$marketplace->slug, $currentStore->slug]) }}">
                                                        {{ $currentStore->name }}
                                                    </a>
                                                </th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Product</th>
                                                <th>Qty</th>
                                                <th>Price</th>
                                                <th>Subtotal</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($cart as $row)
                                                <tr>
                                                    <td>
                                                        <img src="{{ asset($row->model->image) }}" width="100" height="100">
                                                    </td>
                                                    <td>{{ $row->name }}</td>
                                                    <td> {{ $row->qty }} </td>
                                                    <td>{{ formatNumber($row->price) }}</td>
                                                    <td>{{ formatNumber($row->total) }}</td>
                                                    <td></td>
                                                </tr>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            @php
                                                $courier = $cartMeta['shipping_method']['name'];
                                                $service = $cartMeta['shipping_method']['service'];
                                                $shippingCost = $cartMeta['shipping_method']['value'];
                                                $subtotal = floatvalue($subtotal);
                                                $uniqueNumber = $cartMeta['payment_method']['unique_number'];
                                                $total = $subtotal + $shippingCost + $uniqueNumber;

                                                $paymentMethodInfo = $cartMeta['payment_method']['info'];
                                            @endphp
                                            @php
                                                $storeSeller = isset($cartMeta['order']['store_seller']) ? $cartMeta['order']['store_seller'] : null;
                                            @endphp

                                            <tr>
                                                <td colspan="4">Subtotal</td>
                                                <td>{{ formatNumber($subtotal) }}</td>
                                                <td></td>
                                            </tr>


                                            <tr>
                                                <td colspan="4">{{ $courier . ' ' . $service }}</td>
                                                <td>{{ formatNumber($shippingCost) }}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td colspan="4">Unique Number</td>
                                                <td>{{ formatNumber($uniqueNumber) }}</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td colspan="4">
                                                    <strong>Total</strong>
                                                </td>
                                                <td>
                                                    <strong>{{ formatNumber($total) }}</strong>
                                                </td>
                                                <td></td>
                                            </tr>

                                            @if ($storeSeller)
                                                <tr>
                                                    <td colspan="6">
                                                        Seller :
                                                        <strong>{{ $storeSeller }}</strong>
                                                    </td>
                                                </tr>
                                            @endif

                                            <tr>
                                                <td colspan="6">
                                                    <p>Payment Info</p>
                                                    <div class="well">
                                                        {!! $paymentMethodInfo !!}
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="7">
                                                    {!! Form::bs3Submit('Confirm', [
                                                        'class'=>'btn btn-primary',
                                                        'data-action'=>'checkout',
                                                        'data-form-id'=>"checkout-{$currentStore->slug}",
                                                        ]); !!}
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                    {!! Form::open([
                            'route' => ['checkout.steps.confirm.store', $currentStore->slug],
                            'method' => 'POST',
                            'style' => 'display: inline-block;',
                            'id' => 'checkout-'.$currentStore->slug
                        ]) !!}
                    {!! Form::close() !!}



                </div>
            </div>
        </div>
    </div>

    @php
        $isHomepage = $useHomepageFooter;
    @endphp
    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
