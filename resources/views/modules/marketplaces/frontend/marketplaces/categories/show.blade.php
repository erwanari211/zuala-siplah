@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = '{category} - {marketplace} | {APP}';
    $templatePageDescription = 'Get products you need from {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{marketplace}', '{APP}', '{category}'];
    $replaceArray = [$marketplace->name, env('APP_NAME'), $category->name];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .promo-title span {
            display: inline-block;
            border-bottom: 5px solid;
        }

        .product__name a,
        .product__store__name a {
            text-decoration: none;
        }

        .d-inline-block {
            display: inline-block;
        }
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#marketplace-category-sidebar').metisMenu({
                toggle: false,
            });

            $('[data-popular-search]').on('click', function(event) {
                event.preventDefault();
                var searchString = $(this).attr('data-popular-search');
                var marketplaceProductUrl = '{{ route('marketplaces.products.index', [$marketplace->slug]) }}';
                $('[name="search"]').val(searchString);
                $('#navbar-search-form').attr('action', marketplaceProductUrl);
                $('#navbar-search-form').submit();
            });

            $('#products-sort').on('change', function(event) {
                event.preventDefault();
                var url = '{{ $url }}';
                var queryString = '{{ $queryString }}';
                var sortBy = $(this).val();
                var newUrl = '';
                newUrl += url + '?sort=' + sortBy;
                newUrl += (queryString) ? '&' + queryString : '';
                window.location.replace(newUrl);
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-marketplace-category')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('marketplaces.show', [$marketplace->slug]) }}">
                                {{ $marketplace->name }}
                            </a>
                        </li>
                        <li>
                            <a href="#" data-action="open-sidenav"
                                data-target="#marketplace-category-sidebar">
                                Categories
                            </a>
                        </li>
                        @if ($category->parent_id == 0)
                            <li class="active">{{ $category->name }}</li>
                        @endif
                        @if ($category->parent_id != 0)
                            <li>
                                <a href="{{ route('marketplaces.categories.show', [
                                    $marketplace->slug, $category->parent->slug]) }}">
                                    {{ $category->parent->name }}
                                </a>
                            </li>
                            <li class="active">{{ $category->name }}</li>
                        @endif
                    </ol>

                    <div class="box category-container">
                        <div class="box-content row">
                            <div class="col-sm-6">
                                <div class="category-info-container">
                                    <div class="media">
                                        <a class="pull-left" href="{{ route('marketplaces.categories.show', [$marketplace->slug, $category->slug]) }}">
                                            <img class="category-image media-object lazy" data-src="{{ asset($category->image) }}" alt="Image">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="category-container__category-name media-heading">{{ $category->name }}</h4>
                                            <p class="category-container__category-description">{{ $category->description }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="subcategory-container">
                                    @if (count($subcategories))
                                        <h4 class="subcategory-container-title">Subcategories</h4>
                                        <ul class="list-unstyled">
                                            @foreach ($subcategories as $subcategory)
                                                <li>
                                                    <a href="{{ route('marketplaces.categories.show', [$marketplace->slug, $subcategory->slug]) }}">
                                                        {{ $subcategory->name }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    @if (request('search'))
                        <h4>
                            Search result for "{{ request('search') }}"
                        </h4>
                    @endif
                </div>
            </div>
        </div>

        @include('modules.marketplaces.frontend.marketplaces.inc.product-display-options')

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    @if (count($products))
                        <div class="products products-grid row">
                            @foreach ($products as $product)
                                @php
                                    $productMetas = $product->getAllMeta();
                                    $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
                                    $discountAmountPercent = $product->discountAmountPercent;
                                @endphp
                                <div class="col-md-3 col-sm-4 col-xs-6">
                                    <div class="product">
                                        @if ($discountAmountPercent)
                                            <span class="product__label right">
                                                <div class="text-center">
                                                {{ $discountAmountPercent }}%<br>OFF
                                                </div>
                                            </span>
                                        @endif
                                        <a href="{{ route('products.show', [$marketplace->slug, $product->store->slug, $product->slug]) }}">
                                            <div class="product__image-wrapper">
                                                <img class="product__image lazy" data-src="{{ $product->image_url }}">
                                            </div>
                                        </a>
                                        <strong class="product__name">
                                            <a href="{{ route('products.show', [$marketplace->slug, $product->store->slug, $product->slug]) }}">
                                                {{ $product->name }}
                                            </a>
                                        </strong>

                                        @php
                                            $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
                                        @endphp

                                        @if (!$hasKemdikbudZonePrice)

                                        <div class="product__price">
                                            @if ($originalPrice)
                                                <span class="product__original-price">{{ "Rp.".formatNumber($originalPrice) }}</span>
                                            @endif
                                            <span class="product__current-price">{{ "Rp.".formatNumber($product->price) }}</span>
                                        </div>

                                        @endif

                                        @if ($hasKemdikbudZonePrice)
                                            @php
                                                $price = 0;
                                                $minPrice = 0;
                                                $maxPrice = 0;

                                                $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
                                                $kemdikbudMinMaxZonePrice = $product->getKemdikbudMinMaxZonePrices($mappedKemdikbudZonePrices);
                                                $minPrice = $kemdikbudMinMaxZonePrice['minPrice'];
                                                $maxPrice = $kemdikbudMinMaxZonePrice['maxPrice'];
                                                $formattedZonePrice = $product->getKemdikbudFormattedZonePrices($minPrice, $maxPrice);
                                            @endphp
                                            <div class="product__price">
                                                <span class="product__current-price">{{ $formattedZonePrice }}</span>
                                            </div>
                                        @endif

                                        <div class="product__rating">
                                            <select class="rating" data-initial-rating="{{ $product->reviews->average('rating') }}">
                                              <option value="">0</option>
                                              <option value="1">1</option>
                                              <option value="2">2</option>
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                            </select>
                                            @if ($product->reviews->count())
                                                <span>({{ $product->reviews->count() }})</span>
                                            @endif
                                        </div>
                                        <div class="product__store">
                                            <div class="product__store__name">
                                                <a class="text-muted"
                                                    href="{{ route('stores.show', [$marketplace->slug, $product->store->slug]) }}">
                                                    <span>{{ $product->store->name }}</span>
                                                </a>
                                            </div>
                                            @php
                                                $store = $product->store;
                                                $addresses = $store->addresses;
                                                $storeAddress = null;
                                                foreach ($addresses as $address) {
                                                    if ($address->default_address) {
                                                        $storeAddress = $address;
                                                    }
                                                }

                                                $location = [];
                                                if ($storeAddress) {
                                                    if ($storeAddress->city) {
                                                        $cityName = $storeAddress->city->name;
                                                        $location[] = $cityName;
                                                    }
                                                    if ($storeAddress->province) {
                                                        $location[] = $storeAddress->province->name;
                                                    }
                                                }

                                                $storeAddress = '';
                                                $storeAddress = ucwords(strtolower(implode(', ', $location)))
                                            @endphp
                                            <div class="product__store__location">
                                                <small title="{{ $storeAddress }}">
                                                    <i class="fa fa-map-marker-alt text-danger"></i>
                                                    <span class="text-muted">{{ $storeAddress }}</span>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        No Product
                    @endif
                    {{ $products->appends(request()->only(['search']))->links() }}
                </div>
            </div>
        </div>
    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-marketplace-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
