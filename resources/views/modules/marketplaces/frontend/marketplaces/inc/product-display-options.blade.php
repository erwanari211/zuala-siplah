<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-content">
                    <div class="d-inline-block mr-4">
                        Sort
                        {!! Form::select('products-sort', $dropdown['sortBy'], $sortBy, ['id'=>'products-sort']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
