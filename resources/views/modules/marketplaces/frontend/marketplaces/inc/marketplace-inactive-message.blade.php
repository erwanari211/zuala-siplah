@if (!$marketplace->active)
    <div class="container">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            This marketplace is currently inactive and waiting approval.
        </div>
    </div>
@endif
