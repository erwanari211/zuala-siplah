@php
@endphp
<div class="sidenav sidenav-overlay sidenav-overlay--left" id="user-sidebar">
    <div class="sidenav-overlay-toolbar">
        <button class="btn btn-default btn-xs sidenav-overlay-close"
            data-action="close-sidenav"
            data-target="#user-sidebar">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidenav-overlay-container">
        <div class="sidenav-overlay-content slimscroll">
            <nav class="metismenu-vertical-container">
                <ul class="metismenu">
                    <li> <span class="divider"><i class="fa fa-fw fa-user"></i> Administrator</span> </li>
                    <li> <a href="{{ route('administrator.index') }}">Dashboard</a> </li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Marketplaces</span>  </li>
                    <li> <a href="{{ route('administrator.marketplaces.index') }}">Marketplaces</a> </li>
                    <li>
                        <a href="{{ route('administrator.marketplace-collections.index') }}">
                            Marketplace Collections
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('administrator.product-collections.index') }}">
                            Product Collections
                        </a>
                    </li>

                    <li> <span class="divider"><i class="fa fa-fw fa-bullhorn"></i> Promo</span>  </li>
                    <li> <a href="{{ route('administrator.website.banners.index') }}">Banners</a> </li>
                    <li> <a href="{{ route('administrator.website.banner-groups.index') }}">Banner Groups</a> </li>

                    <li> <span class="divider"><i class="fa fa-fw fa-blog"></i> Blog</span>  </li>
                    <li> <a href="{{ route('administrator.website.cms.blog-posts.index') }}">Blog Posts</a> </li>
                    <li> <a href="{{ route('administrator.website.cms.blog-categories.index') }}">Blog Categories</a> </li>
                    <li> <a href="{{ route('administrator.website.cms.blog-banners.index') }}">Banners</a> </li>

                    <li> <span class="divider"><i class="fa fa-fw fa-bullhorn"></i> CMS</span>  </li>
                    <li> <a href="{{ route('administrator.website.cms.pages.index') }}">Pages</a> </li>
                    <li> <a href="{{ route('administrator.website.cms.layout-elements.index') }}">Layout Element</a> </li>
                    <li> <a href="{{ route('administrator.website.cms.media-libraries.index') }}">Media Libraries</a> </li>

                    <li> <span class="divider"><i class="fa fa-fw fa-cogs"></i> Settings</span>  </li>
                    <li> <a href="{{ route('administrator.website.settings.edit') }}">Settings</a> </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
