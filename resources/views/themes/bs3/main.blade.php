@php
    $_themes['navbar']  = isset($_themeSetting['navbar']) ? $_themeSetting['navbar'] :  'themes.bs3.custom.navbar';
    $_themes['head_js'] = isset($_themeSetting['head_js']) ? $_themeSetting['head_js'] :  'themes.bs3.default.head_js';
    $_themes['meta']    = isset($_themeSetting['head_js']) ? $_themeSetting['head_js'] :  'themes.bs3.default.meta';
    $_themes['js']      = isset($_themeSetting['js']) ? $_themeSetting['js'] :  'themes.bs3.default.js';
    $_themes['css']     = isset($_themeSetting['css']) ? $_themeSetting['css'] :  'themes.bs3.default.css';
@endphp
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title', 'Laravel')</title>

        @includeIf($_themes['meta'])
        @includeIf($_themes['css'])
        @includeIf($_themes['head_js'])
    </head>
    <body>
        @includeIf($_themes['navbar'])

        <div class="container">
            @yield('content')
        </div>

        @includeIf($_themes['js'])
    </body>
</html>
