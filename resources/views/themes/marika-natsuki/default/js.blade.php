<!-- Bootstrap JavaScript -->
<script src="{{ asset('assets/marika-natsuki/plugins') }}/bootstrap-3.3.7/js/bootstrap.min.js" ></script>

<!-- Plugins JS -->
<script src="{{ asset('assets/marika-natsuki/plugins') }}/jquery-slimscroll-1.3.8/jquery.slimscroll.min.js"></script>
<script src="{{ asset('assets/marika-natsuki/plugins') }}/lazyload-10.19.0/dist/lazyload.min.js"></script>
<script src="{{ asset('assets/marika-natsuki/plugins') }}/toastr/build/toastr.min.js"></script>
<script src="{{ asset('assets/marika-natsuki/plugins') }}/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<script src="{{ asset('assets/marika-natsuki/plugins') }}/metismenu/dist/metisMenu.min.js"></script>
<script src="{{ asset('assets/marika-natsuki/plugins') }}/owl-carousel2-2.3.4/dist/owl.carousel.min.js"></script>

<!-- App JS -->
<script src="{{ asset('assets/marika-natsuki/js') }}/scripts.js" ></script>

<!-- Page JS -->
@stack('js')
