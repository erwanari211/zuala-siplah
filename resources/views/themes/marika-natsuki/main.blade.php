@php
    $_themes['meta']    = isset($_themeSetting['meta']) ? $_themeSetting['meta'] :  'themes.marika-natsuki.default.meta';
    $_themes['css']     = isset($_themeSetting['css']) ? $_themeSetting['css'] :  'themes.marika-natsuki.default.css';
    $_themes['head_js'] = isset($_themeSetting['head_js']) ? $_themeSetting['head_js'] :  'themes.marika-natsuki.default.head_js';
    $_themes['js']      = isset($_themeSetting['js']) ? $_themeSetting['js'] :  'themes.marika-natsuki.default.js';
    $_themes['skin']      = isset($_themeSetting['skin']) ? $_themeSetting['skin'] :  'skin-blue';
@endphp
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title', 'Laravel')</title>

        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images') }}/favicon-32x32.png">
        @includeIf($_themes['meta'])
        @includeIf($_themes['css'])
        @includeIf($_themes['head_js'])

        @if (isset($websiteSettings['google_analytics']) && $websiteSettings['google_analytics'])
            {!! $websiteSettings['google_analytics'] !!}
        @endif
    </head>
    <body class="{{ $_themes['skin'] }}">

        @yield('content')

        @includeIf($_themes['js'])
    </body>
</html>
