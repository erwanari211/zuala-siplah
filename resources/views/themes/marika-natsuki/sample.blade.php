@php
    $_themeSetting['skin'] = 'skin-red';
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Marketplace')

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('themes.marika-natsuki.custom.navbar-top-sample')

    <div class="page-content" style="height: 1500px;">
        <h1 class="text-center">Marika Natsuki</h1>
    </div>

    <div class="sidenav sidenav-overlay sidenav-overlay--left" id="left-sidebar">
        <div class="sidenav-overlay-toolbar">
            <button class="btn btn-default btn-xs sidenav-overlay-close"
                data-action="close-sidenav"
                data-target="#left-sidebar">
                <i class="fa fa-times"></i>
            </button>
        </div>
        <div class="sidenav-overlay-container">
            <div class="sidenav-overlay-content slimscroll">
                <h2>Left Sidebar</h2>
                @for ($i = 1; $i <= 50; $i++)
                    <h3>Content here {{ $i }}</h3>
                @endfor
            </div>
        </div>
    </div>

@endsection
