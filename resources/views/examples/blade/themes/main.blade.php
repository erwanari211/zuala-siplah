@php
    $_themeDirectory = 'examples.blade.themes';
    $_defaultTheme = 'first-theme';
    $_theme = isset($_currentTheme) ? $_currentTheme : $_defaultTheme;
    $_themeView = "{$_themeDirectory}.{$_theme}.main";
    $_theme = view()->exists($_themeView) ? $_theme : $_defaultTheme;
@endphp

@include("{$_themeDirectory}.{$_theme}.main")
