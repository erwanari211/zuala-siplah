@extends('examples.themes.adminlte.main')

@section('title', 'Edit Post')

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('js')
  <script src="{{ asset('assets/adminlte-2.4.5/bower_components/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

  <script>
    $(document).ready(function() {
      $('.datetimepicker').datetimepicker();
    });
  </script>
@endsection

@section('content-header')
  <h1>
    Edit Post
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Posts</a></li>
    <li class="active">Edit</li>
  </ol>
@endsection

@section('content')
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    {!! Form::model($post, ['route' => ['examples.posts.update', $post->id], 'method' => 'PUT', 'files' => false, ]) !!}
      <section class="col-lg-8">

        {!! adminlte_box_open('Edit Post') !!}

          <div class="box-body">

            @include('flash::message')
            @include('examples.todos.messages')

            {!! Form::bs3Text('title') !!}
            {!! Form::bs3Text('slug') !!}
            {!! Form::bs3Textarea('content', null, ['rows'=>10]) !!}
            {!! Form::bs3Textarea('excerpt') !!}


          </div>

          <div class="box-footer clearfix">
            {!! Form::bs3Submit('Update', ['class'=>'btn btn-primary btn-flat']); !!}
            {!! Form::bs3LinkToRoute('examples.posts.index', 'Back', [], ['class'=>'btn btn-default btn-flat']); !!}
          </div>

        {!! adminlte_box_close() !!}

      </section>

      <section class="col-lg-4">

        {!! adminlte_box_open('') !!}

          <div class="box-body">

            {!! Form::bs3Datetimepicker('published_at') !!}
            {!! Form::bs3Select('post_status', $postStatusOptions) !!}
            {!! Form::bs3Select('comment_status', $commentStatusOptions) !!}

          </div>

        {!! adminlte_box_close() !!}

      </section>
    {!! Form::close() !!}
    <!-- /.Left col -->

  </div>
  <!-- /.row (main row) -->
@endsection
