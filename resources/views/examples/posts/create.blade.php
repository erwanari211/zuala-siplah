@extends('examples.themes.adminlte.main')

@section('title', 'Create Post')

@section('css')
@endsection

@section('js')
@endsection

@section('content-header')
  <h1>
    Create Post
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Posts</a></li>
    <li class="active">Create</li>
  </ol>
@endsection

@section('content')
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    {!! Form::open(['route' => ['examples.posts.store'], 'method' => 'POST', 'files' => false, ]) !!}
      <section class="col-lg-8">

        {!! adminlte_box_open('Create Post') !!}

          <div class="box-body">

            @include('flash::message')
            @include('examples.todos.messages')

            {!! Form::bs3Text('title') !!}
            {!! Form::bs3Text('slug') !!}
            {!! Form::bs3Textarea('content', null, ['rows'=>10]) !!}
            {!! Form::bs3Textarea('excerpt') !!}


          </div>

          <div class="box-footer clearfix">
            {!! Form::bs3Submit('Save', ['class'=>'btn btn-primary btn-flat']); !!}
            {!! Form::bs3LinkToRoute('examples.posts.index', 'Back', [], ['class'=>'btn btn-default btn-flat']); !!}
          </div>

        {!! adminlte_box_close() !!}

      </section>
    {!! Form::close() !!}
    <!-- /.Left col -->

  </div>
  <!-- /.row (main row) -->
@endsection
