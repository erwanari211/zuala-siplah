@extends('examples.themes.adminlte.main')

@section('title', 'Edit Post')

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('js')
  <script src="{{ asset('assets/adminlte-2.4.5/bower_components/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

  <script>
    $(document).ready(function() {
      $('#main-form-messages')
        .hide()
        .on('click', function(event) {
          event.preventDefault();
          $(this).hide();
        });

      $('#main-form').on('submit', function(event) {
        event.preventDefault();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        console.log('submitted');
        var formData = {
            title: $('#title').val(),
            slug: $('#slug').val(),
            content: $('#content').val(),
            excerpt: $('#excerpt').val(),
            published_at: $('#published_at').val(),
            post_status: $('#post_status').val(),
            comment_status: $('#comment_status').val(),
        }
        $.ajax({
          url: '{{ route('examples.ajax-posts.update', [$post->id]) }}',
          type: 'PUT',
          dataType: 'json',
          data: formData,
        })
        .done(function(data) {
          console.log("success");
          console.log(data);

          var message = data.message;

          $('#main-form-messages')
            .addClass('alert-success')
            .removeClass('alert-danger')
            .hide()
            .html(message)
            .fadeIn();
        })
        .fail(function(data) {
          var res = data.responseJSON;
          var errors = res.errors;
          var output = '';

          for (var i in errors) {
            var error = errors[i][0];
            output += error + '<br>';
          }

          $('#main-form-messages')
            .addClass('alert-danger')
            .removeClass('alert-success')
            .hide()
            .html(output)
            .fadeIn();
        })
        .always(function() {
          console.log("complete");
        });
      });

      $('.datetimepicker').datetimepicker();
    });
  </script>
@endsection

@section('content-header')
  <h1>
    Edit Post (AJAX)
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Posts</a></li>
    <li class="active">Edit</li>
  </ol>
@endsection

@section('content')
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    {!! Form::model($post, ['route' => ['examples.posts.store'], 'method' => 'POST', 'files' => false, 'id'=>'main-form']) !!}
      <section class="col-lg-8">

        {!! adminlte_box_open('Edit Post') !!}

          <div class="box-body">

            @include('flash::message')
            @include('examples.todos.messages')
            <div class="alert" id="main-form-messages">
            </div>

            {!! Form::bs3Text('title') !!}
            {!! Form::bs3Text('slug') !!}
            {!! Form::bs3Textarea('content', null, ['rows'=>10]) !!}
            {!! Form::bs3Textarea('excerpt') !!}


          </div>

          <div class="box-footer clearfix">
            {!! Form::bs3Submit('Update', ['class'=>'btn btn-primary btn-flat']); !!}
            {!! Form::bs3LinkToRoute('examples.posts.index', 'Back', [], ['class'=>'btn btn-default btn-flat']); !!}
          </div>

        {!! adminlte_box_close() !!}

      </section>

      <section class="col-lg-4">

        {!! adminlte_box_open('') !!}

          <div class="box-body">

            {!! Form::bs3Datetimepicker('published_at') !!}
            {!! Form::bs3Select('post_status', $postStatusOptions) !!}
            {!! Form::bs3Select('comment_status', $commentStatusOptions) !!}

          </div>

        {!! adminlte_box_close() !!}

      </section>

    {!! Form::close() !!}
    <!-- /.Left col -->

  </div>
  <!-- /.row (main row) -->
@endsection
