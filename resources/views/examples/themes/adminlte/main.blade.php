@php
  $_themeStyles = config('adminlte.layout.styles_location');
  $_themeHeader = config('adminlte.layout.header_location');
  $_themeSidebar = config('adminlte.layout.sidebar_location');
  $_themeFooter = config('adminlte.layout.footer_location');
  $_themeScripts = config('adminlte.layout.scripts_location');
@endphp

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title', 'AdminLte')</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  @includeIf($_themeStyles)
</head>
<body class="hold-transition {!! config('adminlte.skin') !!} sidebar-mini">
  <div class="wrapper">

    @includeIf($_themeHeader)
    @includeIf($_themeSidebar)

    <div class="content-wrapper">
      <section class="content-header">
        @yield('content-header')
      </section>

      <section class="content">
        @yield('content')
      </section>
    </div>

    @includeIf($_themeFooter)

  </div>

  @includeIf($_themeScripts)
</body>
</html>
