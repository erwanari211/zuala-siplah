<header class="main-header">
<!-- Logo -->
<a href="{{ config('adminlte.logo_url') }}" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  {{-- <span class="logo-mini"><b>A</b>LT</span> --}}
  <span class="logo-mini">{!! config('adminlte.logo_mini') !!}</span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg">{!! config('adminlte.logo_lg') !!}</span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      @auth
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ asset('images/avatar-v01.png') }}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{ auth()->user()->name }}</span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
              <img src="{{ asset('images/avatar-v01.png') }}" class="img-circle" alt="User Image">
              <p> {{ auth()->user()->name }} </p>
            </li>
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="{{ route('logout') }}"
                  class="btn btn-default btn-flat"
                  onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out"></i> Sign out
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </div>
            </li>
          </ul>
        </li>
      @else
        <li>
          <a href="{{ route('login') }}"><i class="fa fa-sign-in"></i> Login</a>
        </li>
      @endauth
    </ul>
  </div>
</nav>
</header>
