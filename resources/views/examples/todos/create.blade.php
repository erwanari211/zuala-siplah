@extends('examples.todos.layout')

@section('title', 'Create Todo')

@section('content')
    <h3>{{ __('example_todo.forms.create_todo') }}</h3>

    @include('flash::message')
    @include('examples.todos.messages')

    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::open([
                'route' => ['examples.todos.store'],
                'method' => 'POST',
                'files' => false,
                ]) !!}

                {!! Form::bs3Text('name', null, ['label'=>__('example_todo.forms.name')]) !!}
                {!! Form::bs3Textarea('description', null, ['label'=>__('example_todo.forms.description')]) !!}

                {!! Form::bs3Submit(__('example_todo.forms.save')); !!}
                {!! Form::bs3LinkToRoute('examples.todos.index', __('example_todo.forms.back')); !!}

            {!! Form::close() !!}
        </div>
    </div>


@endsection
