@extends('examples.todos.layout')

@section('title', 'Edit Todo')

@section('content')
    <h3>{{ __('example_todo.forms.edit_todo') }}</h3>

    @include('flash::message')
    @include('examples.todos.messages')

    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::model($todo, [
                'route' => ['examples.todos.update', $todo->id],
                'method' => 'PUT',
                'files' => false,
                ]) !!}

                {!! Form::bs3Text('name', null, ['label'=>__('example_todo.forms.name')]) !!}
                {!! Form::bs3Textarea('description', null, ['label'=>__('example_todo.forms.description')]) !!}
                {!! Form::bs3Select('completed', [1=>'Complete', 0=>'Incomplete'], null, ['label'=>__('example_todo.forms.status')]) !!}

                {!! Form::bs3Submit(__('example_todo.forms.edit')); !!}
                {!! Form::bs3LinkToRoute('examples.todos.index', __('example_todo.forms.back')); !!}

            {!! Form::close() !!}
        </div>
    </div>


@endsection
