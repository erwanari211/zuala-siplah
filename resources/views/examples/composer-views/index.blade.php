@extends('examples.composer-views.layout')

@section('title', 'Index')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h3>Content</h3>
                <p><strong>{{ $posts }}</strong></p>

                @for ($i = 0; $i < 10; $i++)
                    <h3>
                        <a href="{{ action('Examples\ComposerViewsController@show', [$i]) }}">
                            Random Post {{ $i }}
                        </a>
                    </h3>
                    <p>
                        Random content {{ $i }}
                    </p>
                @endfor
            </div>
            <div class="col-sm-4">
                @include('examples.composer-views.sidebar')
            </div>

        </div>
    </div>
@endsection


