<h3>Sidebar</h3>

<hr>

<div class="list-group">
    <h4>Categories</h4>
    @foreach ($categories as $categoryName)
        @php
            $categoryUrl = action('Examples\ComposerViewsController@filterByCategory', [$categoryName]);
            $isActive = '';
            if (isset($category)) {
                $isActive = ($categoryName == $category) ? 'active' : '';
            }
        @endphp
        <a href="{{ $categoryUrl }}" class="list-group-item {{ $isActive }}">{{ $categoryName }}</a>
    @endforeach
</div>

<div class="list-group">
    <h4>Tags</h4>
    @foreach ($tags as $tagName)
        <a href="#" class="list-group-item">{{ $tagName }}</a>
    @endforeach
</div>
