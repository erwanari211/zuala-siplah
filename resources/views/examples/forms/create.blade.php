@extends('examples.forms.layout')

@section('title', 'Create Form')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                {!! Form::open([
                    'action' => ['Examples\FormController@store'],
                    'method' => 'POST',
                    'files' => false,
                    ]) !!}

                    {!! Form::bs3Text('name') !!}
                    {!! Form::bs3Email('email') !!}
                    {!! Form::bs3Password('password') !!}
                    {!! Form::bs3Textarea('description') !!}
                    {!! Form::bs3Select('gender', ['M'=>'Male', 'F'=>'Female']) !!}

                    {!! Form::bs3Submit('Save'); !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

