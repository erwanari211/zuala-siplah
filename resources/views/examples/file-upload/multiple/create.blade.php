@extends('examples.forms.layout')

@section('title', 'Create Form')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                @include('flash::message')

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        @foreach ($errors->all() as $error)
                            {{ $error }} <br>
                        @endforeach
                    </div>
                @endif

                {!! Form::open([
                    'action' => ['Examples\MultipleFileUploadController@store'],
                    'method' => 'POST',
                    'files' => true,
                    ]) !!}

                    {!! Form::bs3File('file[]', ['multiple'=>'multiple', 'label'=>'File']) !!}

                    {!! Form::bs3Submit('Upload'); !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

