@php
    $marketplace = $data['marketplace'];
    $owner = $data['user'];
@endphp

@component('mail::message')
# Marketplace created

Thank you for joining {{ config('app.name') }}.
Your marketplace created but still inactive.
Waiting approval from admin.
We will send you email when your marketplace approved.

**Marketplace Detail**<br/>
Name : **{{ $marketplace->name }}**<br/>
Owner : **{{ $owner->name }}**<br/>

@component('mail::button', ['url' => route('marketplaces.show', $marketplace->slug)])
View marketplace
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
