@php
    $marketplace = $data['marketplace'];
    $store = $data['store'];
    $owner = $data['owner'];
@endphp

@component('mail::message')
# Store approved

Congratulation. Your store is approved.
Thank you for joining {{ config('app.name') }}.

**Store Detail**<br/>
Name : **{{ $store->name }}**<br/>
Owner : **{{ $owner->name }}**<br/>
Status : Active<br/>

@component('mail::button', ['url' => route('stores.show', [$marketplace->slug, $store->slug])])
View store
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
