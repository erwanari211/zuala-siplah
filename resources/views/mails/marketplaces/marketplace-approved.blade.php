@php
    $marketplace = $data['marketplace'];
    $owner = $data['marketplaceOwner'];
@endphp

@component('mail::message')
# Marketplace approved

Congratulation. Your marketplace is approved.
Thank you for joining {{ config('app.name') }}.

**Marketplace Detail**<br/>
Name : **{{ $marketplace->name }}**<br/>
Owner : **{{ $owner->name }}**<br/>
Status : Active<br/>

@component('mail::button', ['url' => route('marketplaces.show', $marketplace->slug)])
View marketplace
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
