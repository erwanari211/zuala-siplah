@php
    $marketplace = $data['marketplace'];
    $store = $data['store'];
    $owner = $data['user'];
@endphp

@component('mail::message')
# Store created

Thank you for joining {{ config('app.name') }}.
Your store created but still inactive.
Waiting approval from marketplace admin.
We will send you email when your store approved.

**Store Detail**<br/>
Name : **{{ $store->name }}**<br/>
Owner : **{{ $owner->name }}**<br/>

@component('mail::button', ['url' => route('stores.show', [$marketplace->slug, $store->slug])])
View store
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
