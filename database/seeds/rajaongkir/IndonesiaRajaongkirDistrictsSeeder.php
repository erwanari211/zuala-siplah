<?php

namespace database\seeds\rajaongkir;

use Illuminate\Database\Seeder;
use DB;

class IndonesiaRajaongkirDistrictsSeeder extends Seeder
{
    public function run()
    {
        $table = 'indonesia_district_rajaongkir';
        $file = base_path().'/database/seeds/rajaongkir/csv/indonesia_rajaongkir_districts.csv';
        $header = array('indonesia_district_id', 'rajaongkir_district_id', 'is_exact');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        foreach($collection->chunk(50) as $chunk) {
            DB::table($table)->insert($chunk->toArray());
        }
    }
}
