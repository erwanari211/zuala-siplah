<?php

namespace database\seeds\rajaongkir;

use Illuminate\Database\Seeder;
use DB;

class RajaongkirCitiesSeeder extends Seeder
{
    public function run()
    {
        $table = 'rajaongkir_cities';
        $file = base_path().'/database/seeds/rajaongkir/csv/rajaongkir_cities.csv';
        $header = array('city_id', 'province_id', 'type', 'city_name', 'postal_code');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        foreach($collection->chunk(50) as $chunk) {
            DB::table($table)->insert($chunk->toArray());
        }
    }
}
