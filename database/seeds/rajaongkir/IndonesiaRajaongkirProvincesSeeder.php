<?php

namespace database\seeds\rajaongkir;

use Illuminate\Database\Seeder;
use DB;

class IndonesiaRajaongkirProvincesSeeder extends Seeder
{
    public function run()
    {
        $table = 'indonesia_province_rajaongkir';
        $file = base_path().'/database/seeds/rajaongkir/csv/indonesia_rajaongkir_provinces.csv';
        $header = array('indonesia_province_id', 'rajaongkir_province_id');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        foreach($collection->chunk(50) as $chunk) {
            DB::table($table)->insert($chunk->toArray());
        }
    }
}
