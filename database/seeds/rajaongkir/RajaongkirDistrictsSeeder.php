<?php

namespace database\seeds\rajaongkir;

use Illuminate\Database\Seeder;
use DB;

class RajaongkirDistrictsSeeder extends Seeder
{
    public function run()
    {
        $table = 'rajaongkir_districts';
        $file = base_path().'/database/seeds/rajaongkir/csv/rajaongkir_districts.csv';
        $header = array('district_id', 'city_id', 'district_name');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        foreach($collection->chunk(50) as $chunk) {
            DB::table($table)->insert($chunk->toArray());
        }
    }
}
