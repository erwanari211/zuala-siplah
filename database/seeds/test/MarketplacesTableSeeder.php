<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\User;
use DB;

class MarketplacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Marketplace::truncate();
        // DB::table('marketplace_promos')->truncate();
        // DB::table('marketplace_promo_details')->truncate();
        DB::table('marketplace_settings')->truncate();

        $admins = User::where('username', 'like', '%mp_admin%')->get();
        $imageDirectory = 'images';
        $pathDirectory = public_path().'/'.$imageDirectory;

        $index = 1;
        if (count($admins)) {
            foreach ($admins as $admin) {
                $marketplace = new Marketplace;
                $marketplace->user_id = $admin->id;
                $marketplace->name = 'mp'.$index.' '.$faker->sentence(1);
                $marketplace->slug = str_slug($marketplace->name);
                $marketplace->image = $imageDirectory .'/no-image-v2.png';
                $marketplace->logo = $imageDirectory .'/no-image-v2.png';
                $marketplace->active = true;
                $marketplace->save();

                $index++;
            }
        }
    }
}
