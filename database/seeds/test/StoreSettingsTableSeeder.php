<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreSetting;
use DB;

class StoreSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        StoreSetting::truncate();
        $stores = Store::get();
        $keys = ['store_tagline', 'store_description', 'location', 'order_prefix'];

        foreach ($stores as $store) {
            $setting = new StoreSetting;
            $setting->store_id = $store->id;
            $setting->group = 'settings';
            $setting->key = 'store_tagline';
            $setting->value = $faker->sentence(1);
            $setting->save();

            $setting = new StoreSetting;
            $setting->store_id = $store->id;
            $setting->group = 'settings';
            $setting->key = 'store_description';
            $setting->value = $faker->sentence(3);
            $setting->save();

            $setting = new StoreSetting;
            $setting->store_id = $store->id;
            $setting->group = 'settings';
            $setting->key = 'location';
            $setting->value = null;
            $setting->save();

            $setting = new StoreSetting;
            $setting->store_id = $store->id;
            $setting->group = 'settings';
            $setting->key = 'order_prefix';
            $setting->value = null;
            $setting->save();
        }
    }
}
