<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection;
use App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail;
use DB;

class WebsiteMarketplaceCollectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $marketplaces = Marketplace::get();
        $featuredMarketplaces = $faker->randomElements($marketplaces, 3);

        $collection = new WebsiteMarketplaceCollection;
        $collection->name = 'featured marketplaces';
        $collection->display_name = 'featured marketplaces';
        $collection->save();

        foreach ($featuredMarketplaces as $marketplace) {
            $detail = new WebsiteMarketplaceCollectionDetail;
            $detail->marketplace_id = $marketplace->id;
            $detail->website_marketplace_collection_id = $collection->id;
            $detail->save();
        }
    }
}
