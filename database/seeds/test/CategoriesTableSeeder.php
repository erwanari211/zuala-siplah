<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Category;
use App\Models\Marketplace;
use DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $totalItem = 3;
        Category::truncate();

        $categories = [
            [
                ['Clothing', 'Shoes', 'Accessories'],
                ['Make-up', 'Skin care', 'Hair Care', 'Personal Care'],
            ],
            [
                ['Phone', 'Camera', 'Video Game'],
                ['PC', 'PS4', 'PS3', 'Xbox'],
                ['Computer Component', 'Laptop', 'Data Storage'],
            ],
            [
                ['Children Book', 'Literature & Fiction', 'Scifi & fantasy', 'Educational Book'],
            ],

        ];

        $marketplaces = Marketplace::get();
        $index = 0;
        foreach ($marketplaces as $marketplace) {
            $storeCategories = $categories[$index];
            $imageDirectory = 'images';
            $pathDirectory = public_path().'/'.$imageDirectory;

            foreach ($storeCategories as $subcategories) {
                foreach ($subcategories as $categoryName) {
                    $category = new Category;
                    $category->user_id = $marketplace->user_id;
                    $category->name = $categoryName;
                    $category->slug = str_slug($categoryName);
                    $category->description = $faker->paragraph(4, true);
                    $category->image = $imageDirectory .'/no-image-v2.png';
                    $category->active = true;

                    $marketplace->categories()->save($category);
                }
            }

            $index++;
        }

    }
}
