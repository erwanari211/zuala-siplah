<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\ProductAttribute;
use DB;

class ProductAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductAttribute::truncate();
        $products = Product::get();

        $totalAttribute = 3;
        $itemCount = 0;
        foreach ($products as $product) {
            $groups = ['group 1', 'group 2'];
            foreach ($groups as $group) {
                for ($i=1; $i <= $totalAttribute; $i++) {
                    $attribute = new ProductAttribute;
                    $attribute->product_id = $product->id;
                    $attribute->group = $group;
                    $attribute->key = 'key '.$i;
                    $attribute->value = 'value '.$i;
                    $attribute->sort_order = $i;
                    $attribute->save();
                }
            }

            $itemCount++;
            if ($itemCount % 10 == 0) {
                dump($itemCount.' product(s)');
            }
        }
    }
}
