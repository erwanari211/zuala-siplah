<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreAddress;
use DB;

class StoreAddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        StoreAddress::truncate();

        // random address
        $randomAddresses = [
            ['province_id'=>'32', 'city_id'=>'3204', 'district_id'=>'3204180'],
            ['province_id'=>'32', 'city_id'=>'3204', 'district_id'=>'3204180'],
            ['province_id'=>'31', 'city_id'=>'3173', 'district_id'=>'3173010'],
            ['province_id'=>'32', 'city_id'=>'3204', 'district_id'=>'3204180'],
            ['province_id'=>'33', 'city_id'=>'3310', 'district_id'=>'3310730'],
        ];

        $stores = Store::get();
        foreach ($stores as $store) {
            $randomAddress = $faker->randomElement($randomAddresses);

            $address = new StoreAddress;
            $address->label = 'Store address';
            $address->full_name = $store->name;
            $address->address = $faker->sentence(4, true);
            $address->province_id = $randomAddress['province_id'];
            $address->city_id = $randomAddress['city_id'];
            $address->district_id = $randomAddress['district_id'];
            $address->default_address = true;
            $address->phone = 123;
            $address->postcode = 123;

            $store->addresses()->save($address);
        }
    }
}
