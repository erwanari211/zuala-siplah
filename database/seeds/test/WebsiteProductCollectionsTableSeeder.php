<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\WebsiteProductCollection;
use App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail;
use DB;

class WebsiteProductCollectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $products = Product::get();
        $featuredProducts = $faker->randomElements($products, 12);

        $collection = new WebsiteProductCollection;
        $collection->name = 'featured products';
        $collection->display_name = 'featured products';
        $collection->save();

        foreach ($featuredProducts as $product) {
            $detail = new WebsiteProductCollectionDetail;
            $detail->product_id = $product->id;
            $detail->website_product_collection_id = $collection->id;
            $detail->save();
        }
    }
}
