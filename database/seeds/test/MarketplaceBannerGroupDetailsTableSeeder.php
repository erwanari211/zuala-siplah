<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail;
use DB;

class MarketplaceBannerGroupDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        MarketplaceBannerGroupDetail::truncate();
        $marketplaces = Marketplace::get();

        foreach ($marketplaces as $marketplace) {
            $imageDirectory = 'images';
            $pathDirectory = public_path().'/'.$imageDirectory;
            $totalItem = 4;
            $banners = $marketplace->banners;
            $bannerGroups = $marketplace->bannerGroups;
            foreach ($bannerGroups as $bannerGroup) {
                $randomBanners = $faker->randomElements($banners, $totalItem);
                $index = 1;
                foreach ($randomBanners as $banner) {
                    $detail = new MarketplaceBannerGroupDetail;
                    $detail->marketplace_banner_group_id = $bannerGroup->id;
                    $detail->marketplace_banner_id = $banner->id;
                    $detail->sort_order = $index;
                    $detail->save();
                    $index++;
                }
            }
        }
    }
}
