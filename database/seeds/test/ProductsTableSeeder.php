<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\Store;
use DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $totalItem = 50;

        Product::truncate();

        DB::table('marketplace_category_product')->truncate();
        DB::table('grouped_product_details')->truncate();

        DB::table('orders')->truncate();
        DB::table('order_histories')->truncate();
        DB::table('order_metas')->truncate();
        DB::table('order_products')->truncate();

        DB::table('product_activity_views')->truncate();
        DB::table('product_discussions')->truncate();
        DB::table('product_discussion_details')->truncate();
        DB::table('product_reviews')->truncate();
        DB::table('product_review_attachments')->truncate();

        $stores = Store::get();
        $imageDirectory = 'images';
        $pathDirectory = public_path().'/'.$imageDirectory;
        $itemCount = 0;
        foreach ($stores as $store) {
            $marketplace = $store->marketplace;
            $categories = $marketplace->categories;
            foreach ($categories as $category) {
                $totalItem = $faker->numberBetween(3, 5);
                for ($i=0; $i < $totalItem; $i++) {
                    $product = new Product;
                    $product->unique_code = Product::randomUniqueCode();
                    $product->store_id = $store->id;
                    $product->user_id = $store->user_id;
                    $product->name = $faker->sentence(4, true);
                    // $product->slug = $faker->slug();
                    $product->slug = Product::getUniqueSlug(str_slug($product->name));
                    $product->description = $faker->paragraph(4, true);
                    $product->price = $faker->numberBetween(15, 150) * 1000;
                    $product->image = $imageDirectory .'/no-image-v2.png';
                    $product->qty = 100;
                    $product->active = true;
                    $product->status = 'active';
                    $product->views = $faker->numberBetween(200, 500);
                    $product->weight = $faker->numberBetween(10, 250) * 10;
                    $product->save();

                    $category->products()->attach($product);

                    $itemCount++;
                    if ($itemCount % 10 == 0) {
                        dump($itemCount.' product(s)');
                    }
                }
            }

        }
    }
}
