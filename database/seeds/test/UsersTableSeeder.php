<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;
use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $totalItem = 3;
        $imageDirectory = 'images';
        $pathDirectory = public_path().'/'.$imageDirectory;

        for ($i=1; $i <= $totalItem; $i++) {
            $user = new User;
            $user->name = $faker->name;
            $user->username = 'mp_admin'.$i;
            $user->email = 'mp_admin'.$i.'@app.com';
            $user->password = bcrypt(12345678);
            $user->photo = $imageDirectory .'/no-image-v2.png';
            $user->save();
        }

        $totalItem = 10;
        for ($i=1; $i <= $totalItem; $i++) {
            $user = new User;
            $user->name = $faker->name;
            $user->username = 'store_admin'.$i;
            $user->email = 'store_admin'.$i.'@app.com';
            $user->password = bcrypt(12345678);
            $user->photo = $imageDirectory .'/no-image-v2.png';
            $user->save();
        }

        $totalItem = 25;
        for ($i=1; $i <= $totalItem; $i++) {
            $user = new User;
            $user->name = $faker->name;
            $user->username = 'customer'.$i;
            $user->email = 'customer'.$i.'@app.com';
            $user->password = bcrypt(12345678);
            $user->photo = $imageDirectory .'/no-image-v2.png';
            $user->save();
        }
    }
}
