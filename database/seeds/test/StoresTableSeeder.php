<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Marketplace;
use App\User;
use DB;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Store::truncate();
        DB::table('store_settings')->truncate();

        $admins = User::where('username', 'like', '%store_admin%')->get();
        $imageDirectory = 'images';
        $pathDirectory = public_path().'/'.$imageDirectory;
        $marketplaces = Marketplace::pluck('id');

        $index = 1;
        foreach ($admins as $admin) {
            $store = new Store;
            $store->marketplace_id = $faker->randomElement($marketplaces);
            $store->user_id = $admin->id;
            $store->name = 'store'.$index.' '.$faker->sentence(1);
            $store->slug = str_slug($store->name);
            $store->image = $imageDirectory .'/no-image-v2.png';
            $store->active = true;
            $store->save();

            $index++;
        }
    }
}
