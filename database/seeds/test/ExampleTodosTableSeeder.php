<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Examples\ExampleTodo;
use DB;

class ExampleTodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $totalItem = 10;
        ExampleTodo::truncate();
        for ($i=0; $i < 10; $i++) {
            $todo = new ExampleTodo;
            $todo->name = $faker->sentence(4, true);
            $todo->description = $faker->sentence(50, true);
            $todo->save();
        }
    }
}
