<?php

namespace database\seeds;

use Illuminate\Database\Seeder;

class RajaongkirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('database\seeds\rajaongkir\RajaongkirProvincesSeeder');
        $this->call('database\seeds\rajaongkir\RajaongkirCitiesSeeder');
        $this->call('database\seeds\rajaongkir\RajaongkirDistrictsSeeder');

        $this->call('database\seeds\rajaongkir\IndonesiaRajaongkirProvincesSeeder');
        $this->call('database\seeds\rajaongkir\IndonesiaRajaongkirCitiesSeeder');
        $this->call('database\seeds\rajaongkir\IndonesiaRajaongkirDistrictsSeeder');
    }
}
