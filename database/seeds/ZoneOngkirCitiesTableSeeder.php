<?php

use Illuminate\Database\Seeder;

class ZoneOngkirCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = RajaOngkir::Kota()->all();
        DB::table('zone_ongkir_cities')->truncate();
        DB::table('zone_ongkir_cities')->insert($data);
    }
}
