<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('store_id');
            $table->string('label')->default('Store address');
            $table->string('full_name');
            $table->string('company')->nullable();
            $table->text('address');
            $table->integer('province_id')->unsigned()->default(0);
            $table->integer('city_id')->unsigned()->default(0);
            $table->string('phone')->nullable();
            $table->string('postcode')->nullable();
            $table->boolean('default_address')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_addresses');
    }
}
