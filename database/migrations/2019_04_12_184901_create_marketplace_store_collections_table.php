<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketplaceStoreCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_store_collections', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('marketplace_id');
            $table->string('name');
            $table->string('display_name')->nullable();
            $table->boolean('active')->default(true);
            $table->string('type')->default('normal');
            $table->unsignedInteger('sort_order')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_store_collections');
    }
}
