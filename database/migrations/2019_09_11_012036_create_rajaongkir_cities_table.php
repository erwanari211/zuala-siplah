<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRajaongkirCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rajaongkir_cities', function (Blueprint $table) {
            $table->bigIncrements('city_id');
            $table->unsignedBigInteger('province_id');
            $table->string('type');
            $table->string('city_name');
            $table->unsignedInteger('postal_code');

            $table->index('province_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rajaongkir_cities');
    }
}
