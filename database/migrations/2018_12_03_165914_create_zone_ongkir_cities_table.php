<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZoneOngkirCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zone_ongkir_cities', function (Blueprint $table) {
            $table->integer('city_id')->unsigned();
            $table->integer('province_id')->unsigned();
            $table->string('province');
            $table->string('type');
            $table->string('city_name');
            $table->integer('postal_code')->unsigned();

            $table->primary('city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zone_ongkir_cities');
    }
}
