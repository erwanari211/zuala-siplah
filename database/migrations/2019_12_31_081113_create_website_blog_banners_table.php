<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsiteBlogBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_blog_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('link')->nullable();
            $table->boolean('active')->default(true);
            $table->string('type')->default('banner');
            $table->text('note')->nullable();
            $table->unsignedInteger('sort_order')->default(1);
            $table->string('position')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_blog_banners');
    }
}
