<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndonesiaCityKemdikbudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indonesia_city_kemdikbud', function (Blueprint $table) {
            $table->char('indonesia_city_id', 15);
            $table->char('kemdikbud_city_id', 15);

            $table->index('indonesia_city_id');
            $table->index('kemdikbud_city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indonesia_city_kemdikbud');
    }
}
