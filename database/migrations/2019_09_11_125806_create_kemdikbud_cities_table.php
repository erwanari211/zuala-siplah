<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKemdikbudCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kemdikbud_cities', function (Blueprint $table) {
            $table->char('id', 15);
            $table->char('province_id', 15);
            $table->string('name');
            $table->unsignedInteger('zone');

            $table->primary('id');
            $table->index('province_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kemdikbud_cities');
    }
}
