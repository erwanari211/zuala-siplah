<?php

/**
* change plain number to formatted currency
*
* @param $number
* @param $currency
*/
if (!function_exists('formatNumber')) {
    function formatNumber($number = 0, $currency = 'IDR')
    {
        if ($currency == 'USD') {
            return number_format($number, 2, '.', ',');
        }

        return number_format($number, 0, ',', '.');
    }
}

if (!function_exists('upload_file')) {
    function upload_file($file, $directory = 'uploads', $newFileName = null)
    {
        $originalName = $file->getClientOriginalName();
        $filename = $newFileName ? $newFileName : pathinfo($originalName, PATHINFO_FILENAME);
        $ext = $file->getClientOriginalExtension();
        $newFileName = str_slug($filename).'-'.str_random(8).'-'.time().'.'.$ext;
        $dir = $directory;
        $file->move($dir,$newFileName);
        $filepath = $dir.'/'.$newFileName;

        return $filepath;
    }
}

if (!function_exists('resize_image')) {
    function resize_image($filepath, $width = 300, $height = 200)
    {
        $path_parts = pathinfo($filepath);
        $ext = $path_parts['extension'];
        $thumbnailPath = str_replace(".{$ext}", "-thumb-{$width}x{$height}.{$ext}", $filepath);
        \Image::make($filepath)->resize($width, $height)->save($thumbnailPath);
        return $thumbnailPath;
    }
}

if (!function_exists('meta')) {
    function meta($name, $content = '', $type = 'name', $newline = 'n')
    {
        $output = '';
        $output .= "<meta {$type}=\"{$name}\" content=\"{$content}\" />";
        return $output;
    }
}

if (!function_exists('n_tag')) {
    function n_tag($repeat = 1)
    {
        $output = '';
        for ($i=0; $i < $repeat; $i++) {
            $output .= "\n";
        }
        return $output;
    }
}

if (!function_exists('t_tag')) {
    function t_tag($repeat = 1)
    {
        $output = '';
        for ($i=0; $i < $repeat; $i++) {
            $output .= "\t";
        }
        return $output;
    }
}

if (!function_exists('nbsp')) {
    function nbsp($repeat = 1, $html = false)
    {
        $output = '';
        for ($i=0; $i < $repeat; $i++) {
            $output .= $html ? '&nbsp;' : "  ";
        }
        return $output;
    }
}

if (!function_exists('adminlte_box_open')) {
    function adminlte_box_open($title = 'Box', $boxType = 'default', $settings = [])
    {
        $showDefaultBoxTools = isset($settings['showDefaultBoxTools']) ? $settings['showDefaultBoxTools'] : true;
        $customBoxTools = isset($settings['customBoxTools']) ? $settings['customBoxTools'] : '';

        $output = ''.n_tag();
        $output .= nbsp().'<div class="box box-'.$boxType.'">'.n_tag();
        $output .= nbsp(2).'<div class="box-header with-border">'.n_tag();
        $output .= nbsp(3).'<h3 class="box-title">'.$title.'</h3>'.n_tag();

        if ($showDefaultBoxTools) {
            $output .= nbsp(3).'<div class="box-tools pull-right">'.n_tag();
            $output .= nbsp(4).'<button type="button" class="btn btn-box-tool" data-widget="collapse">'.n_tag();
            $output .= nbsp(5).'<i class="fa fa-minus"></i>'.n_tag();
            $output .= nbsp(4).'</button>'.n_tag();
            $output .= nbsp(4).'<button type="button" class="btn btn-box-tool" data-widget="remove">'.n_tag();
            $output .= nbsp(5).'<i class="fa fa-times"></i>'.n_tag();
            $output .= nbsp(4).'</button>'.n_tag();
            $output .= nbsp(3).'</div>'.n_tag();
        } else {
            $output .= nbsp(3).'<div class="box-tools pull-right">'.n_tag();
            $output .= $customBoxTools;
            $output .= nbsp(3).'</div>'.n_tag();
        }

        $output .= nbsp(2).'</div>'.n_tag();
        $output .= nbsp(2).'<!-- /.box-header -->'.n_tag();
        return $output;
    }
}

if (!function_exists('adminlte_box_close')) {
    function adminlte_box_close()
    {
        $output = ''.n_tag();
        $output .= nbsp().'</div>'.n_tag();
        $output .= nbsp().'<!-- /.box -->'.n_tag();
        return $output;
    }
}

if (!function_exists('adminlte_get_sidebar_menu')) {
    function adminlte_get_sidebar_menu($menuArray, $activeMenu, $title = '')
    {
        $output = '';
        $output .= "<ul class=\"sidebar-menu\" data-widget=\"tree\">";
        if ($title) {
            $output .= "<li class=\"header\">{$title}</li>";
        }
        foreach ($menuArray as $menu){
            $output .= adminlte_get_sidebar_menu_item($menu, $activeMenu);
        }
        $output .= "</ul>";
        return $output;
    }
}



if (!function_exists('adminlte_get_sidebar_menu_item')) {
    function adminlte_get_sidebar_menu_item($menu = [], $activeMenu = [])
    {
        $icon = isset($menu['icon']) ? $menu['icon'] : 'fa fa-bars';
        $label = isset($menu['label']) ? $menu['label'] : 'Menu';
        $url = isset($menu['url']) ? $menu['url'] : 'javascript:void(0)';

        $treeview = isset($menu['treeview']) ? $menu['treeview'] : true;
        $treeviewClass = $treeview ? 'treeview' : '';

        $hasSubmenu = (isset($menu['submenu']) && !empty($menu['submenu'])) ? true : false;

        $menuName = isset($menu['name']) ? $menu['name'] : null;
        $isActiveClass = in_array($menuName, $activeMenu) ? 'active' : '';

        $output = '';
        $output .= "<li class=\"{$treeviewClass} {$isActiveClass}\">";
        $output .= "  <a href=\"{$url}\">";
        $output .= "    <i class=\"{$icon}\"></i>";
        $output .= "    <span>{$label}</span>";
        if ($hasSubmenu && $treeview) {
            $output .= "  <span class=\"pull-right-container\">";
            $output .= "    <i class=\"fa fa-angle-left pull-right\"></i>";
            $output .= "  </span>";
        }
        $output .= "  </a>";
        if ($hasSubmenu) {
            $output .= "  <ul class=\"treeview-menu\">";
            foreach ($menu['submenu'] as $subMenu) {
                $output .= adminlte_get_sidebar_submenu_item($subMenu, $activeMenu);
            }
            $output .= "  </ul>";
        }

        $output .= "  </li>";
        return $output;
    }
}

if (!function_exists('adminlte_get_sidebar_submenu_item')) {
    function adminlte_get_sidebar_submenu_item($menu = [], $activeMenu = [])
    {
        $icon = isset($menu['icon']) ? $menu['icon'] : 'fa fa-bars';
        $label = isset($menu['label']) ? $menu['label'] : 'Menu';
        $url = isset($menu['url']) ? $menu['url'] : 'javascript:void(0)';

        $menuName = isset($menu['name']) ? $menu['name'] : null;
        $isActiveClass = in_array($menuName, $activeMenu) ? 'active' : '';

        $output = '';
        $output .= "<li class=\"{$isActiveClass}\">";
        $output .= "  <a href=\"{$url}\">";
        $output .= "   <i class=\"{$icon}\"></i>";
        $output .= "   <span>{$label}</span>";
        $output .= "  </a>";
        $output .= "</li>";

        return $output;
    }
}

function link_to_route_html($name, $html, $parameters = array(), $attributes = array())
{
    $url = route($name, $parameters);
    return '<a href="'.$url.'"'.app('html')->attributes($attributes).'>'.$html.'</a>';
}

if (!function_exists('floatvalue')) {
    function floatvalue($val = 0)
    {
        $val = str_replace(",",".",$val);
        $val = preg_replace('/\.(?=.*\.)/', '', $val);
        return floatval($val);
    }
}

if (!function_exists('get_filename')) {
    function get_filename($filepath)
    {
        $path_parts = pathinfo($filepath);
        $filename = $path_parts['basename'];
        return $filename;
    }
}

if (!function_exists('get_rajaongkir_couriers')) {
    function get_rajaongkir_couriers()
    {
        $couriers = [
            'jne' => 'Jalur Nugraha Ekakurir (JNE)',
            'pos' => 'POS Indonesia (POS)',
            'tiki' => 'Citra Van Titipan Kilat (TIKI)',
            'rpx' => 'RPX Holding (RPX)',
            'esl' => 'Eka Sari Lorena (ESL)',
            'pcp' => 'Priority Cargo and Package (PCP)',
            'pandu' => 'Pandu Logistics (PANDU)',
            'wahana' => 'Wahana Prestasi Logistik (WAHANA)',
            'sicepat' => 'SiCepat Express (SICEPAT)',
            'jnt' => 'J&T Express (J&T)',
            'pahala' => 'Pahala Kencana Express (PAHALA)',
            'sap' => 'SAP Express (SAP)',
            'jet' => 'JET Express (JET)',
            'dse' => '21 Express (DSE)',
            'slis' => 'Solusi Ekspres (SLIS)',
            'first' => 'First Logistics (FIRST)',
            'ncs' => 'Nusantara Card Semesta (NCS)',
            'star' => 'Star Cargo (STAR)',
            'ninja' => 'Ninja Xpress (NINJA)',
            'lion' => 'Lion Parcel (LION)',
            'idl' => 'IDL Cargo (IDL)',
            'rex' => 'Royal Express Indonesia (REX)',
        ];
        return $couriers;
    }
}
