<?php

namespace App\Http\Requests\Examples;

use Illuminate\Foundation\Http\FormRequest;

class TodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:example_todos',
            'description' => 'required',
        ];

        switch ($this->method()) {
            case 'PUT':
            case 'PATCH':
                $todo = $this->route('todo');
                $rules['name'] = 'required|unique:example_todos,name,'.$todo->id;
                $rules['completed'] = 'required|integer';
                break;
            default:

                break;
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The todo field is required',
            // 'description.required'  => 'Todo description is required',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'description' => 'todo description',
            'completed' => 'status',
        ];
    }
}
