<?php

namespace App\Http\Controllers\Modules\Wilayah\Examples;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Indonesia;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage;

class WilayahIndonesiaController extends Controller
{
    public function index()
    {
        // return Indonesia::allProvinces();

        // return Indonesia::paginateProvinces($numRows = 15);

        // return Indonesia::allCities();

        // return Indonesia::paginateCities($numRows = 15);

        // return Indonesia::allDistricts();

        // return Indonesia::paginateDistricts($numRows = 15);

        // return Indonesia::allVillages();

        // return Indonesia::paginateVillages($numRows = 15);

        // $provinceId = 32; // Jawa Barat
        // return Indonesia::findProvince($provinceId, ['cities']);
        // return Indonesia::findProvince($provinceId, ['cities.districts']);

        // $cityId = 3204; // Kabupaten Bandung
        // return Indonesia::findCity($cityId, ['province', 'districts']);

        // $districtId = 3204190; // Soreang
        // return Indonesia::findDistrict($districtId, ['villages']);

        // $villageId = 3204190020; // Sekarwangi
        // return Indonesia::findVillage($villageId, ['district.city.province']);

        // return Indonesia::search('jakarta')->all();

        // return Indonesia::search('jakarta')->allProvinces();

        // return Indonesia::search('jakarta')->paginateProvinces();

        // return Indonesia::search('jakarta')->allCities();

        // return Indonesia::search('jakarta')->paginateCities();

        // return Indonesia::search('baru')->allDistricts();

        // return Indonesia::search('baru')->paginateDistricts();

        // return Indonesia::search('bandung')->allVillages();

        // return Indonesia::search('bandung')->paginateVillages();
    }

    public function index2()
    {
        // return IndonesiaProvince::get();
        // return IndonesiaProvince::paginate(15);

        // return IndonesiaCity::get();
        // return IndonesiaCity::paginate(15);

        // return IndonesiaDistrict::get();
        // return IndonesiaDistrict::paginate(15);

        // return IndonesiaVillage::get();
        // return IndonesiaVillage::paginate(15);

        // $provinceId = '32'; // Jawa Barat
        // return IndonesiaProvince::with('cities', 'cities.districts')->find($provinceId);

        // $cityId = '3204'; // Kabupaten Bandung
        // return IndonesiaCity::with('province', 'districts')->find($cityId);

        // $districtId = '3204190'; // Soreang
        // return IndonesiaDistrict::with('villages')->find($districtId);

        // $villageId = '3204190020'; // Sekarwangi
        // return IndonesiaVillage::with('district.city.province')->find($villageId);

        // return IndonesiaProvince::search('jakarta')->get();
        // return IndonesiaProvince::search('jakarta')->paginate(15);

        // return IndonesiaCity::search('jakarta')->get();
        // return IndonesiaCity::search('jakarta')->paginate(15);

        // return IndonesiaDistrict::search('baru')->get();
        // return IndonesiaDistrict::search('baru')->paginate(15);

        // return IndonesiaVillage::search('bandung')->get();
        // return IndonesiaVillage::search('bandung')->paginate(15);
    }

    public function indexKemdikbud()
    {
        // return IndonesiaProvince::with('kemdikbudProvince')->get();

        // return IndonesiaProvince::withCount('kemdikbudProvince')
        //     ->pluck('kemdikbud_province_count', 'name');

        // return IndonesiaProvince::with('kemdikbudProvince')
        //     ->has('kemdikbudProvince')
        //     ->count();

        // return IndonesiaProvince::with('kemdikbudProvince')
        //     ->doesnthave('kemdikbudProvince')
        //     ->count();

        // return IndonesiaCity::with('kemdikbudCity')->paginate(10);

        // return IndonesiaCity::with('kemdikbudCity')
        //     ->has('kemdikbudCity')
        //     ->count();

        // return IndonesiaCity::with('kemdikbudCity')
        //     ->doesnthave('kemdikbudCity')
        //     ->count();

        // return IndonesiaDistrict::with('kemdikbudDistrict')->paginate(10);

        // return IndonesiaDistrict::with('kemdikbudDistrict')
        //     ->has('kemdikbudDistrict')
        //     ->count();

        // return IndonesiaDistrict::with('kemdikbudDistrict', 'city', 'city.province')
        //     ->doesnthave('kemdikbudDistrict')
        //     ->paginate(25);
    }

    public function indexRajaongkir()
    {
        // return IndonesiaProvince::with('rajaongkirProvince')->get();

        // return IndonesiaProvince::with('rajaongkirProvince')
        //     ->has('rajaongkirProvince')
        //     ->count();

        // return IndonesiaProvince::with('rajaongkirProvince')
        //     ->doesnthave('rajaongkirProvince')
        //     ->count();

        // return IndonesiaCity::with('rajaongkirCity')->paginate(10);

        // return IndonesiaCity::with('rajaongkirCity')
        //     ->has('rajaongkirCity')
        //     ->count();

        // return IndonesiaCity::with('rajaongkirCity', 'province')
        //     ->doesnthave('rajaongkirCity')
        //     ->paginate(25);

        // $cities = IndonesiaCity::withCount('rajaongkirCity')->get();
        // $keyed = $cities->mapWithKeys(function ($item) {
        //     $rajaongkir_city_count = $item['rajaongkir_city_count'];
        //     $name = $item['name'];
        //     return [$name => [
        //         'id' => $item['id'],
        //         'count' => $rajaongkir_city_count,
        //     ]];
        // });
        // $citiesWithRelations = [];
        // $cities = $keyed->all();
        // foreach ($cities as $city => $item) {
        //     if ($item['count'] != 1) {
        //         $citiesWithRelations[] = IndonesiaCity::with('rajaongkirCity')->find($item['id']);
        //     }
        // }
        // return $citiesWithRelations;

        // return IndonesiaDistrict::with('rajaongkirDistrict')->paginate(10);

        // return IndonesiaDistrict::with('rajaongkirDistrict')
        //     ->has('rajaongkirDistrict')
        //     ->count();

        // return IndonesiaDistrict::with('rajaongkirDistrict', 'city', 'city.province')
        //     ->doesnthave('rajaongkirDistrict')
        //     ->count();

        // $districts = IndonesiaDistrict::withCount('rajaongkirDistrict')->get();
        // $keyed = $districts->mapWithKeys(function ($item) {
        //     $rajaongkir_district_count = $item['rajaongkir_district_count'];
        //     $name = $item['name'];
        //     return [$name => [
        //         'id' => $item['id'],
        //         'count' => $rajaongkir_district_count,
        //     ]];
        // });
        // $districtsWithRelations = [];
        // $districts = $keyed->all();

        // foreach ($districts as $district => $item) {
        //     if ($item['count'] != 1) {
        //         $districtsWithRelations[] = IndonesiaDistrict::with('rajaongkirDistrict')->find($item['id']);
        //     }
        // }
        // return collect($districtsWithRelations);
    }
}
