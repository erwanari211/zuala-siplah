<?php

namespace App\Http\Controllers\Modules\Wilayah\Examples;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince;
use App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity;
use App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict;

class KemdikbudWilayahController extends Controller
{
    public function index()
    {
        // $provinces = KemdikbudProvince::get();
        // return $provinces;

        // $provinces = KemdikbudProvince::search('jawa')->get();
        // return $provinces;

        // $provinceId = '020000'; // Jawa Barat
        // $province = KemdikbudProvince::find($provinceId);
        // $province->load('cities', 'cities.districts');
        // return $province;

        // $cities = KemdikbudCity::get();
        // return $cities;

        // $cities = KemdikbudCity::search('bandung')->get();
        // return $cities;

        // $cityId = '020800'; // Kab. Bandung
        // $city = KemdikbudCity::find($cityId);
        // $city->load('districts');
        // return $city;

        // $districts = KemdikbudDistrict::take(10)->get();
        // return $districts;

        // $districts = KemdikbudDistrict::with('city', 'city.province')->search('soreang')->get();
        // return $districts;

        // $districtId = '020819';
        // $district = KemdikbudDistrict::find($districtId);
        // $district->load('city', 'city.province');
        // return $district;
    }

    public function index2()
    {
        // $provinceId = '020000'; // Jawa Barat
        // $province = KemdikbudProvince::find($provinceId);
        // $indonesiaProvince = $province->indonesiaProvince ? $province->indonesiaProvince->first() : null;
        // return $indonesiaProvince;

        // return KemdikbudProvince::with('indonesiaProvince')
        //     ->doesnthave('indonesiaProvince')
        //     ->get();

        // return KemdikbudProvince::with('indonesiaProvince')
        //     ->has('indonesiaProvince')
        //     ->get();

        // return KemdikbudCity::with('indonesiaCity')
        //     ->has('indonesiaCity')
        //     ->get();

        // return KemdikbudCity::with('indonesiaCity')
        //     ->has('indonesiaCity')
        //     ->count();

        // return KemdikbudCity::with('indonesiaCity')
        //     ->doesnthave('indonesiaCity')
        //     ->get();

        // return KemdikbudDistrict::paginate(10);

        // return KemdikbudDistrict::with('indonesiaDistrict')
        //     ->has('indonesiaDistrict')
        //     ->count();

        // return KemdikbudDistrict::with('indonesiaDistrict')
        //     ->has('indonesiaDistrict')
        //     ->paginate(10);

        // return KemdikbudDistrict::with('indonesiaDistrict')
        //     ->doesnthave('indonesiaDistrict')
        //     ->count();
    }
}
