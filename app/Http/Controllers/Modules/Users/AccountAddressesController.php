<?php

namespace App\Http\Controllers\Modules\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ZoneOngkirProvince;
use App\Models\ZoneOngkirCity;
use App\Models\UserAddress;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;

class AccountAddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $addresses = $user->addresses()->with('province', 'city', 'district')->get();
        $hasDefaultStoreAddress = $user->hasDefaultStoreAddress();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.user-accounts.backend.addresses.index', compact(
            'user', 'addresses',
            'hasDefaultStoreAddress',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        /*
        $provinces = ZoneOngkirProvince::getProvinceWithCities();
        $provinceId = old('province_id');
        $dropdown['provinces'] = ZoneOngkirProvince::pluck('province', 'province_id');
        $dropdown['cities'] =  ZoneOngkirCity::getDropdown($provinceId);
        */

        // \Cache::forget('provincesWithDetails');
        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id');
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id');
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.user-accounts.backend.addresses.create', compact(
            'user', 'provinces', 'dropdown',
            'provincesWithDetails',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'label' => 'required',
            'full_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'postcode' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
        ]);

        $address = new UserAddress;
        $address->label = request('label');
        $address->full_name = request('full_name');
        $address->company = request('company');
        $address->address = request('address');
        $address->province_id = request('province_id');
        $address->city_id = request('city_id');
        $address->district_id = request('district_id');
        $address->phone = request('phone');
        $address->postcode = request('postcode');
        $address->default_address = false;

        $user = auth()->user();
        $user->addresses()->save($address);

        if ($user->addresses()->count() == 1) {
            $address->default_address = true;
            $address->save();

            if ($address->default_address) {
                $city = $address->city;
                $kemdikbudCity = $city->kemdikbudCity->first();
                $zone = $kemdikbudCity->zone;
                session(['kemdikbud_zone' => $zone]);
            }
        }

        flash('Address saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UserAddress $address)
    {
        $user = auth()->user();
        if (!$address->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.addresses.index');
        }

        /*
        $provinces = ZoneOngkirProvince::getProvinceWithCities();
        $provinceId = old('province_id');
        $dropdown['provinces'] = ZoneOngkirProvince::pluck('province', 'province_id');
        $dropdown['cities'] =  ZoneOngkirCity::getDropdown($provinceId);
        */

        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id', $address->province_id);
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id', $address->city_id);
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.user-accounts.backend.addresses.edit', compact(
            'user', 'address', 'provinces', 'dropdown',
            'provincesWithDetails',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserAddress $address)
    {
        request()->validate([
            'label' => 'required',
            'full_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'postcode' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'default_address' => 'required',
        ]);

        $address->label = request('label');
        $address->full_name = request('full_name');
        $address->company = request('company');
        $address->address = request('address');
        $address->province_id = request('province_id');
        $address->city_id = request('city_id');
        $address->district_id = request('district_id');
        $address->phone = request('phone');
        $address->postcode = request('postcode');
        $address->default_address = request('default_address');
        $address->save();

        $user = $address->user;
        if ($address->default_address) {
            $user->addresses()->update(['default_address'=>false]);
            $address->default_address = true;
            $address->save();
        }

        if ($address->default_address) {
            $city = $address->city;
            $kemdikbudCity = $city->kemdikbudCity->first();
            $zone = $kemdikbudCity->zone;
            session(['kemdikbud_zone' => $zone]);
        }

        flash('Address updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserAddress $address)
    {
        $user = auth()->user();
        if (!$address->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.addresses.index');
        }

        $address->delete();

        flash('Address deleted');
        return redirect()->back();
    }
}
