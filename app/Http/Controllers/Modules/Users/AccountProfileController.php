<?php

namespace App\Http\Controllers\Modules\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteSettings;

class AccountProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $hasDefaultStoreAddress = $user->hasDefaultStoreAddress();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.user-accounts.backend.profiles.index', compact(
            'user',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        request()->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username,'.$user->id,
            'email' => 'required|email|unique:users,email,'.$user->id,
        ]);

        $user->name = request('name');
        $user->username = strtolower(request('username'));
        $user->email = request('email');
        $user->save();

        $image = request('image');
        if ($image) {
            $filename = 'user-photo';
            $filepath = upload_file($image, 'uploads', $filename);
            $user->photo = $filepath;
            $user->save();
        }

        flash('Account updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
