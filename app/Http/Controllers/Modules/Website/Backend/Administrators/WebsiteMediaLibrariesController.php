<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteMediaLibrary;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteSettings;

class WebsiteMediaLibrariesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $medias = WebsiteMediaLibrary::paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.media-libraries.index', compact(
            'marketplace', 'banners', 'medias', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['groups'] = [
            'image' => 'Image',
            'video' => 'Video',
            'document' => 'Document',
            'other' => 'Other'
        ];

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.media-libraries.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'group' => 'required',
            'file' => 'required|max:20000',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $file = request('file');
        if ($file) {
            $filename = 'website-medias';
            $filepath = upload_file($file, 'uploads', $filename);

            $media = new WebsiteMediaLibrary;
            $media->unique_code = $media->randomUniqueCode();
            $media->group = request('group');
            $media->name = request('name');
            $media->description = request('description');
            $media->tags = request('tags');
            $media->active = request('active');
            $media->is_public = request('is_public');

            $media->mimetype = $file->getClientMimeType();
            $media->url = $filepath;
            $media->save();

            flash('Media saved');

        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteMediaLibrary  $websiteMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteMediaLibrary $websiteMediaLibrary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteMediaLibrary  $websiteMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteMediaLibrary $mediaLibrary)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['groups'] = [
            'image' => 'Image',
            'video' => 'Video',
            'document' => 'Document',
            'other' => 'Other'
        ];

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.media-libraries.edit', compact(
            'user', 'dropdown', 'mediaLibrary',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteMediaLibrary  $websiteMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteMediaLibrary $mediaLibrary)
    {
        request()->validate([
            'group' => 'required',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $media = $mediaLibrary;
        $media->group = request('group');
        $media->name = request('name');
        $media->description = request('description');
        $media->tags = request('tags');
        $media->active = request('active');
        $media->is_public = request('is_public');
        $media->save();

        flash('Media updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteMediaLibrary  $websiteMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteMediaLibrary $mediaLibrary)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $mediaLibrary->delete();

        flash('Media deleted');
        return redirect()->back();
    }
}
