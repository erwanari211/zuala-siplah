<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteBanner;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteBannerGroup;
use App\Models\Modules\Website\WebsiteSettings;

class WebsiteBannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $banners = WebsiteBanner::latest()->paginate(10);
        $dropdown['banner_groups'] = WebsiteBannerGroup::active()->pluck('name', 'id');

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.banners.index', compact(
            'marketplace', 'banners', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.banners.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'image' => 'image|required|max:4000',
            'link' => 'url|nullable',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $image = request('image');
        if ($image) {
            $filename = 'website-banners';
            $filepath = upload_file($image, 'uploads', $filename);
        }

        $banner = new WebsiteBanner;
        $banner->image = $filepath;
        $banner->link = request('link');
        $banner->active = request('active');
        $banner->expired_at = request('expired_at');
        $banner->note = request('note');
        $banner->sort_order = request('sort_order');
        $banner->save();

        flash('Banner saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBanner  $websiteBanner
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteBanner $websiteBanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBanner  $websiteBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteBanner $banner)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['banner_types'] = [
            'banner' => 'Banner',
            'video' => 'Video',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.banners.edit', compact(
            'user', 'banner', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteBanner  $websiteBanner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteBanner $banner)
    {
        request()->validate([
            'image' => 'image|max:4000',
            'link' => 'url|nullable|required_if:type,video',
            'sort_order' => 'required|integer|min:1',
            'type' => 'required|in:banner,video',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $type = request('type');
        if ($type == 'video') {
            // check link is video
        }

        $banner->link = request('link');
        $banner->active = request('active');
        $banner->expired_at = request('expired_at');
        $banner->note = request('note');
        $banner->sort_order = request('sort_order');
        $banner->type = request('type');
        $banner->save();

        $image = request('image');
        if ($image) {
            $filename = 'website-banners';
            $filepath = upload_file($image, 'uploads', $filename);
            $banner->image = $filepath;
            $banner->save();
        }

        $isExpired = false;
        $now = date("Y-m-d");
        if ($now > $banner->expired_at) {
            $isExpired = true;
        }
        if ($isExpired) {
            $banner->active = false;
            $banner->save();
        }

        flash('Banner updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteBanner  $websiteBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteBanner $banner)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        // delete from group banner
        $banner->delete();

        flash('Banner deleted');
        return redirect()->back();
    }
}
