<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteBannerGroup;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteSettings;

class WebsiteBannerGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $bannerGroups = WebsiteBannerGroup::latest()->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.banner-groups.index', compact(
            'bannerGroups',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = [
            'normal' => 'Normal',
            'slider' => 'Slider',
            'small' => 'Small',
            'big' => 'Big',
            'popup' => 'Popup',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.banner-groups.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $group = new WebsiteBannerGroup;
        $group->name = request('name');
        $group->display_name = request('display_name');
        $group->active = request('active');
        $group->type = request('type');
        $group->sort_order = request('sort_order');
        $group->save();

        flash('Banner group saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBannerGroup  $websiteBannerGroup
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteBannerGroup $bannerGroup)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal', 'slider'=>'Slider'];
        $details = $bannerGroup->details()->sort()->with('websiteBanner')->get();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.banner-groups.show', compact(
            'user', 'bannerGroup', 'dropdown', 'details',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBannerGroup  $websiteBannerGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteBannerGroup $bannerGroup)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = [
            'normal' => 'Normal',
            'slider' => 'Slider',
            'small' => 'Small',
            'big' => 'Big',
            'popup' => 'Popup',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.banner-groups.edit', compact(
            'user', 'bannerGroup', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteBannerGroup  $websiteBannerGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteBannerGroup $bannerGroup)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $group = $bannerGroup;
        $group->name = request('name');
        $group->display_name = request('display_name');
        $group->active = request('active');
        $group->type = request('type');
        $group->sort_order = request('sort_order');
        $group->save();

        flash('Banner updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteBannerGroup  $websiteBannerGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteBannerGroup $bannerGroup)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $bannerGroup->details()->delete();
        $bannerGroup->delete();

        flash('Banner group deleted');
        return redirect()->back();
    }
}
