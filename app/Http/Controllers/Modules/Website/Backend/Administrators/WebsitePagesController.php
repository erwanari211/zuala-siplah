<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsitePage;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteLayoutElement;
use App\Models\Modules\Website\WebsiteSettings;

class WebsitePagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $pages = WebsitePage::latest()->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.pages.index', compact(
            'marketplace', 'pages',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.pages.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'active' => 'required|integer',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $page = new WebsitePage;
        $page->title = request('title');
        $page->slug = str_slug(request('slug'));
        $page->content = request('content');
        $page->active = request('active');
        $page->save();

        $slugCount = WebsitePage::where('slug', $page->slug)->count();
        if ($slugCount > 1) {
            flash('Slug is not unique')->warning();
        }

        flash('Page saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function show(WebsitePage $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsitePage $page)
    {
        // return $page;
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.pages.edit', compact(
            'user', 'page', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsitePage $page)
    {
        request()->validate([
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'active' => 'required|integer',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $page->title = request('title');
        $page->slug = str_slug(request('slug'));
        $page->content = request('content');
        $page->active = request('active');
        $page->save();

        $slugCount = WebsitePage::where('slug', $page->slug)->count();
        if ($slugCount > 1) {
            flash('Slug is not unique')->warning();
        }

        flash('Page updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsitePage $page)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $page->delete();

        flash('Page deleted');
        return redirect()->back();
    }
}
