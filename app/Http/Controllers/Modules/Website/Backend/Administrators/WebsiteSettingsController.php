<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteSettings;
use Illuminate\Http\Request;

class WebsiteSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteSettings  $websiteSettings
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteSettings $websiteSettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteSettings  $websiteSettings
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $settings = WebsiteSettings::group('settings')->get();
        $settings = $settings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.settings.edit', compact(
            'settings',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteSettings  $websiteSettings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // return request()->all();
        request()->validate([
            'favicon' => 'mimes:png',
            'logo' => 'mimes:png',
            'facebook_url' => 'nullable|url',
            'twitter_url' => 'nullable|url',
            'youtube_url' => 'nullable|url',
            'instagram_url' => 'nullable|url',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $input = request()->except(['_method', '_token']);

        foreach ($input as $key => $value) {
            WebsiteSettings::updateOrCreate([
                'group' => 'settings',
                'key' => $key,
            ], [
                'value' => $value
            ]);
        }

        $favicon = request('favicon');

        if ($favicon) {
            $filename = 'website-favicon';
            $filepath = upload_file($favicon, 'uploads', $filename);
            WebsiteSettings::updateOrCreate([
                'group' => 'settings',
                'key' => 'favicon',
            ], [
                'value' => $filepath
            ]);
        }

        $logo = request('logo');

        if ($logo) {
            $filename = 'website-logo';
            $filepath = upload_file($logo, 'uploads', $filename);
            WebsiteSettings::updateOrCreate([
                'group' => 'settings',
                'key' => 'logo',
            ], [
                'value' => $filepath
            ]);
        }

        flash('Settings Saved');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteSettings  $websiteSettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteSettings $websiteSettings)
    {
        //
    }
}
