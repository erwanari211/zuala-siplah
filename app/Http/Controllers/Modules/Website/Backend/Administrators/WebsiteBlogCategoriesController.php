<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteBlogCategory;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteSettings;

class WebsiteBlogCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $categories = WebsiteBlogCategory::latest()->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-categories.index', compact(
            'categories',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-categories.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required|unique:website_blog_categories',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $slugCount = WebsiteBlogCategory::where('slug', str_slug(request('slug')))->count();
        if ($slugCount > 1) {
            flash('Slug is not unique')->warning();
            return redirect()->back();
        }

        $category = new WebsiteBlogCategory;
        $category->name = request('name');
        $category->slug = str_slug(request('slug'));
        $category->description = request('description');
        $category->save();

        flash('Category saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteBlogCategory $blog_category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteBlogCategory $blog_category)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $category = $blog_category;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-categories.edit', compact(
            'user', 'category',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteBlogCategory $blog_category)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required|unique:website_blog_categories,slug,'.$blog_category->id,
        ]);

        // return request()->all();

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $slugCount = WebsiteBlogCategory::where([
            'slug' => str_slug(request('slug')),
            ['id', '<>', $blog_category->id]
        ])->count();

        if ($slugCount >= 1) {
            flash('Slug is not unique')->warning();
            return redirect()->back()->withInput(request()->all());
        }

        $category = $blog_category;
        $category->name = request('name');
        $category->slug = str_slug(request('slug'));
        $category->description = request('description');
        $category->save();

        flash('Category updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteBlogCategory $blog_category)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $category = $blog_category;
        $category->posts()->detach();
        $category->delete();

        flash('Category deleted');
        return redirect()->back();
    }
}
