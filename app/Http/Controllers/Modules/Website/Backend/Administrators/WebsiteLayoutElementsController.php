<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteLayoutElement;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteSettings;

class WebsiteLayoutElementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $layoutElements = WebsiteLayoutElement::latest()->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.layout-elements.index', compact(
            'marketplace', 'layoutElements',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['groups'] = [
            'footer' => 'Footer'
        ];
        $dropdown['positions'] = [
            'footer.col-1' => 'Footer Column 1',
            'footer.col-2.a' => 'Footer Column 2-a',
            'footer.col-2.b' => 'Footer Column 2-b',
            'footer.col-3.a' => 'Footer Column 3-a',
            'footer.col-3.b' => 'Footer Column 3-b',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.layout-elements.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'group' => 'required',
            'position' => 'required',
            'name' => 'required',
            'active' => 'required|integer',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $element = new WebsiteLayoutElement;
        $element->group = request('group');
        $element->position = request('position');
        $element->name = request('name');
        $element->content = request('content');
        $element->active = request('active');
        $element->sort_order = request('sort_order');
        $element->save();

        flash('Layout Element saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteLayoutElement $websiteLayoutElement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteLayoutElement $layoutElement)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['groups'] = [
            'footer' => 'Footer'
        ];
        $dropdown['positions'] = [
            'footer.col-1' => 'Footer Column 1',
            'footer.col-2.a' => 'Footer Column 2-a',
            'footer.col-2.b' => 'Footer Column 2-b',
            'footer.col-3.a' => 'Footer Column 3-a',
            'footer.col-3.b' => 'Footer Column 3-b',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.layout-elements.edit', compact(
            'user', 'layoutElement', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteLayoutElement $layoutElement)
    {
        request()->validate([
            'group' => 'required',
            'position' => 'required',
            'name' => 'required',
            'active' => 'required|integer',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $element = $layoutElement;
        $element->group = request('group');
        $element->position = request('position');
        $element->name = request('name');
        $element->content = request('content');
        $element->active = request('active');
        $element->sort_order = request('sort_order');
        $element->save();

        flash('Layout Element updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteLayoutElement $layoutElement)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $layoutElement->delete();

        flash('Layout Element deleted');
        return redirect()->back();
    }
}
