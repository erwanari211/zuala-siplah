<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteBannerGroupDetail;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteBanner;
use App\Models\Modules\Website\WebsiteBannerGroup;

class WebsiteBannerGroupDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'banner_id' => 'required',
            'group_id' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $bannerId = request('banner_id');
        $banner = WebsiteBanner::findOrFail($bannerId);

        $groupId = request('group_id');
        $group = WebsiteBannerGroup::findOrFail($groupId);

        WebsiteBannerGroupDetail::updateOrCreate([
            'website_banner_group_id' => $group->id,
            'website_banner_id' => $banner->id,
        ], [
            'sort_order' => request('sort_order')
        ]);

        flash('Banner added to group');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBannerGroupDetail  $websiteBannerGroupDetail
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteBannerGroupDetail $websiteBannerGroupDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBannerGroupDetail  $websiteBannerGroupDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteBannerGroupDetail $websiteBannerGroupDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteBannerGroupDetail  $websiteBannerGroupDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteBannerGroupDetail $websiteBannerGroupDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteBannerGroupDetail  $websiteBannerGroupDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteBannerGroup $bannerGroup, WebsiteBanner $banner)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        WebsiteBannerGroupDetail::where([
            'website_banner_group_id' => $bannerGroup->id,
            'website_banner_id' => $banner->id,
        ])->delete();

        flash('Banner removed from group');
        return redirect()->back();
    }
}
