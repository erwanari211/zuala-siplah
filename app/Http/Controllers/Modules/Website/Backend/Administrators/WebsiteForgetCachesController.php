<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class WebsiteForgetCachesController extends Controller
{
    public function forgetCaches()
    {
        Cache::forget('websiteSettings');

        // homepage
        Cache::forget('bannerGroups');
        Cache::forget('marketplaceCollections');
        Cache::forget('productCollections');

        // footer
        Cache::forget('websiteFooterLayoutElements');

        // address
        Cache::forget('provincesWithDetails');

        // blog banners
        Cache::forget('blogBanners');

        flash('Cached deleted');
        return redirect()->back();
    }
}
