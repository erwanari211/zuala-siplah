<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteBlogBanner;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteSettings;

class WebsiteBlogBannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $banners = WebsiteBlogBanner::latest()->paginate(10);
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-banners.index', compact(
            'banners',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['positions'] = [
            'sidebar' => 'Sidebar',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-banners.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'image' => 'image|required|max:4000',
            'link' => 'url|nullable',
            'sort_order' => 'required|integer|min:1',
            'active' => 'required|boolean',
            'position' => 'required',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $image = request('image');
        if ($image) {
            $filename = 'website-blog-banners';
            $filepath = upload_file($image, 'uploads', $filename);
        }

        $banner = new WebsiteBlogBanner;
        $banner->image = $filepath;
        $banner->link = request('link');
        $banner->active = request('active');
        $banner->type = 'banner';
        $banner->note = request('note');
        $banner->sort_order = request('sort_order');
        $banner->position = request('position');
        $banner->save();

        WebsiteBlogBanner::forgetBlogBanners();

        flash('Banner saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBlogBanner  $websiteBlogBanner
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteBlogBanner $websiteBlogBanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBlogBanner  $websiteBlogBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteBlogBanner $blogBanner)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $banner = $blogBanner;

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['positions'] = [
            'sidebar' => 'Sidebar',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-banners.edit', compact(
            'user', 'banner', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteBlogBanner  $websiteBlogBanner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteBlogBanner $blogBanner)
    {
        request()->validate([
            'image' => 'image|max:4000',
            'link' => 'url|nullable',
            'sort_order' => 'required|integer|min:1',
            'active' => 'required|boolean',
            'position' => 'required',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $banner = $blogBanner;
        $banner->link = request('link');
        $banner->active = request('active');
        $banner->note = request('note');
        $banner->sort_order = request('sort_order');
        $banner->position = request('position');
        $banner->save();

        $image = request('image');
        if ($image) {
            $filename = 'website-blog-banners';
            $filepath = upload_file($image, 'uploads', $filename);
            $banner->image = $filepath;
            $banner->save();
        }

        WebsiteBlogBanner::forgetBlogBanners();

        flash('Banner updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteBlogBanner  $websiteBlogBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteBlogBanner $blogBanner)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $banner = $blogBanner;
        $banner->delete();

        WebsiteBlogBanner::forgetBlogBanners();

        flash('Banner deleted');
        return redirect()->back();
    }
}
