<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceCategory;
use App\Models\Modules\Marketplaces\MarketplaceActivitySearch;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplaceCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($marketplace, $category)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });

        $categories = $marketplace->categories()->where('parent_id', 0)->orderBy('sort_order')->get();
        $category = $marketplace->categories()->where('slug', $category)->firstOrFail();
        $subcategories = $category->subcategories();

        $popularSearches = $marketplace->promoPopularSearches()->orderBy('sort_order')->take(5)->get();

        $url = url()->current();
        $queryString = http_build_query(array_except(request()->query(), ['page', 'sort']));
        $sortBy = request('sort') ? request('sort') : 'newest';

        $products = $category->products()
            ->with('store')
            ->with('store.addresses')
            ->with('store.addresses', 'store.addresses.province', 'store.addresses.city')
            ->active()
            ->status('active');
        $products = $products->with('reviews');
        $products = $products->with('kemdikbudZonePrices');

        $search = request('search');
        if ($search) {
            $products = $products->search($search);

            MarketplaceActivitySearch::newSearch($search, [
                'marketplace_id' => $marketplace->id,
                'store_id' => null,
            ]);
        }

        if ($sortBy) {
            if ($sortBy == 'newest') {
                $products = $products->latest();
            }
            if ($sortBy == 'price_asc') {
                $products = $products->orderBy('price', 'asc');
            }
            if ($sortBy == 'price_desc') {
                $products = $products->orderBy('price', 'desc');
            }
        }

        $products = $products->paginate(20);

        $dropdown['sortBy'] = [
            'newest' => 'Newest',
            'price_asc' => 'Price: Low to High',
            'price_desc' => 'Price: High to Low',
        ];

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.marketplaces.categories.show', compact(
            'marketplace', 'marketplaceSettings',
            'popularSearches',
            'categories', 'category', 'products', 'subcategories',
            'url', 'queryString', 'sortBy', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
