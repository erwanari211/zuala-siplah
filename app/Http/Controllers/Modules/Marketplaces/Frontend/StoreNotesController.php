<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplace\StoreNote;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Store;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;

class StoreNotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($marketplace, $store)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $store->addView();
        $storeSettings = $store->mappedSettings();
        // $note = $store->notes()->findOrFail($note);

        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();

        $storeMenus = $store->menus()
            ->where('group', 'menu header')
            ->where('parent_id', 0)
            ->with(['children' => function($q){
                $q->orderBy('sort_order');
            }])
            ->orderBy('sort_order')
            ->get();
        $storeNotes = $store->notes()
            ->where('active', 1)
            ->paginate(10);

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.notes.index', compact(
            'marketplace', 'marketplaceSettings',
            'store', 'categories', 'collection', 'storeSettings',
            'storeMenus',
            'storeNotes',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplace\StoreNote  $storeNote
     * @return \Illuminate\Http\Response
     */
    public function show($marketplace, $store, $note)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $store->addView();
        $storeSettings = $store->mappedSettings();
        // $note = $store->notes()->findOrFail($note);
        $note = $store->notes()
            ->where('id', $note)
            ->where('active', 1)
            ->firstOrFail();

        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();

        $storeMenus = $store->menus()
            ->where('group', 'menu header')
            ->where('parent_id', 0)
            ->with(['children' => function($q){
                $q->orderBy('sort_order');
            }])
            ->orderBy('sort_order')
            ->get();
        $storeNotes = $store->notes()
            ->where('active', 1)
            ->paginate(10);

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.notes.show', compact(
            'marketplace', 'marketplaceSettings',
            'store', 'categories', 'collection', 'storeSettings',
            'storeMenus',
            'note', 'storeNotes',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplace\StoreNote  $storeNote
     * @return \Illuminate\Http\Response
     */
    public function edit(StoreNote $storeNote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplace\StoreNote  $storeNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StoreNote $storeNote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplace\StoreNote  $storeNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(StoreNote $storeNote)
    {
        //
    }
}
