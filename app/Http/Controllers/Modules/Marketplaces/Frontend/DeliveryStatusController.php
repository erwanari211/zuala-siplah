<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Rajaongkir\Waybill;
use Exception;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;

class DeliveryStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $couriers = get_rajaongkir_couriers();
        $availableWaybillCouriers = [
            'jne', 'pos', 'tiki', 'wahana', 'jnt',
            'rpx', 'sap', 'sicepat', 'pcp', 'jet',
            'dse', 'first', 'ninja', 'lion', 'idl',
            'rex'
        ];
        foreach ($couriers as $courierCode => $courierName) {
            if (in_array($courierCode, $availableWaybillCouriers)) {
                $dropdown['couriers'][$courierCode] = $courierName;
            }
        }

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.marketplaces.delivery-status.index', compact(
            'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexAjax()
    {
        $waybill = request('waybill');
        $courier = request('courier');

        $checkWaybill = new Waybill;
        try {
            $data = $checkWaybill->check([
                'waybill' => $waybill,
                'courier' => $courier,
            ]);
            $message = null;
            $success = true;
        } catch (Exception $e) {
            $data = null;
            $message = $e->getMessage();
            $success = false;
        }

        $result = compact('data', 'message', 'success');
        return $result;
    }
}
