<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\OrderProduct;

class AjaxStoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getStoreInfo($marketplace, $store)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();

        $storeViews = $store->views;
        $lastOnline = $store->getMeta('last_online');
        $lastLogin = $lastOnline ? date("Y-m-d H:i", strtotime($lastOnline)) : '-';
        $successfulTransactions = $store->orders()->where('order_status', 'completed')->count();
        $productSold = (int) OrderProduct::with('order')->whereHas('order', function($q) use ($store) {
            $q->where('store_id', $store->id);
        })->sum('qty');

        $data = compact('storeViews', 'lastLogin', 'successfulTransactions', 'productSold');
        return $data;
    }
}
