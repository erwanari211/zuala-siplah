<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreBanner;
use App\Models\Modules\Marketplaces\StoreBannerGroup;
use App\Models\Modules\Marketplaces\StoreBannerGroupDetail;

class StoreBannerGroupDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'banner_id' => 'required',
            'group_id' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $bannerId = request('banner_id');
        $banner = StoreBanner::findOrFail($bannerId);
        if (!$banner->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banners.index', $store->id);
        }

        $groupId = request('group_id');
        $group = StoreBannerGroup::findOrFail($groupId);
        if (!$group->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banner-groups.index', $store->id);
        }

        StoreBannerGroupDetail::updateOrCreate([
            'store_banner_group_id' => $group->id,
            'store_banner_id' => $banner->id,
        ], [
            'sort_order' => request('sort_order')
        ]);

        flash('Banner added to group');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreBannerGroup $bannerGroup, StoreBanner $banner)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$banner->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banners.index', $store->id);
        }

        if (!$bannerGroup->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banner-groups.index', $store->id);
        }

        StoreBannerGroupDetail::where([
            'store_banner_group_id' => $bannerGroup->id,
            'store_banner_id' => $banner->id,
        ])->delete();

        flash('Banner removed from group');
        return redirect()->back();
    }
}
