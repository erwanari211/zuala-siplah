<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreFeaturedProduct;
use App\Models\Modules\Website\WebsiteSettings;

class StoreFeaturedProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $featuredProducts = $store->storeFeaturedProducts()->latest()->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.featured-products.index', compact(
            'user', 'store', 'featuredProducts',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.featured-products.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'file' => 'image|max:4000',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $category = new StoreFeaturedProduct;
        $category->name = request('name');
        $category->slug = str_slug(request('name'));
        $category->description = request('description');
        $category->active = request('active');
        $category->store_id = $store->id;
        $category->save();

        $image = request('image');
        if ($image) {
            $filename = 'store-featured-products';
            $filepath = upload_file($image, 'uploads', $filename);
            $category->image = $filepath;
            $category->save();
        }

        flash('Featured product saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, StoreFeaturedProduct $featuredProduct)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$featuredProduct->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.featured-products.index', $store->id);
        }

        $products = $featuredProduct->details()->with('product')->get();
        $dropdown['products'] = $store->products()->orderBy('name')->pluck('name', 'id');
        $storeProducts = $store->products()->orderBy('name')->get();
        $storeProducts = $storeProducts->mapWithKeys(function($item){
            $id = $item['id'];
            $name = $item['name'];
            $sku = $item['sku'];
            $productName = $name . ( $sku ? " ($sku)" : "" );

            return [$id => $productName];
        });
        $dropdown['products'] = $storeProducts;
        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.featured-products.show', compact(
            'user', 'store', 'featuredProduct', 'dropdown', 'products',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreFeaturedProduct $featuredProduct)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$featuredProduct->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.featured-products.index', $store->id);
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.featured-products.edit', compact(
            'user', 'store', 'featuredProduct', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreFeaturedProduct $featuredProduct)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'file' => 'image|max:4000',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$featuredProduct->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.featured-products.index', $store->id);
        }

        $category = $featuredProduct;
        $category->name = request('name');
        $category->slug = str_slug(request('slug'));
        $category->description = request('description');
        $category->active = request('active');
        $category->save();

        $image = request('image');
        if ($image) {
            $filename = 'store-featured-products';
            $filepath = upload_file($image, 'uploads', $filename);
            $category->image = $filepath;
            $category->save();
        }

        flash('Featured product updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreFeaturedProduct $featuredProduct)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$featuredProduct->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.featured-products.index', $store->id);
        }

        $featuredProduct->delete();

        flash('Featured product deleted');
        return redirect()->back();
    }
}
