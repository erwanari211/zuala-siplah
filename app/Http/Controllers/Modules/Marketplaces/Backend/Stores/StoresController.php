<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\ProductActivityView;
use DB;
use App\Models\Modules\Marketplaces\MarketplaceActivitySearch;
use App\Models\Modules\Website\WebsiteSettings;

class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $store->updateLastOnline();

        $store->has_addresses = $store->addresses()->count() ? true : false;
        $store->has_default_addresses = $store->addresses()->defaultAddress()->count() ? true : false;
        $store->is_active = $store->active ? true : false;
        $store->has_shipping_methods = $store->shippingMethods()->where('active', true)->count() ? true : false;
        $marketplace = $store->marketplace;

        $oneWeekAgo = date('Y-m-d', strtotime('-1 week'));
        $mostViewedProductsThisWeek = ProductActivityView::groupBy('product_id')
            ->select('product_id', DB::raw('count(*) as total_views'))
            ->with('product')
            ->has('product')
            ->where('store_id', $store->id)
            ->whereDate('viewed_at', '>=', $oneWeekAgo)
            ->orderBy('total_views', 'desc')
            ->take(10)
            ->get();

        $oneMonthAgo = date('Y-m-d', strtotime('-1 month'));
        $mostViewedProductsThisMonth = ProductActivityView::groupBy('product_id')
            ->select('product_id', DB::raw('count(*) as total_views'))
            ->with('product')
            ->has('product')
            ->where('store_id', $store->id)
            ->whereDate('viewed_at', '>=', $oneMonthAgo)
            ->orderBy('total_views', 'desc')
            ->take(10)
            ->get();

        $oneWeekAgo = date('Y-m-d', strtotime('-1 week'));
        $popularSearchThisWeek = MarketplaceActivitySearch::groupBy('search')
            ->select('search', DB::raw('count(*) as total_search'))
            ->where('marketplace_id', $marketplace->id)
            ->whereDate('viewed_at', '>=', $oneWeekAgo)
            ->orderBy('total_search', 'desc')
            ->take(10)
            ->get();

        $oneMonthAgo = date('Y-m-d', strtotime('-1 month'));
        $popularSearchThisMonth = MarketplaceActivitySearch::groupBy('search')
            ->select('search', DB::raw('count(*) as total_search'))
            ->where('marketplace_id', $marketplace->id)
            ->whereDate('viewed_at', '>=', $oneMonthAgo)
            ->orderBy('total_search', 'desc')
            ->take(10)
            ->get();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.index', compact(
            'user', 'store', 'marketplace',
            'mostViewedProductsThisWeek', 'mostViewedProductsThisMonth',
            'popularSearchThisWeek', 'popularSearchThisMonth',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.profile.edit', compact(
            'store',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required',
            'image' => 'image|max:4000',
        ]);


        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $store->updateLastOnline();

        $store->name = request('name');
        $slug = request('slug') ? str_slug(request('slug')) : str_slug(request('name'));
        $store->slug = Store::getUniqueSlug($slug, $store->id);
        $store->save();

        $image = request('image');
        if ($image) {
            $filename = 'store-profile-pictures';
            $filepath = upload_file($image, 'uploads', $filename);
            $store->image = $filepath;
            $store->save();
        }

        flash('Store updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
