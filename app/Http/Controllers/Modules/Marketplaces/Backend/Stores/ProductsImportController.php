<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\ProductAttribute;
use App\Imports\ToCollectionImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Modules\Website\WebsiteSettings;

class ProductsImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $totalProducts = $store->products()->count();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.import.create', compact(
            'user', 'store',
            'totalProducts',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $request = request()->all();

        $file = request('file');
        if ($file) {
            $import = new ToCollectionImport;
            Excel::import($import, $file);

            $excelData = $import->getData();

            $products = $this->processProductData($excelData);
            $storedData = $this->storeData($products, $store);

            $message = 'Product imported '.$storedData['success']['total'].'/'.$storedData['totalItems'];
            flash($message);
            if ($storedData['failed']['total'] > 0) {
                $message = '';
                foreach ($storedData['failed']['data'] as $failedItem) {
                    $message .= 'Product with SKU '.$failedItem['sku'].' not imported ';
                    $message .= ($failedItem['message']) ? '('.$failedItem['message'].')' : '';
                    $message .= '<br>';
                }
                flash($message)->warning();
            }
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $request = request()->all();

        $file = request('file');
        if ($file) {
            $import = new ToCollectionImport;
            Excel::import($import, $file);

            $excelData = $import->getData();

            $products = $this->processUpdateProductData($excelData);
            $updatedData = $this->updateData($products, $store);

            $message = 'Product imported '.$updatedData['success']['total'].'/'.$updatedData['totalItems'];
            flash($message);
            if ($updatedData['failed']['total'] > 0) {
                $message = '';
                foreach ($updatedData['failed']['data'] as $failedItem) {
                    $message .= 'Product with SKU '.$failedItem['sku'].' not imported ';
                    $message .= ($failedItem['message']) ? '('.$failedItem['message'].')' : '';
                    $message .= '<br>';
                }
                flash($message)->warning();
            }
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function processProductData($data)
    {
        $products = [];
        $attributes = [];

        foreach ($data['products'] as $product) {
            if ($product['sku']) {
                $products[$product['sku']] = $product;
            }
        }

        foreach ($data['product_attributes'] as $attribute) {
            if ($attribute['sku']) {
                $attributes[$attribute['sku']][] = $attribute;
            }
        }

        foreach ($attributes as $attributeKey => $attribute) {
            if (isset($products[$attributeKey])) {
                $products[$attributeKey]['attributes'] = $attribute;
            }
        }

        return $products;
    }

    public function processUpdateProductData($data)
    {
        $products = [];
        $attributes = [];

        foreach ($data['products'] as $product) {
            if ($product['unique_code']) {
                $products[$product['unique_code']] = $product;
            }
        }

        foreach ($data['product_attributes'] as $attribute) {
            if ($attribute['unique_code']) {
                $attributes[$attribute['unique_code']][] = $attribute;
            }
        }

        foreach ($attributes as $attributeKey => $attribute) {
            if (isset($products[$attributeKey])) {
                $products[$attributeKey]['attributes'] = $attribute;
            }
        }

        return $products;
    }

    public function storeData($data, Store $store)
    {
        $results = [];
        $results['totalItems'] = 0;
        $results['success']['total'] = 0;
        $results['success']['data'] = [];
        $results['failed']['total'] = 0;
        $results['failed']['data'] = [];

        foreach ($data as $item) {
            $results['totalItems']++;
            $products = Product::where([
                'store_id' => $store->id,
                'sku' => $item['sku'],
            ])->get();

            if (count($products)) {
                echo "sku already exist. overwrite".'<br>';
                $results['failed']['total']++;
                $results['failed']['data'][] = [
                    'sku' => $item['sku'],
                    'message' => 'SKU already exist',
                ];
            } else {
                $rules = [
                    'name' => 'required',
                    'description' => 'required',
                    'price' => 'required|integer|min:0',
                    'qty' => 'required|integer|min:0',
                    'weight' => 'required|integer|min:0',
                    'active' => 'required|integer',
                ];

                $validateditem = $item->toArray();
                $validator = \Validator::make($validateditem, $rules);
                if($validator->fails()){
                    echo "not valid".'<br>';
                    $results['failed']['total']++;
                    $results['failed']['data'][] = [
                        'sku' => $item['sku'],
                        'message' => 'data not valid',
                    ];
                } else {
                    $product = new Product;
                    $product->user_id = auth()->user()->id;
                    $product->store_id = $store->id;
                    $product->sku = $item['sku'];
                    $product->unique_code = Product::randomUniqueCode();
                    $product->name = $item['name'];
                    $product->slug = Product::getUniqueSlug(str_slug($item['name']));
                    $product->description = $item['description'];
                    $product->price = $item['price'];
                    $product->qty = $item['qty'];
                    $product->weight = $item['weight'];
                    $product->active = $item['active'];
                    $product->status = 'active';

                    $imageResult = $this->processImage($item['image']);
                    if ($imageResult['success']) {
                        $product->image = $imageResult['data'];
                    }
                    $product->save();

                    if (isset($item['attributes'])) {
                        $this->storeProductAttributes($product, $item['attributes']);
                    }

                    echo "create new product ".$item['sku'].'<br>';
                    $results['success']['total']++;
                    $results['success']['data'][] = [
                        'sku' => $item['sku'],
                        'message' => 'product created',
                    ];
                }
            }
        }
        return $results;
    }

    public function updateData($data, Store $store)
    {
        $results = [];
        $results['totalItems'] = 0;
        $results['success']['total'] = 0;
        $results['success']['data'] = [];
        $results['failed']['total'] = 0;
        $results['failed']['data'] = [];

        foreach ($data as $item) {
            $results['totalItems']++;
            $products = Product::where([
                'store_id' => $store->id,
                'unique_code' => $item['unique_code'],
            ])->get();

            if (count($products) == 1) {
                $rules = [
                    'name' => 'required',
                    'description' => 'required',
                    'price' => 'required|integer|min:0',
                    'qty' => 'required|integer|min:0',
                    'weight' => 'required|integer|min:0',
                    'active' => 'required|integer',
                ];

                $validateditem = $item->toArray();
                $validator = \Validator::make($validateditem, $rules);
                if($validator->fails()){
                    echo "not valid".'<br>';
                    $results['failed']['total']++;
                    $results['failed']['data'][] = [
                        'unique_code' => $item['unique_code'],
                        'message' => 'data not valid',
                    ];
                } else {
                    $product = Product::where([
                        'store_id' => $store->id,
                        'unique_code' => $item['unique_code'],
                    ])->first();

                    $product->name = $item['name'];
                    $product->sku = $item['sku'];
                    $product->description = $item['description'];
                    $product->price = $item['price'];
                    $product->qty = $item['qty'];
                    $product->weight = $item['weight'];
                    $product->active = $item['active'];

                    $imageResult = $this->processImage($item['image']);
                    if ($imageResult['success']) {
                        $product->image = $imageResult['data'];
                    }
                    $product->save();

                    if (isset($item['attributes'])) {
                        $this->updateProductAttributes($product, $item['attributes']);
                    }

                    $this->updateProductCategory($product, $store, $item);
                    $this->updateProductCollection($product, $store, $item);

                    echo "updated product ".$item['unique_code'].'<br>';
                    $results['success']['total']++;
                    $results['success']['data'][] = [
                        'unique_code' => $item['unique_code'],
                        'message' => 'product updated',
                    ];
                }
            } else {
                echo "SKU is required and must be unique".'<br>';
                $results['failed']['total']++;
                $results['failed']['data'][] = [
                    'unique_code' => $item['unique_code'],
                    'message' => 'SKU is required and must be unique',
                ];
            }
        }
        return $results;
    }

    public function processImage($url)
    {
        $result = [
            'success' => false,
            'message' => null,
            'data' => null,
        ];

        $file_headers = @get_headers($url);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
            $result['success'] = false;
            $result['message'] = 'Invalid url';
        } else {
            $fileContent = file_get_contents($url);

            $name = pathinfo($url, PATHINFO_BASENAME);
            $filename = 'product';
            $ext = pathinfo($name, PATHINFO_EXTENSION);
            $newFileName = str_slug($filename).'-'.str_random(8).'-'.time().($ext ? '.'.$ext : '');

            $tempdir = 'temp';
            $newName = $tempdir.'/'.$newFileName;
            file_put_contents($newName, $fileContent);
            $tempfile = $newName;

            $allowedMimeTypes = ['image/jpeg','image/gif','image/png','image/bmp'];
            $contentType = mime_content_type($tempfile);
            if (in_array($contentType, $allowedMimeTypes)) {
                if (!$ext) {
                    if ($contentType == 'image/jpeg') { $ext = 'jpg'; }
                    if ($contentType == 'image/gif') { $ext = 'gif'; }
                    if ($contentType == 'image/png') { $ext = 'png'; }
                    if ($contentType == 'image/bmp') { $ext = 'bmp'; }
                }

                $newFileName = str_slug($filename).'-'.str_random(8).'-'.time().($ext ? '.'.$ext : '');
                $dir = 'uploads';
                $newName = $dir.'/'.$newFileName;
                file_put_contents($newName, $fileContent);
                $filepath = $newName;

                $result['success'] = true;
                $result['data'] = $filepath;
            } else {
                $result['success'] = false;
                $result['message'] = 'Invalid image';
            }

            unlink($tempfile);
        }

        return $result;
    }

    public function storeProductAttributes($product, $attributes)
    {
        if (is_array($attributes)) {
            foreach ($attributes as $attribute) {
                $rules = [
                    'group' => 'required',
                    'name' => 'required',
                    'value' => 'required',
                    'sort_order' => 'required|integer|min:1',
                ];

                $validateditem = $attribute->toArray();
                $validator = \Validator::make($validateditem, $rules);
                if(!$validator->fails()){
                    if ($attribute['name']) {
                        $productAttribute = new ProductAttribute;
                        $productAttribute->product_id = $product->id;
                        $productAttribute->group = $attribute['group'];
                        $productAttribute->key = $attribute['name'];
                        $productAttribute->value = $attribute['value'];
                        $productAttribute->sort_order = $attribute['sort_order'];
                        $productAttribute->save();
                    }
                }

            }
        }
    }

    public function updateProductAttributes($product, $attributes)
    {
        if (is_array($attributes)) {
            foreach ($attributes as $attribute) {
                $rules = [
                    'group' => 'required',
                    'name' => 'required',
                    'value' => 'required',
                    'sort_order' => 'required|integer|min:1',
                ];

                $validateditem = $attribute->toArray();
                $validator = \Validator::make($validateditem, $rules);
                if(!$validator->fails()){
                    if ($attribute['name']) {
                        ProductAttribute::updateOrCreate([
                            'product_id' => $product->id,
                            'group' => $attribute['group'],
                            'key' => $attribute['name'],
                        ], [
                            'value' => $attribute['value'],
                            'sort_order' => $attribute['sort_order'],
                        ]);
                    }
                }

            }
        }
    }

    public function updateProductCategory($product, $store, $item)
    {
        $marketplace = $store->marketplace;

        $itemCategoryString = $item['category_id'];
        $itemCategoryArray = explode(',', $itemCategoryString);

        $categories = [];
        foreach ($itemCategoryArray as $categoryId) {
            $category = $marketplace->categories()->where([
                'id' => trim($categoryId),
            ])->first();
            if ($category) {
                $categories[] = trim($categoryId);
            }
        }

        if ($categories) {
            $product->marketplaceCategories()->sync($categories);
        } else {
            $product->marketplaceCategories()->sync([]);
        }
    }

    public function updateProductCollection($product, $store, $item)
    {
        $marketplace = $store->marketplace;

        $itemCategoryString = $item['collection_id'];
        $itemCategoryArray = explode(',', $itemCategoryString);

        $categories = [];
        foreach ($itemCategoryArray as $categoryId) {
            $category = $store->categories()->where([
                'id' => trim($categoryId),
            ])->first();
            if ($category) {
                $categories[] = trim($categoryId);
            }
        }

        if ($categories) {
            $product->storeCategories()->sync($categories);
        } else {
            $product->storeCategories()->sync([]);
        }
    }
}
