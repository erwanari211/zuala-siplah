<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\StoreNote;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Website\WebsiteSettings;

class StoreNotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace = $store->marketplace;
        $notes = $store->notes()->latest()->paginate(20);
        // return $notes;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.notes.index', compact(
            'user', 'store', 'marketplace', 'notes',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.notes.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'title' => 'required',
            'content' => 'required',
            'active' => 'required|integer',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $note = new StoreNote;
        $note->user_id = $user->id;
        $note->title = request('title');
        $note->slug = str_slug(request('title'));
        $note->content = request('content');
        $note->active = request('active');
        $store->menus()->save($note);

        flash('Note saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplace\StoreNote  $storeNote
     * @return \Illuminate\Http\Response
     */
    public function show(StoreNote $storeNote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplace\StoreNote  $storeNote
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreNote $note)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$note->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.notes.index', $store->id);
        }

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.notes.edit', compact(
            'user', 'store', 'note', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplace\StoreNote  $storeNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreNote $note)
    {
        request()->validate([
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'active' => 'required|integer',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$note->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.notes.index', $store->id);
        }

        $note->title = request('title');
        $note->slug = str_slug(request('slug'));
        $note->content = request('content');
        $note->active = request('active');
        $note->save();

        flash('Note updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplace\StoreNote  $storeNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreNote $note)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$note->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.notes.index', $store->id);
        }

        $note->delete();

        flash('Note deleted');
        return redirect()->back();
    }
}
