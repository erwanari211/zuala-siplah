<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\GroupedProductDetail;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\FromCollectionWithViewExport;
use App\Exports\FromCollectionWithViewMultipleSheetsExport;
use App\Imports\ToCollectionImport;
use App\Models\Modules\Website\WebsiteSettings;

class GroupedProductDetailsImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.grouped-products.import.create', compact(
            'store', 'product',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store, Product $product)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, Product $product, GroupedProductDetail $groupedProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, Product $product)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $file = request('file');

        if ($file) {
            $import = new ToCollectionImport;
            Excel::import($import, $file);

            $excelData = $import->getData();

            $groupedProducts = $this->processProductData($excelData);
            $storedData = $this->storeData($groupedProducts, $store, $product);

            $message = 'Product added to grouped product : '.$storedData['success']['total'].'/'.$storedData['totalItems'];
            flash($message);
            if ($storedData['failed']['total'] > 0) {
                $message = '';
                foreach ($storedData['failed']['data'] as $failedItem) {
                    $message .= 'Product with SKU '.$failedItem['unique_code'].' not imported ';
                    $message .= ($failedItem['message']) ? '('.$failedItem['message'].')' : '';
                    $message .= '<br>';
                }
                flash($message)->warning();
            }
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Product $product, GroupedProductDetail $groupedProduct)
    {

    }

    public function downloadTemplate(Store $store)
    {
        $products = $store->products()->with('attributes', 'marketplaceCategories', 'storeCategories')->latest()->get();

        $sheetData['products'] = [];
        $sheetData['product_attributes'] = [];
        foreach ($products as $item) {
            $product['id'] = $item['id'];
            $product['unique_code'] = $item['unique_code'];
            $product['sku'] = $item['sku'];
            $product['name'] = $item['name'];
            $product['description'] = $item['description'];
            $product['price'] = $item['price'];
            $product['qty'] = $item['qty'];
            $product['image'] = null;
            $product['weight'] = $item['weight'];
            $product['active'] = $item['active'];
            $product['categories'] = $item['marketplaceCategories'];
            $product['collections'] = $item['storeCategories'];
            $sheetData['products'][] = $product;

            if (isset($item['attributes'])) {
                foreach ($item['attributes'] as $itemAttribute) {
                    if ($item['unique_code']) {
                        $attribute['product_id'] = $item['id'];
                        $attribute['unique_code'] = $item['unique_code'];
                        $attribute['sku'] = $item['sku'];
                        $attribute['group'] = $itemAttribute['group'];
                        $attribute['name'] = $itemAttribute['key'];
                        $attribute['value'] = $itemAttribute['value'];
                        $attribute['sort_order'] = $itemAttribute['sort_order'];
                        $sheetData['product_attributes'][] = $attribute;
                    }
                }
            }
        }

        $products = $sheetData['products'];
        $attributes = $sheetData['product_attributes'];

        $categories = $store->marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['childs' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $sheetData['categories'] = $categories;

        $collections = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['childs' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $sheetData['collections'] = $collections;

        // return view('modules.marketplaces.backend.stores.products.export.products', compact('products'));
        // return view('modules.marketplaces.backend.stores.products.grouped-products.export.grouped-product-detail');

        $excelData = [];
        $excelData['details'] = [
            'sheetname' => 'details',
            'view' => 'modules.marketplaces.backend.stores.products.grouped-products.export.grouped-product-detail',
            'data' => [],
        ];
        $excelData['products'] = [
            'sheetname' => 'products',
            'view' => 'modules.marketplaces.backend.stores.products.grouped-products.export.products',
            'data' => compact('products'),
        ];

        $date = date('Ymd');
        $randomString = str_random(8).'-'.time();
        $outputName = $date.'-export-product-data-'.$randomString.'.xlsx';
        return Excel::download(new FromCollectionWithViewMultipleSheetsExport($excelData), $outputName);
    }

    public function processProductData($data)
    {
        $groupedProducts = [];

        foreach ($data['details'] as $detail) {
            if ($detail['unique_code']) {
                $groupedProducts[$detail['unique_code']] = $detail;
            }
        }

        return $groupedProducts;
    }

    public function storeData($data, Store $store, Product $product)
    {
        $results = [];
        $results['totalItems'] = 0;
        $results['success']['total'] = 0;
        $results['success']['data'] = [];
        $results['failed']['total'] = 0;
        $results['failed']['data'] = [];

        foreach ($data as $item) {
            $results['totalItems']++;
            $groupedProductDetail = Product::where([
                'store_id' => $store->id,
                'unique_code' => $item['unique_code'],
            ])->first();

            if ($groupedProductDetail) {
                 $rules = [
                    'qty' => 'required|integer|min:1',
                ];

                $validateditem = $item->toArray();
                $validator = \Validator::make($validateditem, $rules);
                if($validator->fails()){
                    echo "not valid".'<br>';
                    $results['failed']['total']++;
                    $results['failed']['data'][] = [
                        'unique_code' => $item['unique_code'],
                        'message' => 'data not valid',
                    ];
                } else {
                    GroupedProductDetail::updateOrCreate([
                        'parent_id' => $product->id,
                        'product_id' => $groupedProductDetail->id,
                    ], [
                        'qty' => $item['qty'],
                        'note' => $item['note'],
                    ]);

                    echo "added product to grouped product : ".$item['unique_code'].'<br>';
                    $results['success']['total']++;
                    $results['success']['data'][] = [
                        'unique_code' => $item['unique_code'],
                        'message' => 'product added to grouped product',
                    ];
                }
            } else {
                $results['failed']['total']++;
                $results['failed']['data'][] = [
                    'unique_code' => $item['unique_code'],
                    'message' => 'Product not found',
                ];
            }
        }
        return $results;
    }
}
