<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\StoreSeller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Website\WebsiteSettings;

class StoreSellersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace = $store->marketplace;
        $storeSellers = $store->storeSellers()->latest()->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.store-sellers.index', compact(
            'user', 'store', 'marketplace', 'storeSellers',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.store-sellers.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'seller_name' => 'required',
            'active' => 'required|integer',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $storeSeller = new StoreSeller;
        $storeSeller->store_id = $store->id;
        $storeSeller->seller_name = request('seller_name');
        $storeSeller->note = request('note') ? request('note') : '';
        $storeSeller->active = request('active');
        $store->storeSellers()->save($storeSeller);

        flash('Store seller saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreSeller  $storeSeller
     * @return \Illuminate\Http\Response
     */
    public function show(StoreSeller $storeSeller)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreSeller  $storeSeller
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreSeller $storeSeller)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$storeSeller->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.store-sellers.index', $store->id);
        }

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.store-sellers.edit', compact(
            'user', 'store', 'storeSeller', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\StoreSeller  $storeSeller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreSeller $storeSeller)
    {
        request()->validate([
            'seller_name' => 'required',
            'active' => 'required|integer',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$storeSeller->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.store-sellers.index', $store->id);
        }

        $storeSeller->seller_name = request('seller_name');
        $storeSeller->note = request('note') ? request('note') : '';
        $storeSeller->active = request('active');
        $storeSeller->save();

        flash('Store seller updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreSeller  $storeSeller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreSeller $storeSeller)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$storeSeller->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.store-sellers.index', $store->id);
        }

        $storeSeller->delete();

        flash('Store seller deleted');
        return redirect()->back();
    }
}
