<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\ProductAttribute;
use App\Models\Modules\Website\WebsiteSettings;

class ProductAttributesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.attributes.create', compact(
            'store', 'product',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store, Product $product)
    {
        request()->validate([
            'group' => 'required',
            'key' => 'required',
            'value' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $attribute = new ProductAttribute;
        $attribute->product_id = $product->id;
        $attribute->group = request('group');
        $attribute->key = request('key');
        $attribute->value = request('value');
        $attribute->sort_order = request('sort_order');
        $attribute->save();

        flash('Product attribute saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, Product $product, ProductAttribute $attribute)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        if (!$attribute->isOwnedByProduct($product->id)) {
            flash('Product Attribute not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.attributes.edit', compact(
            'store', 'product', 'attribute',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, Product $product, ProductAttribute $attribute)
    {
        request()->validate([
            'group' => 'required',
            'key' => 'required',
            'value' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        if (!$attribute->isOwnedByProduct($product->id)) {
            flash('Product Attribute not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        $attribute->product_id = $product->id;
        $attribute->group = request('group');
        $attribute->key = request('key');
        $attribute->value = request('value');
        $attribute->sort_order = request('sort_order');
        $attribute->save();

        flash('Product attribute updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Product $product, ProductAttribute $attribute)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        if (!$attribute->isOwnedByProduct($product->id)) {
            flash('Product Attribute not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        $attribute->delete();

        flash('Product attribute deleted');
        return redirect()->back();
    }
}
