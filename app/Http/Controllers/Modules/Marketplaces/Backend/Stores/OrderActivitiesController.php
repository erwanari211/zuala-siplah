<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use App\Models\Modules\Marketplaces\OrderActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderMeta;
use App\Models\Modules\Marketplaces\OrderProduct;

class OrderActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\OrderActivity  $orderActivity
     * @return \Illuminate\Http\Response
     */
    public function show(OrderActivity $orderActivity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\OrderActivity  $orderActivity
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderActivity $orderActivity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\OrderActivity  $orderActivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderActivity $orderActivity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\OrderActivity  $orderActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderActivity $orderActivity)
    {
        //
    }

    public function negotiationCommentsStore(Request $request, Store $store, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'activity' => 'required',
            'note' => 'required',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $activity = new OrderActivity;
        $activity->order_id = $order->id;
        $activity->user_id = $user->id;
        $activity->group = request('group');
        $activity->activity = request('activity');
        $activity->note = request('note');
        $activity->save();

        flash('Order activity saved');
        return redirect()->back();
    }

    public function negotiationPricesStore(Request $request, Store $store, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'activity' => 'required',
            'price' => 'required|numeric',
            'note' => 'required',
            'order_product' => 'required_if:activity,change product price'
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $activity = request('activity');
        $price = request('price');
        $price = $price + 0;

        $ordermetas = $order->getOrderMetas();

        if ($activity == 'adjustment price') {
            $subtotal = $ordermetas['order']['subtotal'];
            $shipping = $ordermetas['order']['shipping'];
            $unique_number = $ordermetas['order']['unique_number'];
            $adjustment_price = $price;

            $total = $subtotal + $shipping + $unique_number + $adjustment_price;

            OrderMeta::updateOrderMeta($order->id, [
                'group' => 'order',
                'key' => 'adjustment_price',
                'value' => $adjustment_price,
            ]);

            OrderMeta::updateOrderMeta($order->id, [
                'group' => 'order',
                'key' => 'total',
                'value' => $total,
            ]);

            $order->total = $total;
            $order->save();
        }

        if ($activity == 'change shipping cost') {
            $subtotal = $ordermetas['order']['subtotal'];
            $shipping = $price;
            $unique_number = $ordermetas['order']['unique_number'];
            $adjustment_price = $ordermetas['order']['adjustment_price'];

            $total = $subtotal + $shipping + $unique_number + $adjustment_price;

            OrderMeta::updateOrderMeta($order->id, [
                'group' => 'order',
                'key' => 'shipping',
                'value' => $shipping,
            ]);

            OrderMeta::updateOrderMeta($order->id, [
                'group' => 'order',
                'key' => 'total',
                'value' => $total,
            ]);

            $order->total = $total;
            $order->save();
        }

        if ($activity == 'change product price') {
            // change order products
            $order_product = request('order_product');
            $price = request('price');

            $orderProduct = OrderProduct::find($order_product);
            $orderProduct->price = $price;
            $orderProduct->total = $price * $orderProduct->qty;
            $orderProduct->save();

            // change order meta
            $orderProducts = collect($order->products);
            $totalAmount = $orderProducts->sum('total');

            OrderMeta::updateOrderMeta($order->id, [
                'group' => 'order',
                'key' => 'subtotal',
                'value' => $totalAmount,
            ]);

            $ordermetas = $order->getOrderMetas();
            $subtotal = $ordermetas['order']['subtotal'];
            $shipping = $ordermetas['order']['shipping'];
            $adjustment_price = $ordermetas['order']['adjustment_price'];
            $unique_number = $ordermetas['order']['unique_number'];

            $total = $subtotal + $shipping + $adjustment_price + $unique_number;
            OrderMeta::updateOrderMeta($order->id, [
                'group' => 'order',
                'key' => 'total',
                'value' => $total,
            ]);

            // change order
            $order->total = $total;
            $order->save();
        }

        $orderActivity = new OrderActivity;
        $orderActivity->order_id = $order->id;
        $orderActivity->user_id = $user->id;
        $orderActivity->group = request('group');
        $orderActivity->activity = request('activity');
        $orderActivity->note = request('note');
        $orderActivity->save();

        $price = request('price');
        $price = $price + 0;
        $orderActivity->setMeta('price', $price);

        if ($activity == 'change product price') {
            $order_product = request('order_product');
            $orderProduct = OrderProduct::find($order_product);
            $orderActivity->setMeta('product_name', $orderProduct->product_name);
        }

        flash('Order activity saved');
        return redirect()->back();
    }

    public function complainCommentsStore(Request $request, Store $store, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'activity' => 'required',
            'note' => 'required',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $activity = new OrderActivity;
        $activity->order_id = $order->id;
        $activity->user_id = $user->id;
        $activity->group = request('group');
        $activity->activity = request('activity');
        $activity->note = request('note');
        $activity->save();

        $file = request('file');
        if ($file) {
            $filename = 'order-complain-attachment';
            $filepath = upload_file($file, 'uploads', $filename);
            $activity->has_attachment = true;
            $activity->save();
            $activity->setMeta('attachments', $filepath);
        }

        flash('Order activity saved');
        return redirect()->back();
    }

    public function documentsStore(Request $request, Store $store, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'activity' => 'required',
            'file' => 'required|file|mimes:jpeg,jpg,png,pdf,xls,xlsx,doc,docx|max:15000',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $activity = new OrderActivity;
        $activity->order_id = $order->id;
        $activity->user_id = $user->id;
        $activity->group = request('group');
        $activity->activity = request('activity');
        $activity->note = request('note');
        $activity->save();

        $file = request('file');
        if ($file) {
            $filename = 'order-document-attachment';
            $filepath = upload_file($file, 'uploads', $filename);
            $activity->has_attachment = true;
            $activity->save();
            $activity->setMeta('attachments', $filepath);
        }

        flash('Order activity saved');
        return redirect()->back();
    }
}
