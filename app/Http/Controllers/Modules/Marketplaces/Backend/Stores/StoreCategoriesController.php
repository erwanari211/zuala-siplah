<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreCategory;
use App\Models\Modules\Website\WebsiteSettings;

class StoreCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $store->updateLastOnline();

        $collections = $store->categories()->with('parent')->latest()->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.categories.index', compact(
            'user', 'store', 'collections',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $store->updateLastOnline();

        $parentCategories = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->pluck('name', 'id');
        $dropdown['parent_categories'] = $parentCategories;
        $dropdown['parent_categories'][0] =  'Is Parent';
        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.categories.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'parent' => 'required',
            'sort_order' => 'required|integer|min:1',
            'file' => 'image|max:4000',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $category = new StoreCategory;
        $category->name = request('name');
        $category->slug = str_slug(request('name'));
        $category->description = request('description');
        $category->active = request('active');
        $category->sort_order = request('sort_order');

        $store->categories()->save($category);

        $parentId = request('parent');
        if ($parentId == 0) {
            $category->parent_id = 0;
            $category->save();
        } else {
            $parentCategory = $store->categories()->find($parentId);
            if ($parentCategory && $parentCategory->isOwnedByStore($store->id)) {
                $category->parent_id = $parentCategory->id;
                $category->save();
            }
        }

        $image = request('image');
        if ($image) {
            $filename = 'store-collections';
            $filepath = upload_file($image, 'uploads', $filename);
            $category->image = $filepath;
            $category->save();
        }

        if (!$category->checkIsUnique()) {
            flash('Slug is not unique. Please change slug')->warning();
        }

        flash('Collection saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, StoreCategory $collection)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$collection->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.collections.index', $store->id);
        }

        $store->updateLastOnline();

        $products = $collection->products;
        $dropdown['products'] = $store->products()->orderBy('name')->pluck('name', 'id');
        $storeProducts = $store->products()->orderBy('name')->get();
        $storeProducts = $storeProducts->mapWithKeys(function($item){
            $id = $item['id'];
            $name = $item['name'];
            $sku = $item['sku'];
            $productName = $name . ( $sku ? " ($sku)" : "" );

            return [$id => $productName];
        });
        $dropdown['products'] = $storeProducts;
        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.categories.show', compact(
            'user', 'store', 'collection', 'dropdown', 'products',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreCategory $collection)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$collection->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.collections.index', $store->id);
        }

        $store->updateLastOnline();

        $parentCategories = $store->categories()
            ->where('parent_id', 0)
            ->where('id', '<>', $collection->id)
            ->orderBy('name')
            ->pluck('name', 'id');
        $dropdown['parent_categories'] = $parentCategories;
        $dropdown['parent_categories'][0] =  'Is Parent';
        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.categories.edit', compact(
            'user', 'store', 'collection', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreCategory $collection)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'parent' => 'required',
            'sort_order' => 'required|integer|min:1',
            'file' => 'image|max:4000',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$collection->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.collections.index', $store->id);
        }

        $category = $collection;
        $category->name = request('name');
        $category->slug = str_slug(request('slug'));
        $category->description = request('description');
        $category->active = request('active');
        $category->sort_order = request('sort_order');
        $category->save();

        $parentId = request('parent');
        if ($parentId == 0) {
            $category->parent_id = 0;
            $category->save();
        } else {
            $parentCategory = $store->categories()->find($parentId);
            if ($parentCategory && $parentCategory->isOwnedByStore($store->id)) {
                $category->parent_id = $parentCategory->id;
                $category->save();
            }
        }

        $image = request('image');
        if ($image) {
            $filename = 'store-collections';
            $filepath = upload_file($image, 'uploads', $filename);
            $category->image = $filepath;
            $category->save();
        }

        if (!$category->checkIsUnique()) {
            flash('Slug is not unique. Please change slug')->warning();
        }

        flash('Collection updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreCategory $collection)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$collection->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.collections.index', $store->id);
        }

        $collection->products()->detach();
        $collection->children()->update(['parent_id'=> 0]);
        $collection->delete();

        flash('Collection deleted');
        return redirect()->back();
    }
}
