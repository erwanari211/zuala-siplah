<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Marketplaces\OrderHistory;

class OrderHistoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store, Order $order)
    {

        request()->validate([
            'status' => 'required',
            'notify' => 'required',
            'file' => 'image|max:4000',
        ]);

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $statusId = request('status');
        $status = OrderStatus::find($statusId);

        $history = new OrderHistory;
        $history->order_id = $order->id;
        $history->order_status_id = $status->id;
        $history->order_status = $status->key;
        $history->notify = request('notify');
        $history->note = request('note');
        $history->has_attachment = false;
        $history->save();

        $file = request('file');
        if ($file) {
            $filename = 'order-history-attachment';
            $filepath = upload_file($file, 'uploads', $filename);
            $history->has_attachment = true;
            $history->save();
            $history->setMeta('attachments', $filepath);
        }

        $order->order_status_id = $status->id;
        $order->order_status = $status->key;
        $order->save();

        if ($status->key == 'paid') {
            $order->payment_status = 'paid';
            $order->save();
        }

        if ($status->key == 'shipped') {
            $order->shipping_status = 'shipped';
            $order->save();
        }

        if ($status->key == 'canceled') {
            $order->payment_status = 'canceled';
            $order->shipping_status = 'canceled';
            $order->save();
        }

        flash('Order history saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
