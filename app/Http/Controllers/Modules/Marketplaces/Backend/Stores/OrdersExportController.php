<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\FromCollectionWithViewExport;
use App\Exports\FromCollectionWithViewMultipleSheetsExport;
use App\Models\Modules\Marketplaces\Order;
use PDF;

class OrdersExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exportBast(Store $store, Order $order)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.stores.orders.exports.bast';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'bast-'.$filename;
        return $pdf->download($filename);
    }

    public function exportSuratJalan(Store $store, Order $order)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.stores.orders.exports.surat-jalan';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'surat-jalan-'.$filename;
        return $pdf->download($filename);
    }

    public function exportFaktur(Store $store, Order $order)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.stores.orders.exports.faktur';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'faktur-'.$filename;
        return $pdf->download($filename);
    }
}
