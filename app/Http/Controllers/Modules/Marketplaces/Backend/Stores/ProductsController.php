<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Website\WebsiteSettings;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        Product::fillUniqueCode();

        $store->updateLastOnline();

        $filters['active'] = request('active') ? request('active') : 'true';
        $filters['status'] = request('status') ? request('status') : null;
        $collectionId = request('collection_id');
        $categoryId = request('category_id');
        $products = $store->products()->with('marketplaceCategories', 'storeCategories', 'meta');

        $name = request('name');
        $sku = request('sku');
        if ($name) {
            $products = $products->where('name', 'LIKE', "%{$name}%");
            $filters['name'] = request('name') ? request('name') : null;
        }
        if ($sku) {
            $products = $products->where('sku', 'LIKE', "%{$sku}%");
            $filters['sku'] = request('sku') ? request('sku') : null;
        }
        if ($categoryId) {
            $products->whereHas('marketplaceCategories', function($q) use ($categoryId){
                $q->where('marketplace_categories.id', $categoryId);
            });
        }
        if ($collectionId) {
            $products->whereHas('storeCategories', function($q) use ($collectionId){
                $q->where('store_categories.id', $collectionId);
            });
        }

        $products = $products
            ->filter($filters)
            ->latest()
            ->paginate(20);

        $collections =  $store->categories()->orderBy('name')->pluck('name', 'id');
        $dropdown['collections'] = $collections;

        $categories =  $store->marketplace->categories()->orderBy('name')->pluck('name', 'id');
        $dropdown['categories'] = $categories;

        $marketplace = $store->marketplace;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.index', compact(
            'user', 'store', 'marketplace', 'products', 'filters', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $store->updateLastOnline();

        $marketplace = $store->marketplace;
        // $dropdown['categories'] = $marketplace->categories()->pluck('name', 'id');
        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
            if (count($marketplaceCategory->children)) {
                foreach ($marketplaceCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $categories[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['marketplaceCategories'] = $categories;

        $storeCategories = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $collections = [];
        foreach ($storeCategories as $key => $storeCategory) {
            $marketplaceId = $storeCategory['id'];
            $marketplaceName = $storeCategory['name'];
            $collections[$marketplaceId] = $marketplaceName;
            if (count($storeCategory->children)) {
                foreach ($storeCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $collections[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['storeCategories'] = $collections;

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['statuses'] = ['active'=>'Active', 'inactive'=>'Inactive', 'archived'=>'Archived'];
        $dropdown['types'] = [
            'product'=>'Product',
            'grouped product'=>'Grouped Product',
            'digital' => 'Digital',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'price' => 'required|integer|min:0',
            'category' => 'required',
            'qty' => 'required|integer|min:0',
            'weight' => 'required|integer|min:0',
            'active' => 'required|integer',
            'image' => 'image|max:4000',
            'type' => 'required',
        ]);


        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $product = new Product;
        $product->name = request('name');
        $slug = request('slug') ? str_slug(request('slug')) : str_slug(request('name'));
        $product->slug = Product::getUniqueSlug($slug);
        $product->unique_code = Product::randomUniqueCode();
        $product->sku = request('sku');
        $product->description = request('description');
        $product->price = request('price');
        $product->qty = request('qty');
        $product->weight = request('weight');
        $product->type = request('type');
        $product->active = request('active');
        $product->status = request('status');
        $product->user_id = $user->id;
        $product->store_id = $store->id;
        $product->save();

        $image = request('image');
        if ($image) {
            $filename = 'products';
            $filepath = upload_file($image, 'uploads', $filename);
            $product->image = $filepath;
            $product->save();
        }

        $category = request('category');
        $category = array_filter($category);
        if ($category) {
            $product->marketplaceCategories()->sync($category);
        }

        $collection = request('collection');
        if ($collection) {
            $collection = array_filter($collection);
            $product->storeCategories()->sync($collection);
        }

        flash('Product saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $store->updateLastOnline();

        $marketplace = $store->marketplace;
        // $dropdown['categories'] = $marketplace->categories()->pluck('name', 'id');
        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
            if (count($marketplaceCategory->children)) {
                foreach ($marketplaceCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $categories[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['marketplaceCategories'] = $categories;

        $storeCategories = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $collections = [];
        foreach ($storeCategories as $key => $storeCategory) {
            $marketplaceId = $storeCategory['id'];
            $marketplaceName = $storeCategory['name'];
            $collections[$marketplaceId] = $marketplaceName;
            if (count($storeCategory->children)) {
                foreach ($storeCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $collections[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['storeCategories'] = $collections;

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['statuses'] = ['active'=>'Active', 'inactive'=>'Inactive', 'archived'=>'Archived'];
        $dropdown['types'] = [
            'product'=>'Product',
            'grouped product'=>'Grouped Product',
            'digital' => 'Digital',
        ];
        // $productCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $groupedProductDetails = $product->groupedProductDetails()->with('parent', 'product')->get();
        $productMarketplaceCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $productStoreCategories = $product->storeCategories()->pluck('store_category_id');


        $attributes = $product->attributes()->orderBy('sort_order', 'asc')->get();
        $groupedAttributes = $attributes->groupBy('group');

        $galleries = $product->galleries()->orderBy('sort_order', 'asc')->get();

        $mappedKemdikbudZonePrices = [];
        $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
        if ($hasKemdikbudZonePrice) {
            $mappedKemdikbudZonePrices =  $product->getMappedKemdikbudZonePrices();
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.show', compact(
            'user', 'store', 'dropdown', 'product',
            'groupedAttributes', 'groupedProductDetails', 'galleries',
            'hasKemdikbudZonePrice', 'mappedKemdikbudZonePrices',
            'productMarketplaceCategories', 'productStoreCategories',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $store->updateLastOnline();

        $marketplace = $store->marketplace;
        // $dropdown['categories'] = $marketplace->categories()->pluck('name', 'id');
        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
            if (count($marketplaceCategory->children)) {
                foreach ($marketplaceCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $categories[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['marketplaceCategories'] = $categories;

        $storeCategories = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $collections = [];
        foreach ($storeCategories as $key => $storeCategory) {
            $marketplaceId = $storeCategory['id'];
            $marketplaceName = $storeCategory['name'];
            $collections[$marketplaceId] = $marketplaceName;
            if (count($storeCategory->children)) {
                foreach ($storeCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $collections[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['storeCategories'] = $collections;

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['statuses'] = ['active'=>'Active', 'inactive'=>'Inactive', 'archived'=>'Archived'];
        $dropdown['types'] = [
            'product'=>'Product',
            'grouped product'=>'Grouped Product',
            'digital' => 'Digital',
        ];
        $productMarketplaceCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $productStoreCategories = $product->storeCategories()->pluck('store_category_id');

        $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.edit', compact(
            'user', 'store', 'dropdown', 'product',
            'hasKemdikbudZonePrice',
            'productMarketplaceCategories', 'productStoreCategories',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, Product $product)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'price' => 'required|integer|min:0',
            'original_price' => 'nullable|integer|min:0',
            // 'category' => 'required',
            'qty' => 'required|integer|min:0',
            'weight' => 'required|numeric|min:0',
            'active' => 'required|integer',
            'image' => 'image|max:4000',
            'type' => 'required',
        ]);


        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $product->name = request('name');
        $slug = request('slug') ? str_slug(request('slug')) : str_slug(request('name'));
        $product->slug = Product::getUniqueSlug($slug, $product->id);
        $product->sku = request('sku');
        $product->description = request('description');
        $product->price = request('price');
        $product->qty = request('qty');
        $product->weight = request('weight');
        $product->type = request('type');
        $product->active = request('active');
        $product->status = request('status');
        $product->user_id = $user->id;
        $product->store_id = $store->id;
        $product->save();

        $image = request('image');
        if ($image) {
            $filename = 'products';
            $filepath = upload_file($image, 'uploads', $filename);
            $product->image = $filepath;
            $product->save();
        }

        $category = request('category');
        $category = is_array($category) ? array_filter($category) : null;
        if ($category) {
            $product->marketplaceCategories()->sync($category);
        } else {
            $product->marketplaceCategories()->sync([]);
        }

        $collection = request('collection');
        $collection = is_array($collection) ? array_filter($collection) : null;
        if ($collection) {
            $product->storeCategories()->sync($collection);
        } else {
            $product->storeCategories()->sync([]);
        }

        $isUniqueSku = Product::checkIsUniqueSku($product->id);
        if (!$isUniqueSku) {
            flash('SKU is not unique. Please change SKU')->warning();
        }

        // meta
        $originalPrice = request('original_price');
        $product->setMeta('original_price', $originalPrice);

        $hasZonePrice = request('has_kemdikbud_zone_price');
        $product->setMeta('has_kemdikbud_zone_price', $hasZonePrice ? true : false);

        flash('Product updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $product->active = false;
        $product->save();

        flash('Product deleted');
        return redirect()->back();
    }

    public function restore(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $product->active = true;
        $product->save();

        flash('Product restored');
        return redirect()->back();
    }
}
