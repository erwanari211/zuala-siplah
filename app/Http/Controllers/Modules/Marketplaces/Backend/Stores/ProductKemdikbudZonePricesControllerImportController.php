<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\GroupedProductDetail;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\FromCollectionWithViewExport;
use App\Exports\FromCollectionWithViewMultipleSheetsExport;
use App\Imports\ToCollectionImport;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice;

class ProductKemdikbudZonePricesControllerImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $totalProducts = $store->products()->count();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.zone-prices.import.create', compact(
            'store', 'product',
            'totalProducts',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store, Product $product)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, Product $product, GroupedProductDetail $groupedProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, Product $product)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $file = request('file');

        if ($file) {
            $import = new ToCollectionImport;
            Excel::import($import, $file);

            $excelData = $import->getData();

            $zonePriceProducts = $this->processProductData($excelData);
            $storedData = $this->storeData($zonePriceProducts, $store);

            $message = 'Product zone prices updated : '.$storedData['success']['total'].'/'.$storedData['totalItems'];
            flash($message);
            if ($storedData['failed']['total'] > 0) {
                $message = '';
                foreach ($storedData['failed']['data'] as $failedItem) {
                    $message .= 'Product with SKU '.$failedItem['unique_code'].' not imported ';
                    $message .= ($failedItem['message']) ? '('.$failedItem['message'].')' : '';
                    $message .= '<br>';
                }
                flash($message)->warning();
            }
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Product $product, GroupedProductDetail $groupedProduct)
    {

    }

    public function downloadTemplate(Store $store)
    {
        // return request()->all();
        $start = request('start');
        $end = request('end');
        $skip = ($start <= 0) ? 0 : $start - 1;
        if ($skip > $end) {
            $skip = $end;
        }
        $limit = $end - $start + 1;

        $products = $store->products()->with(
            'attributes',
            'marketplaceCategories',
            'storeCategories',
            'meta', 'kemdikbudZonePrices')
            ->skip($skip)
            ->take($limit)
            ->get();

        $sheetData['products'] = [];
        $sheetData['product_attributes'] = [];

        $no = $skip + 1;
        foreach ($products as $item) {
            $product['index'] = $no;
            $product['id'] = $item['id'];
            $product['unique_code'] = $item['unique_code'];
            $product['sku'] = $item['sku'];
            $product['name'] = $item['name'];
            $product['description'] = $item['description'];
            $product['price'] = $item['price'];
            $product['qty'] = $item['qty'];
            $product['image'] = null;
            $product['weight'] = $item['weight'];
            $product['active'] = $item['active'];
            $product['categories'] = $item['marketplaceCategories'];
            $product['collections'] = $item['storeCategories'];

            $no++;

            $hasZonePrice = $item->getMeta('has_kemdikbud_zone_price');
            $product['has_kemdikbud_zone_price'] = $hasZonePrice ? 1 : 0;

            $product['price_zone_1'] = 0;
            $product['price_zone_2'] = 0;
            $product['price_zone_3'] = 0;
            $product['price_zone_4'] = 0;
            $product['price_zone_5'] = 0;

            $product['mapped_kemdikbud_zone_price'] = $item->getMappedKemdikbudZonePrices();
            if ($product['mapped_kemdikbud_zone_price']) {
                foreach ($product['mapped_kemdikbud_zone_price'] as $zone => $price) {
                    $product['price_zone_'.$zone] = $price;
                }
            }

            $sheetData['products'][] = $product;

            if (isset($item['attributes'])) {
                foreach ($item['attributes'] as $itemAttribute) {
                    if ($item['unique_code']) {
                        $attribute['product_id'] = $item['id'];
                        $attribute['unique_code'] = $item['unique_code'];
                        $attribute['sku'] = $item['sku'];
                        $attribute['group'] = $itemAttribute['group'];
                        $attribute['name'] = $itemAttribute['key'];
                        $attribute['value'] = $itemAttribute['value'];
                        $attribute['sort_order'] = $itemAttribute['sort_order'];
                        $sheetData['product_attributes'][] = $attribute;
                    }
                }
            }
        }

        $products = $sheetData['products'];
        $attributes = $sheetData['product_attributes'];

        $categories = $store->marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['childs' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $sheetData['categories'] = $categories;

        $collections = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['childs' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $sheetData['collections'] = $collections;

        // return view('modules.marketplaces.backend.stores.products.zone-prices.export.products', compact('products'));

        $excelData = [];
        $excelData['products'] = [
            'sheetname' => 'products',
            'view' => 'modules.marketplaces.backend.stores.products.zone-prices.export.products',
            'data' => compact('products'),
        ];

        $date = date('Ymd');
        $randomString = str_random(8).'-'.time();
        $outputName = $date.'-export-product-data-'.$randomString.'.xlsx';
        return Excel::download(new FromCollectionWithViewMultipleSheetsExport($excelData), $outputName);
    }

    public function processProductData($data)
    {
        $zonePriceProducts = [];

        foreach ($data['products'] as $product) {
            if ($product['unique_code']) {
                $zonePriceProducts[$product['unique_code']] = $product;
            }
        }

        return $zonePriceProducts;
    }

    public function storeData($data, Store $store)
    {
        $results = [];
        $results['totalItems'] = 0;
        $results['success']['total'] = 0;
        $results['success']['data'] = [];
        $results['failed']['total'] = 0;
        $results['failed']['data'] = [];

        foreach ($data as $item) {
            $results['totalItems']++;
            $zonePriceProduct = Product::where([
                'store_id' => $store->id,
                'unique_code' => $item['unique_code'],
            ])->first();

            if ($zonePriceProduct) {
                 $rules = [
                    'zone_1' => 'required|integer|min:0',
                    'zone_2' => 'required|integer|min:0',
                    'zone_3' => 'required|integer|min:0',
                    'zone_4' => 'required|integer|min:0',
                    'zone_5' => 'required|integer|min:0',
                ];

                $validateditem = $item->toArray();
                $validator = \Validator::make($validateditem, $rules);
                if($validator->fails()){
                    echo "not valid".'<br>';
                    $results['failed']['total']++;
                    $results['failed']['data'][] = [
                        'unique_code' => $item['unique_code'],
                        'message' => 'data not valid',
                    ];
                } else {
                    if ($item['has_zone_price']) {
                        $zonePriceProduct->setMeta('has_kemdikbud_zone_price', true);

                        $prices = [
                            [
                                'zone' => 1,
                                'price' => $item['zone_1'],
                            ],
                            [
                                'zone' => 2,
                                'price' => $item['zone_2'],
                            ],
                            [
                                'zone' => 3,
                                'price' => $item['zone_3'],
                            ],
                            [
                                'zone' => 4,
                                'price' => $item['zone_4'],
                            ],
                            [
                                'zone' => 5,
                                'price' => $item['zone_5'],
                            ],
                        ];

                        foreach ($prices as $price) {
                            ProductKemdikbudZonePrice::updateOrCreate([
                                'product_id' => $zonePriceProduct->id,
                                'zone' => $price['zone'],
                            ], [
                                'price' => $price['price'] ? $price['price'] : 0,
                            ]);
                        }
                    } else {
                        $zonePriceProduct->setMeta('has_kemdikbud_zone_price', false);
                    }

                    echo "product zone price updated : ".$item['unique_code'].'<br>';
                    $results['success']['total']++;
                    $results['success']['data'][] = [
                        'unique_code' => $item['unique_code'],
                        'message' => 'product zone price updated',
                    ];
                }
            } else {
                $results['failed']['total']++;
                $results['failed']['data'][] = [
                    'unique_code' => $item['unique_code'],
                    'message' => 'Product not found',
                ];
            }
        }
        return $results;
    }
}
