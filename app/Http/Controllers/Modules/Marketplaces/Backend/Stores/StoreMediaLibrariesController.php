<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\StoreMediaLibrary;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Website\WebsiteSettings;

class StoreMediaLibrariesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace = $store->marketplace;
        $medias = $store->mediaLibraries()->latest()->paginate(20);
        // return $medias;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.media-libraries.index', compact(
            'user', 'store', 'marketplace', 'medias',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['groups'] = [
            'image' => 'Image',
            'video' => 'Video',
            'document' => 'Document',
            'other' => 'Other'
        ];

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.media-libraries.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'group' => 'required',
            'file' => 'required|max:20000',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $file = request('file');
        if ($file) {
            $filename = 'store-medias';
            $filepath = upload_file($file, 'uploads', $filename);

            $media = new StoreMediaLibrary;
            $media->unique_code = $media->randomUniqueCode();
            $media->group = request('group');
            $media->name = request('name');
            $media->description = request('description');
            $media->tags = request('tags');
            $media->active = request('active');
            $media->is_public = request('is_public');

            $media->mimetype = $file->getClientMimeType();
            $media->url = $filepath;
            $store->mediaLibraries()->save($media);

            flash('Media saved');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreMediaLibrary  $storeMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function show(StoreMediaLibrary $storeMediaLibrary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreMediaLibrary  $storeMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreMediaLibrary $mediaLibrary)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$mediaLibrary->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.media-libraries.index', $store->id);
        }

        $dropdown['groups'] = [
            'image' => 'Image',
            'video' => 'Video',
            'document' => 'Document',
            'other' => 'Other'
        ];

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.media-libraries.edit', compact(
            'user', 'store', 'dropdown', 'mediaLibrary',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\StoreMediaLibrary  $storeMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreMediaLibrary $mediaLibrary)
    {
        request()->validate([
            'group' => 'required',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$mediaLibrary->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.media-libraries.index', $store->id);
        }

        $media = $mediaLibrary;
        $media->group = request('group');
        $media->name = request('name');
        $media->description = request('description');
        $media->tags = request('tags');
        $media->active = request('active');
        $media->is_public = request('is_public');
        $media->save();

        flash('Media updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreMediaLibrary  $storeMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreMediaLibrary $mediaLibrary)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$mediaLibrary->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.media-libraries.index', $store->id);
        }

        $mediaLibrary->delete();

        flash('Media deleted');
        return redirect()->back();
    }
}
