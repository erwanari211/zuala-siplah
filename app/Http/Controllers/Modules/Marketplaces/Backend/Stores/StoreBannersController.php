<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreBanner;
use App\Models\Modules\Website\WebsiteSettings;

class StoreBannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $banners = $store->banners()
            ->with('storeBannerGroupDetails')
            ->with('storeBannerGroupDetails.storeBannerGroup')
            ->latest()
            ->paginate(10);

        $dropdown['banner_groups'] = $store->bannerGroups()->active()->pluck('name', 'id');

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.banners.index', compact(
            'user', 'store', 'banners', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.banners.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'image' => 'image|required|max:4000',
            'link' => 'url|nullable',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $image = request('image');
        if ($image) {
            $filename = 'store-banners';
            $filepath = upload_file($image, 'uploads', $filename);
        }

        $banner = new StoreBanner;
        $banner->image = $filepath;
        $banner->link = request('link');
        $banner->active = request('active');
        $banner->expired_at = request('expired_at');
        $banner->note = request('note');
        $banner->sort_order = request('sort_order');
        $banner->store_id = $store->id;
        $banner->save();

        flash('Banner saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreBanner $banner)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$banner->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banners.index', $store->id);
        }
        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.banners.edit', compact(
            'user', 'store', 'banner', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreBanner $banner)
    {
        request()->validate([
            'image' => 'image|max:4000',
            'link' => 'url|nullable',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$banner->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banners.index', $store->id);
        }

        $banner->link = request('link');
        $banner->active = request('active');
        $banner->expired_at = request('expired_at');
        $banner->note = request('note');
        $banner->sort_order = request('sort_order');
        $banner->save();

        $image = request('image');
        if ($image) {
            $filename = 'store-banners';
            $filepath = upload_file($image, 'uploads', $filename);
            $banner->image = $filepath;
            $banner->save();
        }

        $isExpired = false;
        $now = date("Y-m-d");
        if ($now > $banner->expired_at) {
            $isExpired = true;
        }
        if ($isExpired) {
            $banner->active = false;
            $banner->save();
        }

        flash('Banner updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreBanner $banner)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$banner->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banners.index', $store->id);
        }

        $banner->storeBannerGroupDetails()->delete();
        $banner->delete();

        flash('Banner deleted');
        return redirect()->back();
    }
}
