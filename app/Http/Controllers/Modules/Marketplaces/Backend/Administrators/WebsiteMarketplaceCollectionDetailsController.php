<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection;

class WebsiteMarketplaceCollectionDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'marketplace_id' => 'required',
            'collection_id' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplaceId = request('marketplace_id');
        $marketplace = Marketplace::findOrFail($marketplaceId);

        $collectionId = request('collection_id');
        $collection = WebsiteMarketplaceCollection::findOrFail($collectionId);

        WebsiteMarketplaceCollectionDetail::updateOrCreate([
            'website_marketplace_collection_id' => $collection->id,
            'marketplace_id' => $marketplace->id,
        ], [
            'sort_order' => request('sort_order')
        ]);

        flash('Marketplace added to collection');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteMarketplaceCollectionDetail  $websiteMarketplaceCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteMarketplaceCollectionDetail $websiteMarketplaceCollectionDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteMarketplaceCollectionDetail  $websiteMarketplaceCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteMarketplaceCollectionDetail $websiteMarketplaceCollectionDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteMarketplaceCollectionDetail  $websiteMarketplaceCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteMarketplaceCollectionDetail $websiteMarketplaceCollectionDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteMarketplaceCollectionDetail  $websiteMarketplaceCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteMarketplaceCollection $marketplaceCollection, Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        WebsiteMarketplaceCollectionDetail::where([
            'website_marketplace_collection_id' => $marketplaceCollection->id,
            'marketplace_id' => $marketplace->id,
        ])->delete();

        flash('Marketplace removed from collection');
        return redirect()->back();
    }
}
