<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\MarketplaceMediaLibrary;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplaceMediaLibrariesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $medias = $marketplace->mediaLibraries()->latest()->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.media-libraries.index', compact(
            'user', 'store', 'marketplace', 'medias',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['groups'] = [
            'image' => 'Image',
            'video' => 'Video',
            'document' => 'Document',
            'other' => 'Other'
        ];

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.media-libraries.create', compact(
            'user', 'marketplace', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'group' => 'required',
            'file' => 'required|max:20000',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $file = request('file');
        if ($file) {
            $filename = 'marketplace-medias';
            $filepath = upload_file($file, 'uploads', $filename);

            $media = new MarketplaceMediaLibrary;
            $media->unique_code = $media->randomUniqueCode();
            $media->group = request('group');
            $media->name = request('name');
            $media->description = request('description');
            $media->tags = request('tags');
            $media->active = request('active');
            $media->is_public = request('is_public');

            $media->mimetype = $file->getClientMimeType();
            $media->url = $filepath;
            $marketplace->mediaLibraries()->save($media);

            flash('Media saved');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceMediaLibrary  $marketplaceMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function show(MarketplaceMediaLibrary $marketplaceMediaLibrary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceMediaLibrary  $marketplaceMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, MarketplaceMediaLibrary $mediaLibrary)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$mediaLibrary->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.media-libraries.index', $marketplace->id);
        }

        $dropdown['groups'] = [
            'image' => 'Image',
            'video' => 'Video',
            'document' => 'Document',
            'other' => 'Other'
        ];

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.media-libraries.edit', compact(
            'user', 'marketplace', 'dropdown', 'mediaLibrary',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\MarketplaceMediaLibrary  $marketplaceMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, MarketplaceMediaLibrary $mediaLibrary)
    {
        request()->validate([
            'group' => 'required',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$mediaLibrary->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.media-libraries.index', $marketplace->id);
        }

        $media = $mediaLibrary;
        $media->group = request('group');
        $media->name = request('name');
        $media->description = request('description');
        $media->tags = request('tags');
        $media->active = request('active');
        $media->is_public = request('is_public');
        $media->save();

        flash('Media updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceMediaLibrary  $marketplaceMediaLibrary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplaceMediaLibrary $mediaLibrary)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$mediaLibrary->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.media-libraries.index', $marketplace->id);
        }

        $mediaLibrary->delete();

        flash('Media deleted');
        return redirect()->back();
    }
}
