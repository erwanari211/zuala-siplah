<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Store;
use Mail;
use App\Mail\Marketplaces\MarketplaceStoreApprovedMail;
use App\Mail\Marketplaces\MarketplaceStoreDisapprovedMail;

class StoreActivesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace, Store $store)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$store->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.stores.index', [$marketplace->id]);
        }

        $store->active = true;
        $store->save();

        // send approved mail
        $owner = $store->user;
        $emailData = compact('owner', 'marketplace', 'store');
        Mail::to($user->email)
            ->send(new MarketplaceStoreApprovedMail($emailData));

        flash('Store approved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, Store $store)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$store->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.stores.index', [$marketplace->id]);
        }

        $store->active = false;
        $store->save();

        // send disapproved mail
        $owner = $store->user;
        $emailData = compact('owner', 'marketplace', 'store');
        Mail::to($user->email)
            ->send(new MarketplaceStoreDisapprovedMail($emailData));

        flash('Store disapproved');
        return redirect()->back();
    }
}
