<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceBanner;
use App\Models\Modules\Marketplaces\MarketplaceBannerGroup;
use App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail;

class MarketplaceBannerGroupDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'banner_id' => 'required',
            'group_id' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $bannerId = request('banner_id');
        $banner = MarketplaceBanner::findOrFail($bannerId);
        if (!$banner->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banner-groups.index', $marketplace->id);
        }

        $groupId = request('group_id');
        $group = MarketplaceBannerGroup::findOrFail($groupId);
        if (!$group->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banner-groups.index', $marketplace->id);
        }

        MarketplaceBannerGroupDetail::updateOrCreate([
            'marketplace_banner_group_id' => $group->id,
            'marketplace_banner_id' => $banner->id,
        ], [
            'sort_order' => request('sort_order')
        ]);

        flash('Banner added to group');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplaceBannerGroup $bannerGroup, MarketplaceBanner $banner)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$banner->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banner-groups.index', $marketplace->id);
        }

        if (!$bannerGroup->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banner-groups.index', $marketplace->id);
        }

        MarketplaceBannerGroupDetail::where([
            'marketplace_banner_group_id' => $bannerGroup->id,
            'marketplace_banner_id' => $banner->id,
        ])->delete();

        flash('Banner removed from group');
        return redirect()->back();
    }
}
