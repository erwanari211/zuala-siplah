<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceStoreCollection;

class MarketplaceStoreCollectionDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'collection_id' => 'required',
            'store' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $collectionId = request('collection_id');
        $collection = $marketplace->storeCollections()->findOrFail($collectionId);

        $storeId = request('store');
        $store = $marketplace->stores()->findOrFail($storeId);

        MarketplaceStoreCollectionDetail::updateOrCreate([
            'marketplace_store_collection_id' => $collection->id,
            'store_id' => $store->id,
        ], [
            'sort_order' => request('sort_order')
        ]);

        flash('Store added to collection');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail  $marketplaceStoreCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function show(MarketplaceStoreCollectionDetail $marketplaceStoreCollectionDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail  $marketplaceStoreCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(MarketplaceStoreCollectionDetail $marketplaceStoreCollectionDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail  $marketplaceStoreCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarketplaceStoreCollectionDetail $marketplaceStoreCollectionDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail  $marketplaceStoreCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplaceStoreCollection $storeCollection, $store)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$storeCollection->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.store-collections.index', $marketplace->id);
        }

        $deleted = MarketplaceStoreCollectionDetail::where([
            'marketplace_store_collection_id' => $storeCollection->id,
            'store_id' => $store,
        ])->delete();

        if ($deleted) {
            flash('Store removed from collection');
        }

        return redirect()->back();
    }
}
