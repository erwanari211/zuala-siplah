<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceCategory;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplaceCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $parentId = request('parent_id');
        $categories = $marketplace->categories()->with('parent', 'childs');
        if ($parentId) {
            $categories = $categories->where('parent_id', $parentId);
        }
        if (($parentId == 0 && !is_null($parentId) )) {
            $categories = $categories->where('parent_id', $parentId);
        }
        $categories = $categories->latest()->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.categories.index', compact(
            'user', 'marketplace', 'categories',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $parentCategories = $marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->pluck('name', 'id');
        $dropdown['parent_categories'] = $parentCategories;
        $dropdown['parent_categories'][0] =  'Is Parent';
        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.categories.create', compact(
            'user', 'marketplace', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'parent' => 'required',
            'sort_order' => 'required|integer|min:1',
            'file' => 'image|max:4000',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $category = new MarketplaceCategory;
        $category->name = request('name');
        $category->slug = str_slug(request('name'));
        $category->description = request('description');
        $category->active = request('active');
        $category->sort_order = request('sort_order');
        $marketplace->categories()->save($category);

        $image = request('image');
        if ($image) {
            $filename = 'marketplace-categories';
            $filepath = upload_file($image, 'uploads', $filename);
            $category->image = $filepath;
            $category->save();
        }

        $parentId = request('parent');
         if ($parentId == 0) {
             $category->parent_id = 0;
             $category->save();
         } else {
             $parentCategory = $marketplace->categories()->find($parentId);
             if ($parentCategory && $parentCategory->isOwnedByMarketplace($marketplace->id)) {
                 $category->parent_id = $parentCategory->id;
                 $category->save();
             }
         }

        if (!$category->checkIsUnique()) {
            flash('Slug is not unique. Please change slug')->warning();
        }

        flash('Category saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, MarketplaceCategory $category)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$category->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.categories.index', $marketplace->id);
        }

        $categories = $marketplace->categories()
            ->where([
                ['parent_id', 0],
                ['id', '!=', $category->id],
            ])
            ->pluck('name', 'id');

        $parentCategories = $marketplace->categories()
            ->where('parent_id', 0)
            ->where('id', '<>', $category->id)
            ->orderBy('name')
            ->pluck('name', 'id');
        $dropdown['parent_categories'] = $parentCategories;
        $dropdown['parent_categories'][0] =  'Is Parent';
        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $dropdown['categories'] = collect(['0' => 'No Parent'] + $categories->toArray());
        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.categories.edit', compact(
            'user', 'marketplace', 'category', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, MarketplaceCategory $category)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'parent' => 'required',
            'sort_order' => 'required|integer|min:1',
            'file' => 'image|max:4000',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$category->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.categories.index', $marketplace->id);
        }

        // $category->parent_id = request('parent_id');
        $category->name = request('name');
        $category->slug = str_slug(request('slug'));
        $category->description = request('description');
        $category->active = request('active');
        $category->sort_order = request('sort_order');
        $category->save();

        $image = request('image');
        if ($image) {
            $filename = 'marketplace-categories';
            $filepath = upload_file($image, 'uploads', $filename);
            $category->image = $filepath;
            $category->save();
        }

        $parentId = request('parent');
         if ($parentId == 0) {
             $category->parent_id = 0;
             $category->save();
         } else {
             $parentCategory = $marketplace->categories()->find($parentId);
             if ($parentCategory && $parentCategory->isOwnedByMarketplace($marketplace->id)) {
                 $category->parent_id = $parentCategory->id;
                 $category->save();
             }
         }

        if (!$category->checkIsUnique()) {
            flash('Slug is not unique. Please change slug')->warning();
        }

        flash('Category updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplaceCategory $category)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$category->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.categories.index', $marketplace->id);
        }

        $category->childs()->update([
            'parent_id' => 0
        ]);
        $category->products()->detach();
        $category->delete();

        flash('Category deleted');
        return redirect()->back();
    }
}
