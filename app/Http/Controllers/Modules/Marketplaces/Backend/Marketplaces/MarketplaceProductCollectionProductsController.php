<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\WebsiteProductCollection;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplaceProductCollectionProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $storeId = request('store');
        $name = request('name');
        $sku = request('sku');

        $products = Product::has('store')->with('store', 'store.marketplace');
        if ($marketplace) {
            $products = $products->whereHas('store.marketplace', function($q) use ($marketplace) {
                $q->where('id', $marketplace->id);
            });
        }
        if ($storeId) {
            $store = $marketplace->stores()->find($storeId);
            if ($store) {
                $products = $products->whereHas('store', function($q) use ($storeId) {
                    $q->where('id', $storeId);
                });
            }
        }
        if ($name) {
            $products = $products->where('name', 'LIKE', "%{$name}%");
        }
        if ($sku) {
            $products = $products->where('sku', 'LIKE', "%{$sku}%");
        }
        $products = $products->orderBy('name')->paginate(20);

        $dropdown['stores'] = [];
        $stores = $marketplace->stores()->orderBy('name')->pluck('name', 'id');
        $dropdown['stores'] = $stores;

        $collections = $marketplace->productCollections()->active()->orderBy('name')->pluck('name', 'id');
        $dropdown['collections'] = $collections;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.product-collections.products.index', compact(
            'marketplace', 'products', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
