<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\MarketplaceStoreCollection;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplaceStoreCollectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $storeCollections = $marketplace->storeCollections()->withCount('details')->latest()->paginate(10);
        // return $storeCollections;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.store-collections.index', compact(
            'user', 'marketplace', 'storeCollections',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.store-collections.create', compact(
            'user', 'marketplace', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $collection = new MarketplaceStoreCollection;
        $collection->name = request('name');
        $collection->display_name = request('display_name');
        $collection->active = request('active');
        $collection->sort_order = request('sort_order');
        $marketplace->storeCollections()->save($collection);

        flash('Store Collection saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStoreCollection  $marketplaceStoreCollection
     * @return \Illuminate\Http\Response
     */
    public function show(Marketplace $marketplace, MarketplaceStoreCollection $storeCollection)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$storeCollection->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.store-collections.index', $marketplace->id);
        }

        // $dropdown['types'] = ['normal'=>'Normal'];
        $details = $storeCollection->details()->sort()->with('store')->get();
        // return $details;
        $stores = $marketplace->stores()->orderBy('name')->pluck('name', 'id');

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['stores'] = $stores;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.store-collections.show', compact(
            'user', 'marketplace', 'storeCollection', 'dropdown', 'details',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStoreCollection  $marketplaceStoreCollection
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, MarketplaceStoreCollection $storeCollection)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$storeCollection->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.store-collections.index', $marketplace->id);
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.store-collections.edit', compact(
            'user', 'marketplace', 'dropdown', 'storeCollection',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStoreCollection  $marketplaceStoreCollection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, MarketplaceStoreCollection $storeCollection)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$storeCollection->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.store-collections.index', $marketplace->id);
        }

        $collection = $storeCollection;
        $collection->name = request('name');
        $collection->display_name = request('display_name');
        $collection->active = request('active');
        $collection->sort_order = request('sort_order');
        $collection->save();

        flash('Store Collection updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStoreCollection  $marketplaceStoreCollection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplaceStoreCollection $storeCollection)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$storeCollection->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.store-collections.index', $marketplace->id);
        }

        // $storeCollection->details()->delete();
        $storeCollection->delete();

        flash('Store Collection deleted');
        return redirect()->back();
    }
}
