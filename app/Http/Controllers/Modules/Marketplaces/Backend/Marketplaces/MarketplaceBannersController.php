<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceBanner;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplaceBannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $banners = $marketplace->banners()
            ->with('marketplaceBannerGroupDetails')
            ->with('marketplaceBannerGroupDetails.marketplaceBannerGroup')
            ->latest()
            ->paginate(10);
        $dropdown['banner_groups'] = $marketplace->bannerGroups()->active()->pluck('name', 'id');

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.banners.index', compact(
            'marketplace', 'banners', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.banners.create', compact(
            'user', 'marketplace', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'image' => 'image|required|max:4000',
            'link' => 'url|nullable',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $image = request('image');
        if ($image) {
            $filename = 'marketplace-banners';
            $filepath = upload_file($image, 'uploads', $filename);
        }

        $banner = new MarketplaceBanner;
        $banner->image = $filepath;
        $banner->link = request('link');
        $banner->active = request('active');
        $banner->expired_at = request('expired_at');
        $banner->note = request('note');
        $banner->sort_order = request('sort_order');
        $banner->marketplace_id = $marketplace->id;
        $banner->save();

        flash('Banner saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, MarketplaceBanner $banner)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$banner->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banners.index', $marketplace->id);
        }
        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['banner_types'] = [
            'banner' => 'Banner',
            'video' => 'Video',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.banners.edit', compact(
            'user', 'marketplace', 'banner', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, MarketplaceBanner $banner)
    {
        request()->validate([
            'image' => 'image|max:4000',
            'link' => 'url|nullable|required_if:type,video',
            'sort_order' => 'required|integer|min:1',
            'type' => 'required|in:banner,video',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$banner->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banners.index', $marketplace->id);
        }

        $type = request('type');
        if ($type == 'video') {
            // check link is video
        }

        $banner->link = request('link');
        $banner->active = request('active');
        $banner->expired_at = request('expired_at');
        $banner->note = request('note');
        $banner->sort_order = request('sort_order');
        $banner->type = request('type');
        $banner->save();

        $image = request('image');
        if ($image) {
            $filename = 'marketplace-banners';
            $filepath = upload_file($image, 'uploads', $filename);
            $banner->image = $filepath;
            $banner->save();
        }

        $isExpired = false;
        $now = date("Y-m-d");
        if ($now > $banner->expired_at) {
            $isExpired = true;
        }
        if ($isExpired) {
            $banner->active = false;
            $banner->save();
        }

        flash('Banner updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplaceBanner $banner)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$banner->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banners.index', $marketplace->id);
        }

        // delete from group banner
        $banner->delete();

        flash('Banner deleted');
        return redirect()->back();
    }
}
