<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderHistory;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Website\WebsiteSettings;

class PaymentConfirmationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('Order not found')->error();
            return redirect()->back();
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.users.orders.payment-confirmations.create', compact(
            'user', 'order',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order)
    {
        request()->validate([
            'file' => 'required|image|max:4000',
        ]);

        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('Order not found')->error();
            return redirect()->back();
        }

        $file = request('file');
        $filename = 'payment-confirmation';
        $filepath = upload_file($file, 'uploads', $filename);

        $status = OrderStatus::where('key', 'payment confirmation')->first();

        $history = new OrderHistory;
        $history->order_id = $order->id;
        $history->order_status_id = $status->id;
        $history->order_status = $status->key;
        $history->notify = true;
        $history->note = request('note');
        $history->has_attachment = true;
        $history->save();
        $history->setMeta('attachments', $filepath);

        $order->order_status_id = $status->id;
        $order->order_status = $status->key;
        $order->save();
        $order->touch();

        flash('Payment confirmation saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
