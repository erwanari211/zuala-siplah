<?php

namespace App\Http\Controllers\Examples;

use App\Models\Examples\ExamplePost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Examples\PostRequest;

class ExamplePostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = ExamplePost::latest()->paginate(10);
        return view('examples.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('examples.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $post = new ExamplePost;
        $post->title = request('title');
        $post->slug = request('slug');
        $post->content = request('content');
        $post->excerpt = request('excerpt');
        $post->save();

        flash('Data saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Examples\ExamplePost  $post
     * @return \Illuminate\Http\Response
     */
    public function show(ExamplePost $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Examples\ExamplePost  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamplePost $post)
    {
        $postStatusOptions = ['published'=>'Published', 'draft'=>'Draft'];
        $commentStatusOptions = ['open'=>'Open', 'close'=>'Close'];

        return view('examples.posts.edit', compact('post', 'postStatusOptions', 'commentStatusOptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Examples\ExamplePost  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, ExamplePost $post)
    {
        $post->title = request('title');
        $post->slug = request('slug');
        $post->content = request('content');
        $post->excerpt = request('excerpt');
        $post->post_status = request('post_status');
        $post->comment_status = request('comment_status');
        $post->published_at = request('published_at');
        $post->save();

        flash('Data updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Examples\ExamplePost  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = ExamplePost::withTrashed()->find($id);
        if ($post->trashed()) {
            $post->forceDelete();
        }

        flash('Data deleted');
        return redirect()->back();
    }
}
