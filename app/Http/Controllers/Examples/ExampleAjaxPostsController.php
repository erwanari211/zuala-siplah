<?php

namespace App\Http\Controllers\Examples;

use App\Models\Examples\ExamplePost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Examples\AjaxPostRequest;

class ExampleAjaxPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('examples.ajax-posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AjaxPostRequest $request)
    {
        $post = new ExamplePost;
        $post->title = request('title');
        $post->slug = request('slug');
        $post->content = request('content');
        $post->excerpt = request('excerpt');
        $post->save();

        return response()->json(['message'=>'Data saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Examples\ExamplePost  $post
     * @return \Illuminate\Http\Response
     */
    public function show(ExamplePost $ajaxPost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Examples\ExamplePost  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamplePost $ajaxPost)
    {
        $post = $ajaxPost;
        $postStatusOptions = ['published'=>'Published', 'draft'=>'Draft'];
        $commentStatusOptions = ['open'=>'Open', 'close'=>'Close'];

        return view('examples.ajax-posts.edit', compact('post', 'postStatusOptions', 'commentStatusOptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Examples\ExamplePost  $post
     * @return \Illuminate\Http\Response
     */
    public function update(AjaxPostRequest $request, ExamplePost $ajaxPost)
    {
        $post = $ajaxPost;
        $post->title = request('title');
        $post->slug = request('slug');
        $post->content = request('content');
        $post->excerpt = request('excerpt');
        $post->post_status = request('post_status');
        $post->comment_status = request('comment_status');
        $post->published_at = request('published_at');
        $post->save();

        return response()->json(['message'=>'Data updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Examples\ExamplePost  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
