<?php

namespace App\Models\Modules\Website;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class WebsiteBlogBanner extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getBlogBanners()
    {
        $cacheDurationSecond = 600;
        $blogBanners = Cache::remember('blogBanners', $cacheDurationSecond, function () {
            $banners = static::where('active', true)
                ->orderBy('position')
                ->orderBy('sort_order')
                ->get();
            return $banners->groupBy('position');
        });

        return $blogBanners;
    }

    public static function forgetBlogBanners()
    {
        $cacheDeleted = false;

        Cache::forget('blogBanners');
        $cacheDeleted = true;

        return $cacheDeleted;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
