<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedBy($userId)
    {
        return $userId == $this->user_id;
    }

    public function isOwnedByStore($storeId)
    {
        return $storeId == $this->store_id;
    }

    public function getOrderMetas()
    {
        $metas = $this->orderMetas()
            ->orderBy('group')
            ->orderBy('sort_order')
            ->get();

        $groupedMetas = [];
        foreach ($metas as $meta) {
            $group = $meta['group'];
            $key = $meta['key'];
            $value = $meta['value'];
            $groupedMetas[$group][$key] = $value;
        }
        return $groupedMetas;
    }

    public static function getStoreOrderStatuses($storeId)
    {
        $statuses = ['pending', 'paid', 'shipping', 'completed', 'canceled'];
        $orderStatuses = [];
        foreach ($statuses as $status) {
            $count = static::where('store_id', $storeId)->filter($status)->count();
            $orderStatuses[$status] = [
                'status' => $status,
                'total' => $count,
            ];
        }
        return $orderStatuses;
    }

    public static function getUserOrderStatuses($userId)
    {
        $statuses = ['pending', 'paid', 'shipping', 'completed', 'canceled'];
        $orderStatuses = [];
        foreach ($statuses as $status) {
            $count = static::where('user_id', $userId)->filter($status)->count();
            $orderStatuses[$status] = [
                'status' => $status,
                'total' => $count,
            ];
        }
        return $orderStatuses;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function products()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\OrderProduct');
    }

    public function histories()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\OrderHistory');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Store');
    }

    public function orderMetas()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\OrderMeta');
    }

    public function activities()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\OrderActivity');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeFilter($query, $status = null)
    {
        if ($status) {
            if ($status == 'pending') {
                $query->whereIn('order_status', ['pending']);
            }
            if ($status == 'paid') {
                $query->whereIn('order_status', ['payment confirmation', 'paid']);
            }
            if ($status == 'shipping') {
                $query->whereIn('order_status', ['processing', 'processed', 'shipped']);
            }
            if ($status == 'completed') {
                $query->whereIn('order_status', ['completed']);
            }
            if ($status == 'canceled') {
                $query->whereIn('order_status', ['canceled']);
            }
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
