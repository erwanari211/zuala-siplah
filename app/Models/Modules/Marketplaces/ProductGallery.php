<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class ProductGallery extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedByProduct($productId)
    {
        return $productId == $this->product_id;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function product()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Product');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getFileTypeAttribute($value)
    {
        $fileType = 'file';
        $type = $this->type;

        if (strpos($type, 'image/') !== false) {
            $fileType = 'image';
        }

        if (strpos($type, 'video/') !== false) {
            $fileType = 'video';
        }

        if (strpos($type, 'pdf') !== false) {
            $fileType = 'pdf';
        }

        if (strpos($type, 'spreadsheet') !== false) {
            $fileType = 'excel';
        }

        if (strpos($type, 'excel') !== false) {
            $fileType = 'excel';
        }

        if (strpos($type, 'xls') !== false) {
            $fileType = 'excel';
        }

        return $fileType;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
