<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class StoreCategory extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function subcategories()
    {
        $parent_id = $this->id;
        return StoreCategory::where('parent_id', $parent_id)->get();
    }

    public function checkIsUnique($slug = null)
    {
        $count = StoreCategory::where([
            'store_id' => $this->store_id,
            'slug' => $this->slug,
        ])->count();
        $isUnique = true;

        if ($count > 1) {
            $isUnique = false;
        }

        return $isUnique;
    }

    public function isOwnedByStore($storeId)
    {
        return $storeId == $this->store_id;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function store()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Store');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Modules\Marketplaces\Product', 'store_category_product');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreCategory', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreCategory', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\StoreCategory', 'parent_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
