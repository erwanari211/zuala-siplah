<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class MarketplaceCategory extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedByMarketplace($marketplaceId)
    {
        return $marketplaceId == $this->marketplace_id;
    }

    public function subcategories()
    {
        $parent_id = $this->id;
        return MarketplaceCategory::where('parent_id', $parent_id)->get();
    }

    public function checkIsUnique($slug = null)
    {
        $slug = $slug ? $slug : $this->slug;

        $result = MarketplaceCategory::where([
            'marketplace_id' => $this->marketplace_id,
            'slug' => $slug,
        ])->get();

        if ($result->count() > 1) {
            return false;
        }

        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function marketplace()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Marketplace');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Modules\Marketplaces\Product');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceCategory', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceCategory', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\MarketplaceCategory', 'parent_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
