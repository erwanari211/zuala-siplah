<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class MarketplaceActivitySearch extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'user_id', 'marketplace_id', 'store_id', 'search', 'viewed_at', 'ip_address'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function newSearch($search = null, $data = [])
    {
        if ($search) {
            MarketplaceActivitySearch::updateOrCreate([
                'user_id' => auth()->check() ? auth()->user()->id : null,
                'marketplace_id' => isset($data['marketplace_id']) ? $data['marketplace_id'] : null,
                'store_id' => isset($data['store_id']) ? $data['store_id'] : null,
                'search' => $search,
                'ip_address' => request()->ip(),
                [\DB::raw("DATE(viewed_at)"), date('Y-m-d') ]
            ], [
                'viewed_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
