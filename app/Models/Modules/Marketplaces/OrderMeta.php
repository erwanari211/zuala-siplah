<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class OrderMeta extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'order_id', 'group', 'key', 'value', 'sort_order',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function setOrderMeta($order, $attributes, $group = null)
    {
        $index = 1;
        $result = [];
        foreach ($attributes as $key => $value) {
            $meta = new OrderMeta;
            $meta->order_id = $order->id;
            $meta->group = $group;
            $meta->key = $key;
            $meta->value = $value;
            $meta->sort_order = $index;
            $meta->save();

            $result[] = $meta;
            $index++;
        }
        return $result;
    }

    public static function getOrderAddressAttributes($address)
    {
        $attributes = [];
        $attributes['id'] = $address['id'];
        $attributes['full_name'] = $address['full_name'];
        $attributes['company'] = $address['company'];
        $attributes['phone'] = $address['phone'];
        $attributes['address'] = $address['address'];
        $attributes['district'] = $address['district']['name'];
        $attributes['city'] = $address['city']['name'];
        $attributes['province'] = $address['province']['name'];
        $attributes['postcode'] = $address['postcode'];

        return $attributes;
    }

    public static function setOrderPaymentAddress($order, $address)
    {
        $attributes = static::getOrderAddressAttributes($address);
        return static::setOrderMeta($order, $attributes, 'payment_address');
    }

    public static function setOrderShippingAddress($order, $address)
    {
        $attributes = static::getOrderAddressAttributes($address);
        return static::setOrderMeta($order, $attributes, 'shipping_address');
    }

    public static function getOrderShippingMethodAttributes($method)
    {
        $attributes = [];
        $attributes['code'] = $method['code'];
        $attributes['name'] = $method['name'];
        $attributes['service'] = $method['service'];
        $attributes['description'] = $method['description'];
        $attributes['value'] = $method['value'];

        return $attributes;
    }

    public static function setOrderShippingMethod($order, $method)
    {
        $attributes = static::getOrderShippingMethodAttributes($method);
        return static::setOrderMeta($order, $attributes, 'shipping_method');
    }

    public static function getOrderPaymentMethodAttributes($method)
    {
        $attributes = [];
        $attributes['name'] = $method['name'];
        $attributes['label'] = $method['label'];
        $attributes['info'] = $method['info'];
        $attributes['note'] = $method['note'];
        $attributes['unique_number'] = $method['unique_number'];

        return $attributes;
    }

    public static function setOrderPaymentMethod($order, $method)
    {
        $attributes = static::getOrderPaymentMethodAttributes($method);
        return static::setOrderMeta($order, $attributes, 'payment_method');
    }

    public static function getOrderMetaAttributes($detail)
    {
        $attributes = [];
        $attributes['subtotal'] = $detail['subtotal'];
        $attributes['shipping'] = $detail['shipping'];
        $attributes['adjustment_price'] = 0;
        $attributes['unique_number'] = $detail['unique_number'];
        $attributes['total'] = $detail['total'];
        $attributes['store_seller'] = $detail['store_seller'] ? $detail['store_seller'] : null;

        return $attributes;
    }

    public static function setOrderDetail($order, $detail)
    {
        $attributes = static::getOrderMetaAttributes($detail);
        return static::setOrderMeta($order, $attributes, 'order');
    }

    public static function updateOrderMeta($orderId, $data = [])
    {
        $group = $data['group'] ? $data['group'] : false;
        $key = $data['key'] ? $data['key'] : false;
        $value = $data['value'] ? $data['value'] : null;

        $result = false;
        if ($group && $key) {
            OrderMeta::updateOrCreate([
                'order_id' => $orderId,
                'group' => $group,
                'key' => $key,
            ], [
                'value' => $value,
            ]);
            $result = true;
        }

        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
