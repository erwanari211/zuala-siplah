<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use Plank\Metable\Metable;

class Store extends Model
{
    use Metable;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];
    protected $with = ['meta'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedBy($userId)
    {
        return $userId == $this->user_id;
    }

    public function isOwnedByMarketplace($marketplaceId)
    {
        return $marketplaceId == $this->marketplace_id;
    }

    public static function getUniqueSlug($slug, $id = null, $index = 0)
    {
        $newSlug = ($index == 0) ? $slug : $slug.'--'.$index;
        $rules = ['slug'=>'unique:stores,slug'];
        if ($id) {
            $rules = ['slug'=>'unique:stores,slug,'.$id];
        }

        $validator = \Validator::make(['slug'=>$newSlug],$rules);
        if($validator->fails()){
            $index++;
            return static::getUniqueSlug($slug, $id, $index);
        }

        return $newSlug;
    }

    public static function randomUniqueCode()
    {
        $table = 'stores';
        $column = 'unique_code';

        $id = strtolower(str_random(8));
        $validator = \Validator::make(['id'=>$id],['id'=>'unique:'.$table.','.$column]);
        if($validator->fails()){
            $this->randomUniqueCode();
        }

        return $id;
    }

    public static function fillUniqueCode()
    {
        $total = 0;
        $stores = static::get();
        foreach ($stores as $store) {
            if (!$store->unique_code) {
                $uniqueCode = $store->randomUniqueCode();
                $store->unique_code = $uniqueCode;
                $store->save();
                $total++;
            }
        }

        dump('completed');
        if ($total) {
            dump($total.' row affected');
        }
    }

    public function hasDefaultStoreAddress()
    {
        return $this->addresses()->where('default_address', 1)->count() > 0;
    }

    public function getDefaultStoreAddress()
    {
        return $this->addresses()->where('default_address', 1)
            ->with('province', 'city', 'district')
            ->first();
    }

    public function mappedSettings()
    {
        $mappedSettings = [];
        $settings = $this->settings;
        if ($settings) {
            $mappedSettings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });
        }

        return $mappedSettings;
    }

    public function updateLastOnline()
    {
        $now = date('Y-m-d H:i:s');
        $this->setMeta('last_online', $now);
        return $now;
    }

    public function addView($view = 1)
    {
        $this->increment('views', $view);
        return $this->views;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function marketplace()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Marketplace');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Product');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreAddress');
    }

    public function paymentMethods()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StorePaymentMethod');
    }

    public function shippingMethods()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreShippingMethod');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Order');
    }

    public function categories()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreCategory');
    }

    public function settings()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreSetting');
    }

    public function banners()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreBanner');
    }

    public function bannerGroups()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreBannerGroup');
    }

    public function storeFeaturedProducts()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreFeaturedProduct');
    }

    public function menus()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreMenu');
    }

    public function notes()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreNote');
    }

    public function productDiscussions()
    {
        return $this->hasManyThrough('App\Models\Modules\Marketplaces\ProductDiscussion', 'App\Models\Modules\Marketplaces\Product');
    }

    public function productReviews()
    {
        return $this->hasManyThrough('App\Models\Modules\Marketplaces\ProductReview', 'App\Models\Modules\Marketplaces\Product');
    }

    public function mediaLibraries()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreMediaLibrary');
    }

    public function storeSellers()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreSeller');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeFilterActive($query, $status = null)
    {
        if ($status) {
            if (is_array($status)) {
                $query->whereIn('active', $status);
            } else {
                $query->where('active', $status);
            }
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getStoreUrlAttribute($value)
    {
        $store = $this->slug;
        $marketplace = $this->marketplace->slug;
        return route('stores.show', [$marketplace, $store]);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
