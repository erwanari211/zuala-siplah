<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class Marketplace extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedBy($userId)
    {
        return $userId == $this->user_id;
    }

    public static function getUniqueSlug($slug, $id = null, $index = 0)
    {
        $newSlug = ($index == 0) ? $slug : $slug.'--'.$index;
        $rules = ['slug'=>'unique:marketplaces,slug'];
        if ($id) {
            $rules = ['slug'=>'unique:marketplaces,slug,'.$id];
        }

        $validator = \Validator::make(['slug'=>$newSlug],$rules);
        if($validator->fails()){
            $index++;
            return static::getUniqueSlug($slug, $id, $index);
        }

        return $newSlug;
    }

    public static function randomUniqueCode()
    {
        $table = 'marketplaces';
        $column = 'unique_code';

        $id = strtolower(str_random(8));
        $validator = \Validator::make(['id'=>$id],['id'=>'unique:'.$table.','.$column]);
        if($validator->fails()){
            $this->randomUniqueCode();
        }

        return $id;
    }

    public static function fillUniqueCode()
    {
        $total = 0;
        $marketplaces = static::get();
        foreach ($marketplaces as $marketplace) {
            if (!$marketplace->unique_code) {
                $uniqueCode = $marketplace->randomUniqueCode();
                $marketplace->unique_code = $uniqueCode;
                $marketplace->save();
                $total++;
            }
        }

        dump('completed');
        if ($total) {
            dump($total.' row affected');
        }
    }

    public function addView($view = 1)
    {
        $this->increment('views', $view);
        return $this->views;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function stores()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Store');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->hasManyThrough('App\Models\Modules\Marketplaces\Product', 'App\Models\Modules\Marketplaces\Store');
    }

    public function categories()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceCategory');
    }

    public function promos()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplacePromo');
    }

    public function settings()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceSetting');
    }

    public function banners()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceBanner');
    }

    public function bannerGroups()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceBannerGroup');
    }

    public function activitySearches()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceActivitySearch');
    }

    public function promoPopularSearches()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch');
    }

    public function storeCollections()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceStoreCollection');
    }

    public function productCollections()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceProductCollection');
    }

    public function mediaLibraries()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceMediaLibrary');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getMarketplaceUrlAttribute($value)
    {
        $marketplace = $this->slug;
        return route('marketplaces.show', [$marketplace]);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
