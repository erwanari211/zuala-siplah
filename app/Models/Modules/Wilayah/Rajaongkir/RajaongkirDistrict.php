<?php

namespace App\Models\Modules\Wilayah\Rajaongkir;

use Illuminate\Database\Eloquent\Model;

class RajaongkirDistrict extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    protected $primaryKey = 'district_id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function city()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity', 'city_id');
    }

    public function indonesiaDistrict()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict',
            'indonesia_district_rajaongkir',
            'rajaongkir_district_id',
            'indonesia_district_id'
        )->withPivot('is_exact');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSearch($query, $name = null)
    {
        if ($name) {
            $query->where('district_name', 'like', "%$name%");
        }

        return $query;
    }

    public function scopeByCity($query, $cityId = null)
    {
        if ($cityId) {
            $query->where('city_id', $cityId);
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
