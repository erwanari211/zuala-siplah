<?php

namespace App\Models\Modules\Wilayah\Rajaongkir;

use Illuminate\Database\Eloquent\Model;

class RajaongkirProvince extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    protected $primaryKey = 'province_id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function cities()
    {
        return $this->hasMany('App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity', 'province_id');
    }

    public function indonesiaProvince()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince',
            'indonesia_province_rajaongkir',
            'rajaongkir_province_id',
            'indonesia_province_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSearch($query, $name = null)
    {
        if ($name) {
            $query->where('province', 'like', "%$name%");
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
