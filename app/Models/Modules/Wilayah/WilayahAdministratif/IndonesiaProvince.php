<?php

namespace App\Models\Modules\Wilayah\WilayahAdministratif;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class IndonesiaProvince extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    public $incrementing = false;
    protected $keyType = 'string';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getProvincesWithDetails($remember = 600)
    {
        $cacheDurationSecond = $remember;
        $result = Cache::remember('provincesWithDetails', $cacheDurationSecond, function () {
            $wilayah = static::orderBy('name')->with(['cities' => function($q){
                $q->orderBy('name')
                    ->with(['districts' => function($q2){
                        $q2->orderBy('name');
                    }]);
            }])->get();

            $provincesWithDetails = [];
            $provincesWithDetails = $wilayah->mapWithKeys(function($provinceData) {
                $cities = $provinceData['cities'];
                $cities = $cities->mapWithKeys(function($cityData) {
                    $districts = $cityData['districts'];
                    $districts = $districts->mapWithKeys(function($districtData) {
                        return [$districtData['id'] => $districtData];
                    });
                    $cityData['districts_data'] = $districts;

                    return [$cityData['id'] => $cityData];
                });
                $provinceData['cities_data'] = $cities;

                return [$provinceData['id'] => $provinceData];
            });

            return $provincesWithDetails;
        });

        return $result;
    }

    public static function getDropdown()
    {
        return static::orderBy('name')->pluck('name', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function cities()
    {
        return $this->hasMany('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity', 'province_id');
    }

    public function kemdikbudProvince()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince',
            'indonesia_province_kemdikbud',
            'indonesia_province_id',
            'kemdikbud_province_id'
        );
    }

    public function rajaongkirProvince()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince',
            'indonesia_province_rajaongkir',
            'indonesia_province_id',
            'rajaongkir_province_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSearch($query, $name = null)
    {
        if ($name) {
            $query->where('name', 'like', "%$name%");
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
