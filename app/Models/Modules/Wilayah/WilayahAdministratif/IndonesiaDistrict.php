<?php

namespace App\Models\Modules\Wilayah\WilayahAdministratif;

use Illuminate\Database\Eloquent\Model;

class IndonesiaDistrict extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    public $incrementing = false;
    protected $keyType = 'string';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getDropdown($cityId = null)
    {
        $districts = static::orderBy('name');
        if ($cityId) {
            $districts = $districts->where('city_id', $cityId);
        }
        $districts = $districts->pluck('name', 'id');
        return $districts;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function villages()
    {
        return $this->hasMany('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage', 'district_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity', 'city_id');
    }

    public function kemdikbudDistrict()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict',
            'indonesia_district_kemdikbud',
            'indonesia_district_id',
            'kemdikbud_district_id'
        );
    }

    public function rajaongkirDistrict()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict',
            'indonesia_district_rajaongkir',
            'indonesia_district_id',
            'rajaongkir_district_id'
        )->withPivot('is_exact');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSearch($query, $name = null)
    {
        if ($name) {
            $query->where('name', 'like', "%$name%");
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
