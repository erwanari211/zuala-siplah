<?php

namespace App\Models\Modules\Wilayah\Kemdikbud;

use Illuminate\Database\Eloquent\Model;

class KemdikbudProvince extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    public $incrementing = false;
    protected $keyType = 'string';
    // protected $casts = ['id' => 'string'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function cities()
    {
        return $this->hasMany('App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity', 'province_id');
    }

    public function indonesiaProvince()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince',
            'indonesia_province_kemdikbud',
            'kemdikbud_province_id',
            'indonesia_province_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSearch($query, $name = null)
    {
        if ($name) {
            $query->where('name', 'like', "%$name%");
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
